<?php
$lang_name = 'Mozambique'; // Country Name
$lang_meta_title ='CodeJIKA.com - Coding Clubs in Schools in Mozambique - Have fun, make friends & build websites.';
$lang_meta_description = 'CodeJIKA - eco-systems of vibrant student-run coding clubs in secondary schools in Mozambique';
$lang_meta_keywords = 'Coding, clubs, codejika, schools, africa, Mozambique';
$lang_h1_seo ='<div style="font-size: 70%;padding-bottom: 30px;line-height: 86%;">OLA MOZAMBIQUE</div> Let\'s learn coding!';
?>