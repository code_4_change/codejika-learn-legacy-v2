<?php
$lang_name = 'Johannesburg'; // Country Name
$lang_where = 'in Johannesburg, South Africa'; // location description
$lang_meta_title ='CodeJIKA.com - Coding Clubs in Schools in Johannesburg - Have fun, make friends & build websites.';
$lang_h1_seo ='<div style="font-size: 70%;padding-bottom: 30px;line-height: 86%;">HELLO JOHANNESBURG</div> Let\'s learn coding!';
?>