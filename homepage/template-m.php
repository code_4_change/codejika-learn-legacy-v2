<?php

$dp  = "<div class=\"box-menu-wraper\">
  <div class=\"container\">
    <div class=\"bxmenu-wrap\">
      <h3>Menu</h3>
      <div class=\"row\">
        <div class=\"col-lg-12 col-md-12\">
          <a href=\"/schools\"><div class=\"menu-col\">
          <h4>Schools</h4>
          <p>Join the network of forward-thinking schools building an eco-system of fun, student-run coding clubs.</p>
          
          </div></a>
        </div>
        <div class=\"col-lg-12 col-md-12\">
          <a href=\"/business\"><div class=\"menu-col\">
          <h4>Business</h4>
          <p>Coding in schools allows youth to create and build tools to empower SMEs and the economy.</p>
         
          </div> </a>
        </div>
        <div class=\"col-lg-12 col-md-12\">
          <a href=\"/coding-resources\">
					<div class=\"menu-col\">
          <h4>CodeJIKA Resources</h4>
          <p>Find the tools to teach, inspire, advertise and \"Rock this\". :)</p>
          
          </div></a>
        </div>
       <!-- <div class=\"col-lg-12 col-md-12\">
          <a href=\"/coding-clubs\">
						<div class=\"menu-col\">
          <h4>Coding Clubs</h4>
          <p>Check out who's the top coding clubs in Southern Africa</p>
          
          </div>
					</a>
        </div>-->
				<div class=\"col-lg-12 col-md-12\">
          <a href=\"#curriculum_section\">
					<div class=\"menu-col\">
          <h4>Curriculum</h4>
          <p>YAAASSSS! This is the fun part. </p>
						<p>Download our offline tools or link up to rad mobile lessons.</p>
          </div>
					</a>
        </div>
				<!--<div class=\"col-lg-12 col-md-12\">
      		<a href=\"/news\">
					<div class=\"menu-col\">
						<h4>News & Media</h4>
						<p>What's the latest blog& reports.</p>
						<p>Find our media pack & graphics.</p>
					</div>
					</a>
				</div> -->
			</div>
    </div>
  </div>
</div>";
$curriculam  = "<div class=\"box-menu-wraper x-box\">
   <div class=\"container\" id=\"curriculum_section\">
      <div class=\"bxmenu-wrap\">
         <h3>Curriculum</h3>
         <div class=\"main-box\" >
                <div class=\"sub-headings\"><h4>Online</h4></div>
            <div class=\"row\">
               <div class=\"col-lg-12 col-md-12\">
                  <div class=\"inside-col\">
                     <h4>5-Minute-Website</h4>
                     <h6>Lovin it! Do not ask me, just do it!</h6>
                     <div class=\"table-content\">
                        <div class=\"fst\">
                           <p>Awesome</p>
                           <p>Level: Beginner</p>
                        </div>
                        <div class=\"scnd\">
                           <p>Level: Beginner</p>
                           <p>Time: 10 Mins</p>
                        </div>
                     </div>
                     <div class=\"foot-links\">
                        <a href=\"/learn/5-minute-website?\" class=\"link btn-green\">Start Now</a>
                     </div>
                  </div>
               </div>
               <div class=\"col-lg-12 col-md-12\">
                  <div class=\"inside-col\">
                     <h4>Project 1</h4>
                     <h6>Instead of watching a movie...</h6>
                     <div class=\"table-content\">
                        <div class=\"fst\">
                           <p>HTML & CSS Basics</p>
                           <p>4 Trainings</p>
                        </div>
                        <div class=\"scnd\">
                           <p>Level: Beginner</p>
                           <p>Time: 90 Mins</p>
                        </div>
                     </div>
                     <div class=\"foot-links\">
                        <a href=\"/learn/P1Training1\" class=\"link btn-green\">Start Now</a>
                     </div>
                  </div>
               </div>
            </div>
                <div class=\"sub-headings\"><h4>Offline</h4></div>
            <div class=\"row\">
               <div class=\"col-lg-12 col-md-12\">
                  <div class=\"inside-col\">
                     <h4>5-Minute-Website</h4>
                     <h6>Lovin it! Do not ask me, just do it!</h6>
                     <div class=\"table-content\">
                        <div class=\"fst\">
                           <p>Awesome</p>
                           <p>Level: Beginner</p>
                        </div>
                        <div class=\"scnd\">
                           <p>Level: Beginner</p>
                           <p>Time: 20 Mins</p>
                        </div>
                     </div>
                     <div class=\"foot-links\">
                        <a href=\"/curriculum\" class=\"link btn-gray\">Learn More</a>
                        <a href=\"/wp-content/uploads/2019/08/2019-CodeJIKA-5MW-Instr-How-to-teach-the-5-Minute-Website-v1.pdf\" download class=\"link color_green\">How To PDF</a>
                        <a href=\"/wp-content/uploads/2019/08/201906-5-Minute-Website-CodeJIKA.com-Workshop-v6.pdf\" class=\"link color_red\" download >Download PDF</a>
                     </div>
                  </div>
               </div>
               <div class=\"col-lg-12 col-md-12\">
                  <div class=\"inside-col\">
                     <h4>Project 1</h4>
                     <h6>Instead of watching a movie...</h6>
                     <div class=\"table-content\">
                        <div class=\"fst\">
                           <p>HTML & CSS Basics</p>
                           <p>4 Trainings</p>
                        </div>
                        <div class=\"scnd\">
                           <p>Level: Beginner</p>
                           <p>Time: 90 Mins</p>
                        </div>
                     </div>
                     <div class=\"foot-links\">
                        <a href=\"/curriculum\" class=\"link btn-gray\">Learn More</a>
                        <a href=\"/wp-content/uploads/2019/09/201908-Intro-Guide-CodeJIKA.com-v2.pdf\" class=\"link color_green\" download>Intro Guide</a>
                        <a href=\"/downloads/201908 PROJECT 1 - CodeJIKA.com DT v2.pdf\" class=\"link color_red \" download >Download PDF</a>
                     </div>
                  </div>
               </div>
            </div>
            <div class=\"row\">
               <div class=\"col-lg-12 col-md-12\">
                  <div class=\"inside-col\">
                     <h4>Project 2</h4>
                     <h6>Mind blowing</h6>
                     <div class=\"table-content\">
                        <div class=\"fst\">
                           <p>Responsive & Forms</p>
                           <p>5 Trainings</p>
                        </div>
                        <div class=\"scnd\">
                           <p>Level: Beginner</p>
                           <p>Time: 150 Mins</p>
                        </div>
                     </div>
                     <div class=\"foot-links\">
                        <a href=\"/curriculum\" class=\"link btn-gray\">Learn More</a>
                        <a href=\"/wp-content/uploads/2019/08/201903-PROJECT-2-CodeJIKA.com-DT-v2.pdf\" class=\"link color_red separate-it\" download >Download PDF</a>
                     </div>
                  </div>
               </div>
               <div class=\"col-lg-12 col-md-12\">
                  <div class=\"inside-col\">
                     <h4>Project 3</h4>
                     <h6>You would not believe it.</h6>
                     <div class=\"table-content\">
                        <div class=\"fst\">
                           <p>Responsive & Forms</p>
                           <p>6 Trainings</p>
                        </div>
                        <div class=\"scnd\">
                           <p>Level: Beginner</p>
                           <p>Time: 210 Mins</p>
                        </div>
                     </div>
                     <div class=\"foot-links\">
                        <a href=\"/curriculum\" class=\"link btn-gray\">Learn More</a>
                        <a href=\"/wp-content/uploads/2019/07/201903-PROJECT-3-CodeJIKA.com-DT-v1.pdf\" class=\"link color_red separate-it\" download>Download PDF</a>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>";

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
  <meta name="google-site-verification" content="SxN62DDQ5qWO9qlDiyiqQBj72i1hGxOukG8sqIDTZVo" />
  <meta name="google-site-verification" content="Cv8eoG1XMqtBWl5jJasnizpoNzQgThpzSBwynXUId0c" />
  <link rel="icon" type="image/ico" href="<?php echo $home_url;?>/assets/img/favicon.ico">
  <link rel="canonical" href="/" />
  <link rel="alternate" media="handheld" href="/?desktop" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title><?php echo $home_url;?></title>
  <meta name="title" content="<?php echo $lang_meta_title;?>">
  <meta name="description" content="<?php echo $lang_meta_description;?>">
  <meta name="keywords" content="<?php echo $lang_meta_keywords;?>">
  
  <!-- Google / Search Engine Tags -->
  <meta itemprop="name" content="Rock Your World –Learn to code, free, offline & with your creative flare">
  <meta itemprop="description" content="<?php echo $lang_meta_description;?>">
  <meta itemprop="image" content="http://codejika.com/img/201908_cj_OG_girl_fb.jpg">

  <!-- Facebook Meta Tags -->
  <meta property="og:title" content="Rock Your World – Learn to code, free, offline & with your creative flare">
  <meta property="og:site_name" content="codejika">
  <meta property="og:url" content="/">
  <meta property="og:description" content="code on your phone, offline or in-class, but always have fun">
  <meta property="og:type" content="website">
  <meta property="og:image" content="/img/201908_cj_OG_girl_fb.jpg">
  <meta property="og:image:url" content="/img/201908_cj_OG_girl_fb.jpg" />
  
  <!-- Twitter Meta Tags -->
  <meta name="twitter:card" content="summary" />
  <meta name="twitter:site" content="@Codejika" />
  <meta name="twitter:creator" content="@Codejika" />
  <meta name="twitter:title" content="Rock Your World –Learn to code, free, offline & with your creative flare" />
  <meta name="twitter:description" content="Code on your phone, offline or in-class, but always have fun. Frontend Web Development for awesome teens in Africa, South America or Anywhere." />
  <meta name="twitter:url" content="https://codejika.com" />
  <meta name="twitter:image" content="/img/201908_cj_OG_girl_tw.jpg" />
  <meta name="twitter:image:alt" content="Codejika" />
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
      
  <!--link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" type="text/css" /-->
  <!--link href="https://fonts.googleapis.com/css?family=Rajdhani:500" rel="stylesheet" type="text/css" /-->
  <!--link href="<?php echo $home_url;?>/css/custom-bootstrap.css" rel="stylesheet" type="text/css" /-->
  <!--link rel="stylesheet" href="<?php echo $home_url;?>/css/swiper.min.css"-->
	<!--meta name="robots" content="noindex, nofollow"-->
  <!--link href="<?php echo $home_url;?>/css/theme/base16-dark.css" rel='stylesheet' type="text/css" /-->
  <script type="text/javascript">
  //<![CDATA[
  var randEnabled = true;

  function randText() {
    			var text = "";
    if (randEnabled) {
			var text = "?";
				var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

				for (var i = 0; i < 5; i++)
					text += possible.charAt(Math.floor(Math.random() * possible.length));
        } 
			return text;
        
		}

  document.write("<link rel='stylesheet' type='text/css' href='<?php echo $home_url;?>/css/mobile-homepage-swiper.css" + randText() + "'\/>");


  //]]>
  </script>
  
<!--link rel="stylesheet" href="<?php echo $home_url;?>/css/home.css"-->

<style> 
  h1, h2 {
  	font-size: 60px;
  	line-height: 54px;
  	font-weight: normal;
  	font-family: 'Rajdhani', sans-serif;
  	color: #fff;
  	margin: 0;
  	padding: 0;
  	text-transform: none;
  }
  h2 {} .container {
  	text-align: center;
  }
  .swiper-pagination-bullet {
  	background: #444;
  	opacity: .9;
  }
  .swiper-container-vertical>.swiper-pagination-bullets .swiper-pagination-bullet {
  	margin: 5px 0px 5px 2px;
  }
  .swiper-pagination-bullet {
  	width: 5px;
  	height: 5px;
  	display: inline-block;
  	border-radius: 100%;
  	background: #000;
  	opacity: .2;
  }
  .swiper-pagination-bullet {
  	background: #444;
  	opacity: 1;
  }
  .swiper-pagination-bullet-active {
  	opacity: 1!important;
  	background: #fff!important;
  	border: 1px solid #444;
  	width: 9px;
  	height: 9px;
  	margin: 5px 0px 5px 0px!important;
  }
  .bounce {
  	-moz-animation: bounce 3s infinite;
  	-webkit-animation: bounce 3s infinite;
  	animation: bounce 3s infinite;
  }
  @-moz-keyframes bounce {
  	0%, 20%, 50%, 80%, 100% {
  		-moz-transform: translateY(0);
  		transform: translateY(0);
  	}
  	40% {
  		-moz-transform: translateY(-30px);
  		transform: translateY(-30px);
  	}
  	60% {
  		-moz-transform: translateY(-15px);
  		transform: translateY(-15px);
  	}
  }
  @-webkit-keyframes bounce {
  	0%, 20%, 50%, 80%, 100% {
  		-webkit-transform: translateY(0);
  		transform: translateY(0);
  	}
  	40% {
  		-webkit-transform: translateY(-20px);
  		transform: translateY(-20px);
  	}
  	60% {
  		-webkit-transform: translateY(-12px);
  		transform: translateY(-12px);
  	}
  }
  @keyframes bounce {
  	0%, 20%, 50%, 80%, 100% {
  		-moz-transform: translateY(0);
  		-ms-transform: translateY(0);
  		-webkit-transform: translateY(0);
  		transform: translateY(0);
  	}
  	40% {
  		-moz-transform: translateY(-10px);
  		-ms-transform: translateY(-10px);
  		-webkit-transform: translateY(-10px);
  		transform: translateY(-10px);
  	}
  	60% {
  		-moz-transform: translateY(-8px);
  		-ms-transform: translateY(-8px);
  		-webkit-transform: translateY(-8px);
  		transform: translateY(-8px);
  	}
  }
  #header {
  	position: fixed;
  	height: 32px;
  	display: block;
  	width: 100%;
  	background: none;
  	z-index: 1000;
  	color: #f2f2f2;
  	padding: 0;
  	top: 0;
  	text-align: left;
  }
  #logo {
  	margin-bottom: 0;
  	padding: 14px 0 0 14px!important;
  	height: 28px;
  	width: 80px;
  	height: auto;
  }
  .embed-nav li.active {
  	background: none;
  	color: #000;
  }
  .embed-nav li.active a:after {
  	display: none;
  }
  .fullscreen .modal-dialog {
  	width: 100%;
  	max-width: 100%;
  	height: 100%;
  	max-height: 100%;
  	margin: 0;
  }
  .modal.show .modal-dialog {
  	top: auto;
  	margin: 0!important;
  }
  .modal-content button.close {
  	font-weight: normal;
  }
  .close {
  	float: right;
  	font-size: 1.5rem;
  	font-weight: 700;
  	line-height: 1;
  	color: #000;
  	text-shadow: 0 1px 0 #fff;
  	opacity: .5
  }
  .close:not(:disabled):not(.disabled) {
  	cursor: pointer
  }
  .close:not(:disabled):not(.disabled):hover,
  .close:not(:disabled):not(.disabled):focus {
  	color: #000;
  	text-decoration: none;
  	opacity: .75
  }
  button.close {
  	padding: 0;
  	background-color: transparent;
  	border: 0;
  	-webkit-appearance: none
  }
  .modal-open {
  	overflow: hidden
  }
  .modal {
  	position: fixed;
  	top: 0;
  	right: 0;
  	bottom: 0;
  	left: 0;
  	z-index: 1050;
  	display: none;
  	overflow: hidden;
  	outline: 0
  }
  .modal-open .modal {
  	overflow-x: hidden;
  	overflow-y: auto
  }
  .modal-dialog {
  	position: relative;
  	width: auto;
  	margXin: .5rem;
  	pointer-events: none
  }
  .modal.fade .modal-dialog {
  	transition: transform .3s ease-out;
  	transform: translate(0, -25%)
  }
  @media screen and (prefers-reduced-motion: reduce) {
  	.modal.fade .modal-dialog {
  		transition: none
  	}
  }
  .modal.show .modal-dialog {
  	transform: translate(0, 0)
  }
  .modal-dialog-centered {
  	display: flex;
  	align-items: center;
  	min-height: calc(100% -(.5rem * 2))
  }
  .modal-content {
  	position: relative;
  	display: flex;
  	flex-direction: column;
  	width: 100%;
  	pointer-events: auto;
  	background-color: #fff;
  	background-clip: padding-box;
  	border: 1px solid rgba(0, 0, 0, 0.2);
  	border-radius: .3rem;
  	outline: 0
  }
  .modal-backdrop {
  	position: fixed;
  	top: 0;
  	right: 0;
  	bottom: 0;
  	left: 0;
  	z-index: 1040;
  	background-color: #000
  }
  .modal-backdrop.fade {
  	opacity: 0
  }
  .modal-backdrop.show {
  	opacity: .5
  }
  .modal-header {
  	display: flex;
  	align-items: flex-start;
  	justify-content: space-between;
  	padding: 1rem;
  	border-bottom: 1px solid #e9ecef;
  	border-top-left-radius: .3rem;
  	border-top-right-radius: .3rem
  }
  .modal-header .close {
  	padding: 1rem;
  	margin: -1rem -1rem -1rem auto
  }
  .modal-title {
  	margin-bottom: 0;
  	line-height: 1.5
  }
  .modal-body {
  	position: relative;
  	flex: 1 1 auto;
  	padding: 1rem
  }
  .modal-footer {
  	display: flex;
  	align-items: center;
  	justify-content: flex-end;
  	padding: 1rem;
  	border-top: 1px solid #e9ecef
  }
  .modal-footer>:not(:first-child) {
  	margin-left: .25rem
  }
  .modal-footer>:not(:last-child) {
  	margin-right: .25rem
  }
  .modal-scrollbar-measure {
  	position: absolute;
  	top: -9999px;
  	width: 50px;
  	height: 50px;
  	overflow: scroll
  }
  .fullscreen .modal-dialog {
  	width: 100%;
  	max-width: 100%;
  	height: 100%;
  	max-height: 100%;
  	margin: 0;
  }
  box-menu-wraper.x-box .bxmenu-wrap {
  	max-width: 810px;
  	padding-bottom: 40px;
  }
  @media screen and (min-width: 1300px) and (max-width: 1500px) {
  	.bxmenu-wrap {
  		padding-top: 50px;
  	}
  }
  @media screen and (min-width: 1024px) and (max-width: 1299px) {
  	.bxmenu-wrap {
  		padding-top: 30px;
  	}
  	.bxmenu-wrap .col-lg-12 {
  		padding: 9px 15px;
  	}
  }
  @media(min-width: 992px) {
  	.col-lg-12 {
  		flex: 0 0 50%;
  		max-width: 50%
  	}
  }
  .embed-nav a {
  	text-decoration: none;
  	position: relative;
  	Xline-height: 26px;
  	transition: unset;
  	cursor: pointer;
  	padding: 8px 16px;
  	color: #fff;
  	font-size: 30px;
  }
  .powered-by {
  	padding: 10px 20px;
  	background-color: #444;
  	margin-top: 10px;
  	align-self: flex-start;
  	font-size: 14px!important;
  	;
  }
  .powered-by a {
  	font-size: 14px!important;
  	;
  }
  .footer,
  .footer h4,
  .footer a {
  	color: white;
  	font-size: 18px;
  }
  .footer li {
  	text-align: left!important;
  }
  .footer a:hover {
  	color: #efefef;
  }
  .footer h4 {
  	margin-top: 0px;
  	margin-bottom: 0px;
  	font-family: Rajdhani;
  	font-size: 16px;
  	line-height: initial;
  	text-transform: uppercase;
  	text-align: left!important;
  }
  .footer .container {
  	padding: 20px 36px 20px 36px;
  }
  @font-face {
  	font-family: 'codejika';
  	src: url('<?php echo $home_url;?>/fonts/codejika.eot?jzj9');
  	src: url('<?php echo $home_url;?>/fonts/codejika.eot?jzj9#iefix') format('embedded-opentype'), url('<?php echo $home_url;?>/fonts/codejika.ttf?jzj9') format('truetype'), url('<?php echo $home_url;?>/fonts/codejika.woff?jzj9') format('woff'), url('<?php echo $home_url;?>/fonts/codejika.svg?jzj9#codejika') format('svg');
  	font-weight: normal;
  	font-style: normal;
  }
  i {
  	font-family: 'codejika' !important;
  	speak: none;
  	font-style: normal;
  	font-weight: normal;
  	font-variant: normal;
  	text-transform: none;
  	line-height: 1.2;
  	font-size: 1.5em;
  	vertical-align: -.25em;
  	-webkit-font-smoothing: antialiased;
  	-moz-osx-font-smoothing: grayscale;
  }
  .fs0 {
  	: 16px;
  }
  .fs1 {
  	font-size: 1em;
  }
  .fs2 {
  	font-size: 2em;
  }
  .fs3 {
  	font-size: 3em;
  }
  .fs4 {
  	font-size: 4em;
  }
  .fs5 {
  	font-size: 5em;
  }
  .icon-sentiment_satisfied,
  .icon-arrow_forward {
  	font-size: 1em;
  	vertical-align: -.1em;
  }
  .icon-uniE010:before {
  	content: "\e010";
  }
  .icon-uniE011:before {
  	content: "\e011";
  }
  .icon-account_circle:before {
  	content: "\e853";
  }
  .icon-arrow_forward:before {
  	content: "\e5c8";
  }
  .icon-assignment:before {
  	content: "\e85d";
  }
  .icon-beenhere:before {
  	content: "\e52d";
  }
  .icon-check_circle:before {
  	content: "\e86c";
  }
  .icon-close1:before {
  	content: "\e5cd";
  }
  .icon-code1:before {
  	content: "\e86f";
  }
  .icon-comment:before {
  	content: "\e0b9";
  }
  .icon-dehaze:before {
  	content: "\e3c7";
  }
  .icon-desktop_mac:before {
  	content: "\e30b";
  }
  .icon-face:before {
  	content: "\e87c";
  }
  .icon-home:before {
  	content: "\e88a";
  }
  .icon-lock:before {
  	content: "\e897";
  }
  .icon-import_contacts:before {
  	content: "\e0e0";
  }
  .icon-local_library:before {
  	content: "\e54b";
  }
  .icon-lock_open:before {
  	content: "\e898";
  }
  .icon-lock_outline:before {
  	content: "\e899";
  }
  .icon-personal_video:before {
  	content: "\e63b";
  }
  .icon-replay:before {
  	content: "\e042";
  }
  .icon-rotate_left:before {
  	content: "\e419";
  }
  .icon-school:before {
  	content: "\e80c";
  }
  .icon-sentiment_satisfied:before {
  	content: "\e813";
  }
  .icon-settings_ethernet:before {
  	content: "\e8be";
  }
  .icon-view_list:before {
  	content: "\e8ef";
  }
  .icon-th-list:before {
  	content: "\f00b";
  }
  .icon-check:before {
  	content: "\f00c";
  }
  .icon-close:before {
  	content: "\f00d";
  }
  .icon-remove:before {
  	content: "\f00d";
  }
  .icon-times:before {
  	content: "\f00d";
  }
  .icon-repeat:before {
  	content: "\f01e";
  }
  .icon-rotate-right:before {
  	content: "\f01e";
  }
  .icon-twitter-square:before {
  	content: "\f081";
  }
  .icon-facebook-square:before {
  	content: "\f082";
  }
  .icon-linkedin-square:before {
  	content: "\f08c";
  }
  .icon-bars:before {
  	content: "\f0c9";
  }
  .icon-navicon:before {
  	content: "\f0c9";
  }
  .icon-reorder:before {
  	content: "\f0c9";
  }
  .icon-linkedin:before {
  	content: "\f0e1";
  }
  .icon-rotate-left:before {
  	content: "\f0e2";
  }
  .icon-undo:before {
  	content: "\f0e2";
  }
  .icon-angle-up:before {
  	content: "\f106";
  }
  .icon-angle-down:before {
  	content: "\f107";
  }
  .icon-smile-o:before {
  	content: "\f118";
  }
  .icon-code:before {
  	content: "\f121";
  }
  .icon-youtube-square:before {
  	content: "\f166";
  }
  .icon-youtube:before {
  	content: "\f167";
  }
  .icon-youtube-play:before {
  	content: "\f16a";
  }
  .icon-instagram:before {
  	content: "\f16d";
  }
  .icon-tumblr-square:before {
  	content: "\f174";
  }
  .icon-facebook-official:before {
  	content: "\f230";
  }
  .icon-hand-grab-o:before {
  	content: "\f255";
  }
  .icon-hand-rock-o:before {
  	content: "\f255";
  }
  .icon-hand-pointer-o:before {
  	content: "\f25a";
  }
  .icon-television:before {
  	content: "\f26c";
  }
  .icon-tv:before {
  	content: "\f26c";
  }
  .icon-commenting:before {
  	content: "\f27a";
  }
  .icon-user-circle:before {
  	content: "\f2bd";
  }
  
  .FAQ .card {
  	background-color: inherit;
  	border: 0;
  	border-radius: 0;
  }   
  .FAQ .card-header {
  	background-color: inherit;
    padding: 4px 0px;
  }   
  .FAQ .card-header button {

    padding: 0;
      	border: 0;
  } 
  .FAQ .card .card-body {
  	background-color: #444;
  	min-height: 80px;
  	min-height: 80px;
  	padding: 10px;
    text-align: left;
  } 
  .FAQ .card .card-body ol {
    padding-left: 20px;
    margin-bottom: 0;
  }
  .FAQ .card .btn {
    padding-right: 40px;
        font-size: 15px;
  }
  .card {
  	background-color: #555555;
  	border: inherit;
  	border-radius: 0;
  }
  .card h4 {
  	font-weight: normal;
  }
  .card .card-body {
  	background-color: #444;
  	min-height: 80px;
  	min-height: 80px;
  	padding: .6rem 1.2rem;
  }
  .card .card-body li {
  	padding-right: 5px;
  	padding-left: 5px;
  }
  .card .card-body li a {
  	font-size: 14px;
  }
  .card .card-body li a:hover {
  	color: #ddd;
  	text-decoration: none;
  }
  .card .btn {
    white-space: unset;
  }
  .footer .btn-link {
  	font-weight: inherit;
  	color: #fff;
  	background-color: transparent;
  	width: 100%;
  	text-align: left
  }
  .card-header {
  	padding: 4px 10px;
  }
  .btn:not(:disabled):not(.disabled) {
  	text-decoration: none;
  }
  .switch-mode {
  	border: 1px solid #eee;
  	padding: 6px;
  	border-radius: 6px;
  	margin-right: 20px;
  }
  .copyright-footer .copyright {
  	margin: 6px 0px;
  	padding: 6px 0;
  	text-align: center;
  	float: none;
  	font-size: 12px;
  }
  .copyright-footer,
  .copyright-footer a {
  	font-size: 12px;
  }
  .copyright-footer.col-md-24.pl-1.pr-1.pl-sm-2.pr-sm-2 {
  	position: absolute;
  	bottom: 0;
  	left: 0;
  }
  .btn-primary {
  	font-size: 26px;
  	background-color: #353535;
  	line-height: 26px;
  	color: #fff;
  	padding: 16px 16px;
  	font-weight: normal;
  	text-transform: inherit;
    width: 80%;
  }
  .btn-primary:hover {
  	background-color: #007bff!important;
  }
.grey-btn {
    width: 60%!important;
  }
  .grey-btn:hover {
    background-color:#3b3a3a!important;
    border-color: #fff;
  }
  .footer-menu a {
  	font-size: 16px;
  } 
  .fullscreen button.close {
    position: sticky;
    right: 0px!important;
    top: 0px;
    outline: 0;
    z-index: 1000;
    line-height: 40px;
    font-weight: normal;
    font-size: 6em;
    font-family: rajdhani;
    background-color: #fff;
    display: block;
    padding: 4px;
    opacity: 1;
    height: 42px;
    width: 50px;
    transform: unset;
  }
  .fullscreen .modal-content {
    border: 0px;    top: -15px;
  }
  
  .fullscreen .container:not(#curriculum_section), .fullscreen .container:not(#curriculum_section) .bxmenu-wrap{
    padding-top: 0;

}

.fullscreen h3, .fullscreen h4 {
    text-transform: inherit;
    font-weight: normal;
}
  .FAQ #accordion .btn>.icon {
    top: 3px;
    right: 6px;
  }
  #accordion .btn>.icon,  #accordion1 .btn>.icon {
  	transform: rotate(0deg);
  	Xtransition: .3s transform ease-in-out;
  	display: inline-block;
  	position: absolute;
  	z-index: 500;
  	right: 16px;
  	color: #fff;
  }
  #accordion .btn[aria-expanded="true"]>.icon, #accordion1 .btn[aria-expanded="true"]>.icon {
  	transform: rotate(-180deg);
  }
  #slide4aX {
    
/* Permalink - use to edit and share this gradient: https://colorzilla.com/gradient-editor/#c63da6+0,c63da6+60,960066+86,960066+100 */
background: #c63da6; /* Old browsers */
background: -moz-linear-gradient(top,  #c63da6 0%, #c63da6 60%, #960066 86%, #960066 100%); /* FF3.6-15 */
background: -webkit-linear-gradient(top,  #c63da6 0%,#c63da6 60%,#960066 86%,#960066 100%); /* Chrome10-25,Safari5.1-6 */
background: linear-gradient(to bottom,  #c63da6 0%,#c63da6 60%,#960066 86%,#960066 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#c63da6', endColorstr='#960066',GradientType=0 ); /* IE6-9 */
  }
  #slide1 {
background-color: #3bceac;
background-image: url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' x='32' y='0' width='80' height='80' viewBox='0 0 80 80'%3E%3Cg fill='%234af2cb' fill-opacity='0.31'%3E%3Cpath fill-rule='evenodd' d='M11 0l5 20H6l5-20zm42 31a3 3 0 1 0 0-6 3 3 0 0 0 0 6zM0 72h40v4H0v-4zm0-8h31v4H0v-4zm20-16h20v4H20v-4zM0 56h40v4H0v-4zm63-25a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm10 0a3 3 0 1 0 0-6 3 3 0 0 0 0 6zM53 41a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm10 0a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm10 0a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm-30 0a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm-28-8a5 5 0 0 0-10 0h10zm10 0a5 5 0 0 1-10 0h10zM56 5a5 5 0 0 0-10 0h10zm10 0a5 5 0 0 1-10 0h10zm-3 46a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm10 0a3 3 0 1 0 0-6 3 3 0 0 0 0 6zM21 0l5 20H16l5-20zm43 64v-4h-4v4h-4v4h4v4h4v-4h4v-4h-4zM36 13h4v4h-4v-4zm4 4h4v4h-4v-4zm-4 4h4v4h-4v-4zm8-8h4v4h-4v-4z'/%3E%3C/g%3E%3C/svg%3E");
background-position: 10px 10px;
background-size: 125%;

  }
  #slide2 {
background-color: #ee4e3a;
background-image: url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='80' height='80' viewBox='0 0 80 80'%3E%3Cg fill='%23f27f4d' fill-opacity='0.31'%3E%3Cpath fill-rule='evenodd' d='M11 0l5 20H6l5-20zm42 31a3 3 0 1 0 0-6 3 3 0 0 0 0 6zM0 72h40v4H0v-4zm0-8h31v4H0v-4zm20-16h20v4H20v-4zM0 56h40v4H0v-4zm63-25a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm10 0a3 3 0 1 0 0-6 3 3 0 0 0 0 6zM53 41a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm10 0a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm10 0a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm-30 0a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm-28-8a5 5 0 0 0-10 0h10zm10 0a5 5 0 0 1-10 0h10zM56 5a5 5 0 0 0-10 0h10zm10 0a5 5 0 0 1-10 0h10zm-3 46a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm10 0a3 3 0 1 0 0-6 3 3 0 0 0 0 6zM21 0l5 20H16l5-20zm43 64v-4h-4v4h-4v4h4v4h4v-4h4v-4h-4zM36 13h4v4h-4v-4zm4 4h4v4h-4v-4zm-4 4h4v4h-4v-4zm8-8h4v4h-4v-4z'/%3E%3C/g%3E%3C/svg%3E");
background-position: 150px 40px;
background-size: 85%;

  }
  #slide3 {
background-color: #0ead69;
background-image: url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='80' height='80' viewBox='0 0 80 80'%3E%3Cg fill='%2323cd84' fill-opacity='0.31'%3E%3Cpath fill-rule='evenodd' d='M11 0l5 20H6l5-20zm42 31a3 3 0 1 0 0-6 3 3 0 0 0 0 6zM0 72h40v4H0v-4zm0-8h31v4H0v-4zm20-16h20v4H20v-4zM0 56h40v4H0v-4zm63-25a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm10 0a3 3 0 1 0 0-6 3 3 0 0 0 0 6zM53 41a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm10 0a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm10 0a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm-30 0a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm-28-8a5 5 0 0 0-10 0h10zm10 0a5 5 0 0 1-10 0h10zM56 5a5 5 0 0 0-10 0h10zm10 0a5 5 0 0 1-10 0h10zm-3 46a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm10 0a3 3 0 1 0 0-6 3 3 0 0 0 0 6zM21 0l5 20H16l5-20zm43 64v-4h-4v4h-4v4h4v4h4v-4h4v-4h-4zM36 13h4v4h-4v-4zm4 4h4v4h-4v-4zm-4 4h4v4h-4v-4zm8-8h4v4h-4v-4z'/%3E%3C/g%3E%3C/svg%3E");
background-position: 20px 60px;
background-size: 115%;
  }
  #slide4 {
background-color: #f9a743;
background-image: url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='80' height='80' viewBox='0 0 80 80'%3E%3Cg fill='%23f6c15d' fill-opacity='0.31'%3E%3Cpath fill-rule='evenodd' d='M11 0l5 20H6l5-20zm42 31a3 3 0 1 0 0-6 3 3 0 0 0 0 6zM0 72h40v4H0v-4zm0-8h31v4H0v-4zm20-16h20v4H20v-4zM0 56h40v4H0v-4zm63-25a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm10 0a3 3 0 1 0 0-6 3 3 0 0 0 0 6zM53 41a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm10 0a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm10 0a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm-30 0a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm-28-8a5 5 0 0 0-10 0h10zm10 0a5 5 0 0 1-10 0h10zM56 5a5 5 0 0 0-10 0h10zm10 0a5 5 0 0 1-10 0h10zm-3 46a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm10 0a3 3 0 1 0 0-6 3 3 0 0 0 0 6zM21 0l5 20H16l5-20zm43 64v-4h-4v4h-4v4h4v4h4v-4h4v-4h-4zM36 13h4v4h-4v-4zm4 4h4v4h-4v-4zm-4 4h4v4h-4v-4zm8-8h4v4h-4v-4z'/%3E%3C/g%3E%3C/svg%3E");
background-position: 100px 10px;
background-size: 75%;
  }
  #slide4a {
background-color: #d23152;
background-image: url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='80' height='80' viewBox='0 0 80 80'%3E%3Cg fill='%23ee4266' fill-opacity='0.4'%3E%3Cpath fill-rule='evenodd' d='M11 0l5 20H6l5-20zm42 31a3 3 0 1 0 0-6 3 3 0 0 0 0 6zM0 72h40v4H0v-4zm0-8h31v4H0v-4zm20-16h20v4H20v-4zM0 56h40v4H0v-4zm63-25a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm10 0a3 3 0 1 0 0-6 3 3 0 0 0 0 6zM53 41a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm10 0a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm10 0a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm-30 0a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm-28-8a5 5 0 0 0-10 0h10zm10 0a5 5 0 0 1-10 0h10zM56 5a5 5 0 0 0-10 0h10zm10 0a5 5 0 0 1-10 0h10zm-3 46a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm10 0a3 3 0 1 0 0-6 3 3 0 0 0 0 6zM21 0l5 20H16l5-20zm43 64v-4h-4v4h-4v4h4v4h4v-4h4v-4h-4zM36 13h4v4h-4v-4zm4 4h4v4h-4v-4zm-4 4h4v4h-4v-4zm8-8h4v4h-4v-4z'/%3E%3C/g%3E%3C/svg%3E");
background-position: 10px 30px;
background-size: 135%;
  }
  #slide5, #slide7 {
background-color: #333;


  }
 #slide6, #slide8 {
background-color: #333333;
background-image: url("data:image/svg+xml,%3Csvg width='100' height='20' viewBox='0 0 100 20' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath d='M21.184 20c.357-.13.72-.264 1.088-.402l1.768-.661C33.64 15.347 39.647 14 50 14c10.271 0 15.362 1.222 24.629 4.928.955.383 1.869.74 2.75 1.072h6.225c-2.51-.73-5.139-1.691-8.233-2.928C65.888 13.278 60.562 12 50 12c-10.626 0-16.855 1.397-26.66 5.063l-1.767.662c-2.475.923-4.66 1.674-6.724 2.275h6.335zm0-20C13.258 2.892 8.077 4 0 4V2c5.744 0 9.951-.574 14.85-2h6.334zM77.38 0C85.239 2.966 90.502 4 100 4V2c-6.842 0-11.386-.542-16.396-2h-6.225zM0 14c8.44 0 13.718-1.21 22.272-4.402l1.768-.661C33.64 5.347 39.647 4 50 4c10.271 0 15.362 1.222 24.629 4.928C84.112 12.722 89.438 14 100 14v-2c-10.271 0-15.362-1.222-24.629-4.928C65.888 3.278 60.562 2 50 2 39.374 2 33.145 3.397 23.34 7.063l-1.767.662C13.223 10.84 8.163 12 0 12v2z' fill='%2327413e' fill-opacity='0.4' fill-rule='evenodd'/%3E%3C/svg%3E");
background-size: auto;
  }
  .swiper-container {

  //  height: 100vh;
}
html, body {
    posXition: unset;
    hXeight: 100vh;
}
</style>
</head>

<body>
  
<div id="header" class="award " style="z-index: 1049;position: absolute; left: 0; top: 0; border: 0; width: 100%; height:auto; font-size:12px;background: #7c43cf;ext-align: center;">
          <a style="display: block;text-transform:none;color: #fff;text-align: center;font-size:12px;line-height: 32px;width: 100%; height:100%;padding: 0px 4px;" href="/schools-closed-whats-next" target="_blank">Schools Closed… What's next? Find out more here.</a>
</div>
<div id="header" class="embed-nav ">
	<a class="logo hide" style="padding: 0!important;" >
		<img src="
			<?php echo $home_url;?>/img/jika-logo-white.png" alt="logo" id="logo" style=
  "" />
		</a>
		<ul id="lesson-navbar" class="hide" style="position: absolute; right: 0; top: 0; border: 0;">
			<li id="menu-profile" class="active">
				<a href="#" class="" id="menu-modal" data-toggle="modal" data-target="#menuModal" style="">
					<i class="fas fa-bars"></i>
				</i>
			</a>
		</li>
	</ul>
</div>
<div class="swiper-container swiper-container-v">
	<div class="swiper-wrapper">
		<div class="swiper-slide" id="slide1" style="backgrXound: url(<?php echo $home_url;?>/img/homepage-bg.jpg) no-repeat bottom right #12b9c1; bacXkground-size: 100%; align-items: flex-start;padding-top: 60px;">
			<div class="container">
				<img src="<?php echo $home_url;?>/img/jika-logo-white.png" alt="logo" style="padding-bottom:40px;" />
				<h1>
 <?php echo $lang_h1_seo;?>
  </h1>
				<h2 class="hide" style="padding-top:20px; font-sXize: 36px;">
 <?php echo $lang_h2_seo;?>
  </h2>
				<h2 style="padding-top:50px; font-size: 30px;line-height: initial;text-transform: uppercase;">
 <span  style="color: #efea0f;">It's free</span>, start now
  </h2>
			</div>
			<div class="arrow" style="opacity: 1;position: absolute;
  z-index: 106;bottom: 20px;left: 50%;cursor: pointer;opaciXty: 0; color: white;text-transform: uppercase;font-size: 10px;letter-spacing: 0.4em;">
				<div class="align-middle" style="left: -50%;
  position: relative;
  text-align: center">
					<img src="	<?php echo $home_url;?>/img/up-scroll.png" class="bounce">
					<div id="scrollTitle" style="padding-top: 6px;">SWIPE UP</div>
				</div>
			</div>
		</div>
		<div class="swiper-slide" id="slide2" style="backXground: url(<?php echo $home_url;?>/img/homepage-bg2.jpg) no-repeat bottom right #ee4e3a; bacXkground-size: 100%; alignX-items: flex-start; pXadding-top: 90px;">
			<div class="container">
				<h2>Your coding journey has just begun</h2>
			</div>
		</div>
		<div class="swiper-slide" id="slide3" style="bacXkground: url(<?php echo $home_url;?>/img/homepage-bg4.jpg) no-repeat bottom right #66b943;  bacXkground-size: 100%; alignX-items: flex-start;padXding-top: 90px;">
			<div class="container">
				<h2>Start with the 5-Minute-Website</h2>
			
			</div>
		</div>

		<div class="swiper-slide" id="slide4" style="backgrXound-color: #f9a743; backgXround: url(<?php echo $home_url;?>/img/homepage-bg3.jpg) no-repeat bottom right #f9a743;bacXkground-size: 100%;aXlign-items: flex-start;padding-top: 50px;">
			<div class="container">
				<h2 style="padXding-top: 60px;padding-bottom:0px;fontXs-size: 50px;lineXheight: 40px;">Time to get coding!</h2>
				<a id="start5MW" href="/learn/5-minute-website" class="track-click btn btn-primary mb-3" style="background-color: #2196F3;    margin-top: 150px;     padding: 16px 40px;" data-label="Start 5MW" data-category="CJ.com Mobile Homepage" data-action="Button Clicked">Start NOW!	 
<div class="hide" style="line-height: normal;font-size: 13px;  padding-top: 60px;">( It really only takes 5 mins. )</div>
 </a>
				<a href="/learn/projects" class="track-click btn btn-primary mb-1 grey-btn" style="font-size: 16px;    padding: 8px 16px;    margin-top: 26px;" data-label="See Course" data-category="CJ.co.za Mobile Homepage" data-action="Button Clicked">See the course</a>
<div style="line-height: normal;font-size: 13px;  padding-top: 0px;" class="mb-3">( The whole curriculum. )</div>



			</div>
		</div>
		<div class="swiper-slide" id="slide4a" style="backgroXund-color: #5f0e71; baXckground: url(<?php echo $home_url;?>/img/homepage-bg3.jpg) no-repeat bottom right #f9a743;bacXkground-size: 100%;aXlign-items: flex-start;padding-top: 20px;">
			<div class="container">
				<h2 style="padXding-top: 60px;padding-bottom:0px;fontXs-size: 50px;lineXheight: 40px;">Share with 5 friends and get 3 free lessons.	</h2>
				<a id="whatsapp2" class="track-click btn btn-primary mb-3" style="background-color: #2196F3;  color: #fff;   margin-top: 60px;   padding: 16px 40px;" data-label="Invite Friend" data-category="CJ.co.za Mobile Homepage" data-action="Button Clicked">Tell the Squad	 </a>
    			<a id="skip"  class="track-click btn btn-primary mb-1 grey-btn" style=" font-size: 16px; color: #fff;    padding: 8px 16px;
       margin: 16px 10px 0px 10px;" data-label="Skip" data-category="CJ.co.za Mobile Homepage" data-action="Button Clicked">Skip</a>
			</div>
		</div>
		<div class="footer swiper-slide FAQ" id="slide5" style=" backXground-color: rgb(51, 51, 51); color:#0;bacXkground-size: 100%;align-items: center;padding-top: 0px;">
			<div class="container">
				<div class="row">
					<div class="col-24 col-md-8 offset-md-8 " style="">
						<div class="row">
							<div class=" col-24" style="">
								<h4 style="margin-bottom: 20px;">FAQs: Freakin' Awesome Questions</h4>
								<div id="accordion">
									<div class="card">
										<div class="card-header" id="headingFAQWhatCJ">
											<h4 class="mb-0">
                      <button class="btn btn-link" data-toggle="collapse" data-target="#collapseFAQWhatCJ" aria-expanded="false" aria-controls="collapseFAQWhatCJ">
                        What is CodeJika?
                      <span class="icon">
                      <svg version="1.1" id=""
                        xmlns="http://www.w3.org/2000/svg"
                        xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"width="16px" height="16px" fill="#fff" viewBox="0 0 451.847 451.847" style="enable-background:new 0 0 451.847 451.847;"xml:space="preserve">
                        <g><path d="M225.923,354.706c-8.098,0-16.195-3.092-22.369-9.263L9.27,151.157c-12.359-12.359-12.359-32.397,0-44.751  c12.354-12.354,32.388-12.354,44.748,0l171.905,171.915l171.906-171.909c12.359-12.354,32.391-12.354,44.744,0 c12.365,12.354,12.365,32.392,0,44.751L248.292,345.449C242.115,351.621,234.018,354.706,225.923,354.706z"/></g>
                      </svg>
                      </span>
                      </button>
                      </h4>
										</div>
										<div id="collapseFAQWhatCJ" class="collapse" aria-labelledby="headingFAQWhatCJ" data-parent="#accordion">
											<div class="card-body">
                        <p>A vibrant eco-system of student-run coding clubs in secondary schools.</p>
                        <p class="mb-0">Operationally it is divided into 3 Pillars:</p>
                        <ol class="mb-0">
                          <li>Online (platform and partners), </li>
                          <li>Awareness (media &amp; advocacy) and </li>
                          <li>Hands-on (In-school Clubs &amp; Events)</li>
                        </ol>
											</div>
										</div>
								  </div>
									<div class="card">
										<div class="card-header" id="headingCJMean">
											<h4 class="mb-0">
                      <button class="btn btn-link" data-toggle="collapse" data-target="#collapseCJMean" aria-expanded="false" aria-controls="collapseCJMean">
                        What does JIKA mean?
                      <span class="icon">
                      <svg version="1.1" id=""
                        xmlns="http://www.w3.org/2000/svg"
                        xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"width="16px" height="16px" fill="#fff" viewBox="0 0 451.847 451.847" style="enable-background:new 0 0 451.847 451.847;"xml:space="preserve">
                        <g><path d="M225.923,354.706c-8.098,0-16.195-3.092-22.369-9.263L9.27,151.157c-12.359-12.359-12.359-32.397,0-44.751  c12.354-12.354,32.388-12.354,44.748,0l171.905,171.915l171.906-171.909c12.359-12.354,32.391-12.354,44.744,0 c12.365,12.354,12.365,32.392,0,44.751L248.292,345.449C242.115,351.621,234.018,354.706,225.923,354.706z"/></g>
                      </svg>
                      </span>
                      </button>
                      </h4>
										</div>
										<div id="collapseCJMean" class="collapse" aria-labelledby="headingCJMean" data-parent="#accordion">
											<div class="card-body">
                        <p>PRONOUNCED: CODE–GEE-KA</p>
                        <p class="mb-0">JIKA MEANS “DANCE” OR TURN IN ZULU, A SOUTH AFRICAN LANGUAGE.</p>
											</div>
										</div>
									</div>
									<div class="card">
										<div class="card-header" id="headingLearnCJ">
											<h4 class="mb-0">
                      <button class="btn btn-link" data-toggle="collapse" data-target="#collapseLearnCJ" aria-expanded="false" aria-controls="collapseLearnCJ">
                        What do I learn at CodeJIKA?
                      <span class="icon">
                      <svg version="1.1" id=""
                        xmlns="http://www.w3.org/2000/svg"
                        xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"width="16px" height="16px" fill="#fff" viewBox="0 0 451.847 451.847" style="enable-background:new 0 0 451.847 451.847;"xml:space="preserve">
                        <g><path d="M225.923,354.706c-8.098,0-16.195-3.092-22.369-9.263L9.27,151.157c-12.359-12.359-12.359-32.397,0-44.751  c12.354-12.354,32.388-12.354,44.748,0l171.905,171.915l171.906-171.909c12.359-12.354,32.391-12.354,44.744,0 c12.365,12.354,12.365,32.392,0,44.751L248.292,345.449C242.115,351.621,234.018,354.706,225.923,354.706z"/></g>
                      </svg>
                      </span>
                      </button>
                      </h4>
										</div>
										<div id="collapseLearnCJ" class="collapse" aria-labelledby="headingLearnCJ" data-parent="#accordion">
											<div class="card-body">
                        <p class="mb-0">You learn how to code, starting with "1-Hour-Website" - How to code a simple website using HTML &amp; CSS. Each consecutive project builds on this skill until you can customize and build beautiful business websites for SMEs from scratch – Like a PRO.</p>
											</div>
										</div>
									</div>
									<div class="card">
										<div class="card-header" id="headingHelpCJ">
											<h4 class="mb-0">
                      <button class="btn btn-link" data-toggle="collapse" data-target="#collapseHelpCJ" aria-expanded="false" aria-controls="collapseHelpCJ">
                        How can I help?
                      <span class="icon">
                      <svg version="1.1" id=""
                        xmlns="http://www.w3.org/2000/svg"
                        xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"width="16px" height="16px" fill="#fff" viewBox="0 0 451.847 451.847" style="enable-background:new 0 0 451.847 451.847;"xml:space="preserve">
                        <g><path d="M225.923,354.706c-8.098,0-16.195-3.092-22.369-9.263L9.27,151.157c-12.359-12.359-12.359-32.397,0-44.751  c12.354-12.354,32.388-12.354,44.748,0l171.905,171.915l171.906-171.909c12.359-12.354,32.391-12.354,44.744,0 c12.365,12.354,12.365,32.392,0,44.751L248.292,345.449C242.115,351.621,234.018,354.706,225.923,354.706z"/></g>
                      </svg>
                      </span>
                      </button>
                      </h4>
										</div>
										<div id="collapseHelpCJ" class="collapse" aria-labelledby="headingHelpCJ" data-parent="#accordion">
											<div class="card-body">
                        <p class="mb-0">Run an event or assist schools or other organizations who are.</p>
											</div>
										</div>
									</div>
                  
									<div class="card">
										<div class="card-header" id="headingCJEvent">
											<h4 class="mb-0">
                      <button class="btn btn-link" data-toggle="collapse" data-target="#collapseCJEvent" aria-expanded="false" aria-controls="collapseCJEvent">
                        How do I run a CodeJIKA event?
                      <span class="icon">
                      <svg version="1.1" id=""
                        xmlns="http://www.w3.org/2000/svg"
                        xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"width="16px" height="16px" fill="#fff" viewBox="0 0 451.847 451.847" style="enable-background:new 0 0 451.847 451.847;"xml:space="preserve">
                        <g><path d="M225.923,354.706c-8.098,0-16.195-3.092-22.369-9.263L9.27,151.157c-12.359-12.359-12.359-32.397,0-44.751  c12.354-12.354,32.388-12.354,44.748,0l171.905,171.915l171.906-171.909c12.359-12.354,32.391-12.354,44.744,0 c12.365,12.354,12.365,32.392,0,44.751L248.292,345.449C242.115,351.621,234.018,354.706,225.923,354.706z"/></g>
                      </svg>
                      </span>
                      </button>
                      </h4>
										</div>
										<div id="collapseCJEvent" class="collapse" aria-labelledby="headingCJEvent" data-parent="#accordion">
											<div class="card-body">
                        <p class="mb-0">Easy. Go to the “How to run an event” page here to get started. There you’ll find the curriculum, suggested format, easy to use curriculum and tools to engage volunteers and even fundraise for the event.</p>
											</div>
										</div>
									</div>
                  
                
									<div class="card">
										<div class="card-header" id="headingPrimary">
											<h4 class="mb-0">
                      <button class="btn btn-link" data-toggle="collapse" data-target="#collapsePrimary" aria-expanded="false" aria-controls="collapsePrimary">
                        Do you have a version for primary schools?
                      <span class="icon">
                      <svg version="1.1" id=""
                        xmlns="http://www.w3.org/2000/svg"
                        xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"width="16px" height="16px" fill="#fff" viewBox="0 0 451.847 451.847" style="enable-background:new 0 0 451.847 451.847;"xml:space="preserve">
                        <g><path d="M225.923,354.706c-8.098,0-16.195-3.092-22.369-9.263L9.27,151.157c-12.359-12.359-12.359-32.397,0-44.751  c12.354-12.354,32.388-12.354,44.748,0l171.905,171.915l171.906-171.909c12.359-12.354,32.391-12.354,44.744,0 c12.365,12.354,12.365,32.392,0,44.751L248.292,345.449C242.115,351.621,234.018,354.706,225.923,354.706z"/></g>
                      </svg>
                      </span>
                      </button>
                      </h4>
										</div>
										<div id="collapsePrimary" class="collapse" aria-labelledby="headingPrimary" data-parent="#accordion">
											<div class="card-body">
                        <p class="mb-0">There is no official version for primary schools, but from what we’ve heard the 5th, 6th and 7th graders love the program as well.</p>
											</div>
										</div>
									</div>
                  
									<div class="card hide">
										<div class="card-header" id="headingSecondary">
											<h4 class="mb-0">
                      <button class="btn btn-link" data-toggle="collapse" data-target="#collapseSecondary" aria-expanded="false" aria-controls="collapseSecondary">
                        Can I join if I’m not in secondary school?
                      <span class="icon">
                      <svg version="1.1" id=""
                        xmlns="http://www.w3.org/2000/svg"
                        xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"width="16px" height="16px" fill="#fff" viewBox="0 0 451.847 451.847" style="enable-background:new 0 0 451.847 451.847;"xml:space="preserve">
                        <g><path d="M225.923,354.706c-8.098,0-16.195-3.092-22.369-9.263L9.27,151.157c-12.359-12.359-12.359-32.397,0-44.751  c12.354-12.354,32.388-12.354,44.748,0l171.905,171.915l171.906-171.909c12.359-12.354,32.391-12.354,44.744,0 c12.365,12.354,12.365,32.392,0,44.751L248.292,345.449C242.115,351.621,234.018,354.706,225.923,354.706z"/></g>
                      </svg>
                      </span>
                      </button>
                      </h4>
										</div>
										<div id="collapseSecondary" class="collapse" aria-labelledby="headingSecondary" data-parent="#accordion">
											<div class="card-body">
                        <p class="mb-0">Of course. If you’ve already left school you can join the online CodeJIKA program and can help in organizing events and mentoring. The clubs and competitions are only for secondary school learners at this time.</p>
											</div>
										</div>
									</div>
                  
									<div class="card">
										<div class="card-header" id="headingDifferentHOC">
											<h4 class="mb-0">
                      <button class="btn btn-link" data-toggle="collapse" data-target="#collapseDifferentHOC" aria-expanded="false" aria-controls="collapseDifferentHOC">
                        How is CodeJIKA different than Hour of Code?
                      <span class="icon">
                      <svg version="1.1" id=""
                        xmlns="http://www.w3.org/2000/svg"
                        xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"width="16px" height="16px" fill="#fff" viewBox="0 0 451.847 451.847" style="enable-background:new 0 0 451.847 451.847;"xml:space="preserve">
                        <g><path d="M225.923,354.706c-8.098,0-16.195-3.092-22.369-9.263L9.27,151.157c-12.359-12.359-12.359-32.397,0-44.751  c12.354-12.354,32.388-12.354,44.748,0l171.905,171.915l171.906-171.909c12.359-12.354,32.391-12.354,44.744,0 c12.365,12.354,12.365,32.392,0,44.751L248.292,345.449C242.115,351.621,234.018,354.706,225.923,354.706z"/></g>
                      </svg>
                      </span>
                      </button>
                      </h4>
										</div>
										<div id="collapseDifferentHOC" class="collapse" aria-labelledby="headingDifferentHOC" data-parent="#accordion">
											<div class="card-body">
                        <p class="mb-0">CodeJIKA teaches you how to become a Junior Frontend Web-developer, as fast as possible. It’s about building communites and creating revolutions within the existing educational system, stemming from the student base rather than a teacher-driven approach.</p>
											</div>
										</div>
									</div>
                  
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="footer swiper-slide" id="slide6" style=" bacXkground-color: rgb(56, 56, 61); color:#0;bacXkground-size: 100%;align-items: center;padding-top: 50px;">
			<div class="container">
				<div class="row">
					<div class="col-md-8 offset-md-8" style="alXign-self: flex-end;">
						<div class="row">
							<div class="col-md-24 offsXet-md-1">
								<p style="padding-top:20px; font-size: 1.05rem;">CodeJIKA.com is a vocational coding program for teens & high-schools
									around the World. Our mission is to create awesome experiences, cool online tools and fun curriculum to support the youth of today and leaders of tomorrow.</p>
								<p style="font-size: 1.05rem;">This includes coding clubs, challenges and a "Coding League". CodeJIKA.com is a social impact project driven by Code for Change, supporters &amp; partners.</p>
								<p style="font-sXize: 12px">
									<a href="https://www.facebook.com/codejika/" class="mr-2" target="_blank">	<i class="icon-facebook-official fs2"></i></a>
                  <a href="https://twitter.com/codejika" class="mr-2" target="_blank">	<i class="icon-twitter-square fs2"></i></a>
									<a href="https://www.instagram.com/codejika/" class="mr-2" target="_blank">	<i class="icon-instagram fs2"></i></a>
									<a href="http://www.linkedin.com/company/codejika/" class="mr-2" target="_blank">	<i class="icon-linkedin fs2"></i></a>
									<a href="https://www.youtube.com/channel/UCkmsiNfz3SnmNCUJv4P5YUA" class="" target="_blank">	<i class="icon-youtube-square fs2"></i></a>
								</p>
							</div>
							<div class="col-24 powered-by">
								<div class="row">
									<div class="col-24" style="">
										<h4 class="pb-2">POWERED BY:</h4>
									</div>
								</div>
								<div class="row align-items-center">
									<div class="col-12 " style="text-align: left;">	<a href="https://code4change.co.za/" target="_blank">
<img class="C4C-logo"  src="
<?php echo $home_url;?>/img/C4C-logo-white.png" style="max-width:80px; ">
</a>
									</div>
									<div class="col-12" style="text-align: left;">
										<ul style="padding-left: 0;margin-bottom: 0;">
											<li><a href="https://code4change.co.za/" target="_blank">Learn More</a></li>
											<li><a href="https://code4change.co.za/about" target="_blank">Team</a></li>
											<li><a href="https://code4change.co.za/about/sponsors" target="_blank">Sponsors</a></li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="footer swiper-slide" id="slide7" style=" background-color: rgb(51, 51, 51); color:#0;bacXkground-size: 100%;align-items: center;padding-top: 0px;">
			<div class="container">
				<div class="row">
					<div class="col-22 col-md-8 offset-md-8 offset-1" style="">
						<div class="row">
							<div class=" col-24" style="">
								<h4 style="margin-bottom: 20px;">Where we Work:</h4>
								<div id="accordion1">
									<div class="card">
										<div class="card-header" id="headingWorldwide">
											<h4 class="mb-0">
                      <button class="btn btn-link" data-toggle="collapse" data-target="#collapseWorldwide" aria-expanded="false" aria-controls="collapseWorldwide">Worldwide
                      <span class="icon">
                        <svg version="1.1" id=""
                         xmlns="http://www.w3.org/2000/svg"
                         xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="16px" height="16px" fill="#fff" viewBox="0 0 451.847 451.847" style="enable-background:new 0 0 451.847 451.847;"xml:space="preserve">
                         <g><path d="M225.923,354.706c-8.098,0-16.195-3.092-22.369-9.263L9.27,151.157c-12.359-12.359-12.359-32.397,0-44.751  c12.354-12.354,32.388-12.354,44.748,0l171.905,171.915l171.906-171.909c12.359-12.354,32.391-12.354,44.744,0 c12.365,12.354,12.365,32.392,0,44.751L248.292,345.449C242.115,351.621,234.018,354.706,225.923,354.706z"/></g>
                        </svg>
                      </span>
                      </button>
                      </h4>
										</div>
										<div id="collapseWorldwide" class="collapse" aria-labelledby="headingWorldwide" data-parent="#accordion1">
											<div class="card-body">
												<ul class="">
													<li class="item-language-international ">	<a href="<?php echo $home_url;?>" data-lang="international">International</a></li>
												</ul>
											</div>
										</div>
									</div>
									<div class="card">
										<div class="card-header" id="headingAfrica">
											<h4 class="mb-0">
                      <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseAfrica" aria-expanded="false"aria-controls="collapseAfrica"> Africa
                      <span class="icon">
                        <svg version="1.1" id=""
                         xmlns="http://www.w3.org/2000/svg"
                         xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="16px" height="16px" fill="#fff" viewBox="0 0 451.847 451.847" style="enable-background:new 0 0 451.847 451.847;"xml:space="preserve">
                         <g><path d="M225.923,354.706c-8.098,0-16.195-3.092-22.369-9.263L9.27,151.157c-12.359-12.359-12.359-32.397,0-44.751  c12.354-12.354,32.388-12.354,44.748,0l171.905,171.915l171.906-171.909c12.359-12.354,32.391-12.354,44.744,0 c12.365,12.354,12.365,32.392,0,44.751L248.292,345.449C242.115,351.621,234.018,354.706,225.923,354.706z"/></g>
                        </svg>
                      </span>
                      </button>
                      </h4>
										</div>
										<div id="collapseAfrica" class="collapse" aria-labelledby="headingAfrica" data-parent="#accordion1">
											<div class="card-body">
												<ul class="row" style="margin-bottom: 0;  padding: 4px;list-style-position: inside;">
													<li class="col-12 item-language-botswana ">	<a href="<?php echo $home_url;?>/botswana/" data-lang="botswana">Botswana</a></li>
													<li class="col-12  item-language-namibia">	<a href="<?php echo $home_url;?>/namibia/" data-lang="namibia">Namibia</a></li>
													<li class="col-12 item-language-mozambique ">	<a href="<?php echo $home_url;?>/mozambique/" data-lang="mozambique">Mozambique</a></li>
													<li class="col-12 item-language-zambia">	<a href="<?php echo $home_url;?>/zambia/" data-lang="zambia">Zambia</a></li>
													<li class="col-12 item-language-southafrica">	<a href="<?php echo $home_url;?>/southafrica/" data-lang="southafrica">South Africa<br></a>
														<a style="padding-left: 6px;" href="<?php echo $home_url;?>/johannesburg/" data-lang="johannesburg">&rsaquo;&nbsp;&nbsp;Johannesburg</a></li>
												</ul>
											</div>
										</div>
									</div>
									<div class="card">
										<div class="card-header" id="headingAsia">
											<h4 class="mb-0">
                      <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseAsia" aria-expanded="false" aria-controls="collapseAsia"> Asia 
                      <span class="icon">
                        <svg version="1.1" id=""
                         xmlns="http://www.w3.org/2000/svg"
                         xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="16px" height="16px" fill="#fff" viewBox="0 0 451.847 451.847" style="enable-background:new 0 0 451.847 451.847;"xml:space="preserve">
                         <g><path d="M225.923,354.706c-8.098,0-16.195-3.092-22.369-9.263L9.27,151.157c-12.359-12.359-12.359-32.397,0-44.751  c12.354-12.354,32.388-12.354,44.748,0l171.905,171.915l171.906-171.909c12.359-12.354,32.391-12.354,44.744,0 c12.365,12.354,12.365,32.392,0,44.751L248.292,345.449C242.115,351.621,234.018,354.706,225.923,354.706z"/></g>
                        </svg>
                      </span>
                      </button>
                      </h4>
										</div>
										<div id="collapseAsia" class="collapse" aria-labelledby="headingAsia" data-parent="#accordion1">
											<div class="card-body">
												<ul class="">
													<li class="item-language-palestine ">	<a href="<?php echo $home_url;?>/palestine/" data-lang="palestine">Palestine</a></li>
												</ul>
											</div>
										</div>
									</div>
									<div class="card">
										<div class="card-header" id="headingNorthAmerica">
											<h4 class="mb-0">
                      <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseNorthAmerica" aria-expanded="false" aria-controls="collapseNorthAmerica">North America 
                      <span class="icon">
                        <svg version="1.1" id=""
                         xmlns="http://www.w3.org/2000/svg"
                         xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="16px" height="16px" fill="#fff" viewBox="0 0 451.847 451.847" style="enable-background:new 0 0 451.847 451.847;"xml:space="preserve">
                         <g><path d="M225.923,354.706c-8.098,0-16.195-3.092-22.369-9.263L9.27,151.157c-12.359-12.359-12.359-32.397,0-44.751  c12.354-12.354,32.388-12.354,44.748,0l171.905,171.915l171.906-171.909c12.359-12.354,32.391-12.354,44.744,0 c12.365,12.354,12.365,32.392,0,44.751L248.292,345.449C242.115,351.621,234.018,354.706,225.923,354.706z"/></g>
                        </svg>
                      </span>
                      </button>
                      </h4>
										</div>
										<div id="collapseNorthAmerica" class="collapse" aria-labelledby="headingNorthAmerica" data-parent="#accordion1">
											<div class="card-body">
												<ul class="">
													<li class="item-language-usa ">	<a href="<?php echo $home_url;?>/usa/" data-lang="usa">United States</a>
													</li>
												</ul>
											</div>
										</div>
									</div>
									<div class="card">
										<div class="card-header" id="headingSouthAmerica">
											<h4 class="mb-0">
                      <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSouthAmerica" aria-expanded="false" aria-controls="collapseSouthAmerica">South America 
                      <span class="icon">
                        <svg version="1.1" id=""
                         xmlns="http://www.w3.org/2000/svg"
                         xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="16px" height="16px" fill="#fff" viewBox="0 0 451.847 451.847" style="enable-background:new 0 0 451.847 451.847;"xml:space="preserve">
                         <g><path d="M225.923,354.706c-8.098,0-16.195-3.092-22.369-9.263L9.27,151.157c-12.359-12.359-12.359-32.397,0-44.751  c12.354-12.354,32.388-12.354,44.748,0l171.905,171.915l171.906-171.909c12.359-12.354,32.391-12.354,44.744,0 c12.365,12.354,12.365,32.392,0,44.751L248.292,345.449C242.115,351.621,234.018,354.706,225.923,354.706z"/></g>
                        </svg>
                      </span>
                      </button>
                      </h4>
										</div>
										<div id="collapseSouthAmerica" class="collapse" aria-labelledby="headingSouthAmerica" data-parent="#accordion1">
											<div class="card-body">
												<ul class="">
													<li class="item-language-brazil ">	<a href="<?php echo $home_url;?>/brazil/" data-lang="brazil">Brazil</a></li>
												</ul>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="footer swiper-slide" id="slide8" style=" background-color: rgb(56, 56, 61); color:#0;bacXkground-size: 100%;align-items: center;">
			<div class="container">
				<div class="row">
					<div class="ol-md-8 offset-md-8" style="alXign-self: flex-end;">
						<div class="row">
							<div class="col-24" style="alXign-self: flex-end;text-align: left;">
								<div class="footer-menu" style="display:inline-block; padding: 0px 0 0;text-align: left;">
									<h4>CodeJika:</h4>
									<ul style="padding-left: 16px;">
										<li class="hide">Sign Up</li>
										<li> <a href="/learn/5-minute-website" target="_blank">Start Coding
</a> 
										</li>
										<li> <a href="/club-signup">Start a Club</a> 
										</li>
										<li> <a href="/1-hour-website">1 Hour Website</a> 
										</li>
										<li> <a href="/the-5-minute-website">5 Minute Website</a> 
										</li>
										<li> <a href="/coding-clubs">Coding Clubs</a> 
										</li>
										<li> <a href="/computer-science-week2018">Computer Science Week 2018</a> 
										</li>
										<li> <a href="/edutech-africa-2019">EduTECH Africa 2019</a> 
										</li>
									</ul>
								</div>
							</div>
							<div class="col-24" style="alXign-self: flex-end;text-align: left;">
								<div class="footer-menu" style="display:inline-block; padding: 10px 0 0;text-align: left;">
									<h4>Education:</h4>
									<ul style="padding-left: 16px;">
										<li> <a href="/news">News and Articles</a> 
										</li>
										<li> <a href="/coding-resources">CodeJIKA Resources</a> 
										</li>
										<li> <a href="https://csedweek.org/" target="_blank">Hour of Code</a> 
										</li>
										<ul></ul>
									</ul>
								</div>
							</div>
							<div class="col-md-12 col-24" style="alXign-self: flex-end;text-align: left;">
								<div class="footer-menu" style="display:inline-block; padding: 10px 0 0;">
									<h4>Partners:</h4>
									<ul style="padding-left: 16px;">
										<li> <a href="/business">Corporates</a> 
										</li>
										<li> <a href="/schools">Schools</a> 
										</li>
										<li> <a href="/schools">Districts</a> 
										</li>
										<li> <a href="/media-corner">Media Corner</a> 
										</li>
										<ul></ul>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class=" copyright-footer col-md-24 pl-1 pr-1  pl-sm-2 pr-sm-2" style="background-color: #292929;">
					<div class="copyright ">	<a id="switch-device" class="switXch-mode  mr-2  mr-sm-4" href="">View as Desktop
</a> ©
						<script>
							document.write(new Date().getFullYear())
						</script>	<a href="https://code4change.co.za/" target="_blank">Code for Change</a> | CodeJIKA.com</div>
				</div>        
			</div>
		</div>
	</div>
<div class="swiper-pagination swiper-pagination-v"></div>
</div>
<div class="modal fullscreen" id="menuModal">
  <button id="modal-close" type="button" class="close" data-dismiss="modal">&times;</button>
  <div class="modal-dialog">

<div class="modal-content">

<!-- Modal body -->
<div class="cmain-box-wrap">
  <?php echo $dp; ?>
  <?php echo $curriculam; ?>
</div>
</div>
  </div>
</div>
<div class="modalPlaceholder"></div>


  <!--Core JS Files-->
  <!--script src="js/firebase.js"></script-->
<!-- script src="js/jquery-1.11.0.min.js" type="text/javascript"></script-->
  <!--script type="text/javascript" src="js/jquery-ui.min.js"></script-->
  <!--script src="https://code.jquery.com/pep/0.4.3/pep.js"></script-->
  <!--script type="text/javascript" src="../js/jquery.jscroll.js"></script-->
  <!--script src="js/popper.min.js" type="text/javascript"></script-->
  <!-- <script src="/assets/js/bootstrap.min.js" type="text/javascript"></script-->  
  <!--script src="js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous" type="text/javascript"></script-->
  <!-- Control Center for Now Ui Kit: parallax effects, scripts for the example pages etc -->
  <!--script src="/js/now-ui-kit.js?v=1.1.0" type="text/javascript"></script-->
<!--script src="js/swiper.min.js"></script-->
  
	<!--script  src="/js/fa-regular.js" type="text/javascript"></script-->
	<!--script  src="/js/fontawesome.min.js" type="text/javascript"></script-->
  <!--script  src="/js/fa-solid.js" type="text/javascript"></script-->
  <!--script src="/js/confirmDialog.jquery.js"></script-->


  <!-- Go to www.addthis.com/dashboard to customize your tools -->
  <!--script defer type="text/javascript" src="//s7.addtXhis.com/js/300/addthis_widget.js#pubid=ra-5ab3998e4fdf2760"></script-->

  <script type="text/javascript">
//<![CDATA[
document.write("<script type='text/javascript' src='<?php echo $home_url;?>/js/mobile-homepage-swiper.js?" + randText() + "'><\/script>");
//]]>
  </script>
	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-63106610-3"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-63106610-3');

  $(document).ready(function () {
  //swiperV.slideTo(4);
  });
  
  console.log("<?php echo $get_lang; ?>");
  
  $('#whatsapp,#whatsapp2').attr('href', 'whatsapp://send?text=Hey, I think you should learn to code too. Check out CodeJIKA.com it\'s free, fun and teaches you to code websites easily.😎‏%0A‎👉 ' + 'https://codejika.com')
</script>
     <script>
      $('#switch-device').click(function () {
      console.log("se1: " + $(location).attr("href"));
     //  $.cookie('devicetype', 'desktop');
        document.cookie = "devicetype=desktop; domain=."+window.location.hostname+"; ?>;path=/; secure";
        //window.location.reload()
      })
    </script>
</body>

</html>