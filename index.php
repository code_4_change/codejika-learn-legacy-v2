<?php
/**
 * Front to the WordPress application. This file doesn't do anything, but loads
 * wp-blog-header.php which does and tells WordPress to load the theme.
 *
 * @package WordPress
 */
 
$uri_path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
$uri_segments = explode('/', $uri_path);
$get_lang = trim($uri_path, '/');

$home_link = 'https://'.$_SERVER['SERVER_NAME'].'/'.$get_lang.'/';

//$country_code = $_SERVER['HTTP_CF_IPCOUNTRY'];

$cookie_name = "pll_language";
$mobile = "-m";
$logic = "default set to mob";

/*
if($_COOKIE['devicetype'] == "desktop") {
    $mobile = "";
    $logic = "local cookie found desk";
} 
if($_COOKIE['devicetype'] == "mobile") {
  $mobile = "-m";
  $logic = "local cookie found mob";
}  

if ( isset($_GET['desktop'])) {
   $mobile = "";
    $logic = "GET desk";
}  
  if ( isset($_GET['mobile'])) {
     $mobile = "-m";
    $logic = "GET mobile";
}
 
   
    
    if ( isset($_GET['desktop'])) {
   $mobile = "";
    $logic = "GET desk";
}  
  if ( isset($_GET['mobile'])) {
     $mobile = "-m";
    $logic = "GET mobile";
} */
    
if (getenv("DEVICE_TYPE") == "desktop") {
    $mobile = "";
    $logic = "DEVICE_TYPE found desk";
} else if (getenv("DEVICE_TYPE") == "mobile") {
  $mobile = "-m";
  $logic = "DEVICE_TYPE found mob";
}  

$countries = array(
  "", "index.php", //include root directory (homepage)
  "botswana",
  "johannesburg",
  "namibia",
  "mozambqiue",
  "southafrica",
  "zambia",
  "palestine",
  "usa",
  "brazil"    
);

setcookie( "homeURL", $home_link , time() + 60 * 60 * 24 * 1, '/' ); // 1 day(s)
setcookie( "logic-mobile", $logic . " | " . $mobile , time() + 60 * 60 * 24 * 1, '/' ); // 1 day(s)
    
if (in_array($get_lang, $countries)) {
  if(!isset($_COOKIE[$cookie_name] )) {
    setcookie( $cookie_name, $get_lang, time() + 60 * 60 * 24 * 1, '/' ); // 1 day(s)
  }
 include($_SERVER['DOCUMENT_ROOT'].'/homepage/language'.$mobile.'.php'); 

  if (file_exists($_SERVER['DOCUMENT_ROOT'].'/homepage/'.$get_lang.'/language'.$mobile.'.php')) {
    include($_SERVER['DOCUMENT_ROOT'].'/homepage/'.$get_lang.'/language'.$mobile.'.php');   // override with localized strings
  } 
  
  if (file_exists($_SERVER['DOCUMENT_ROOT'].'/homepage/'.$get_lang.'/template'.$mobile.'.php')) {
    include($_SERVER['DOCUMENT_ROOT'].'/homepage/'.$get_lang.'/template'.$mobile.'.php');   
  } else {
    include($_SERVER['DOCUMENT_ROOT'].'/homepage/template'.$mobile.'.php'); 
  }
} else {

/**
 * Tells WordPress to load the WordPress theme and output it.
 *
 * @var bool
 */
define( 'WP_SITEURL', 'https://'.$_SERVER['SERVER_NAME'] );
define( 'WP_HOME', 'https://'.$_SERVER['SERVER_NAME'] );


/** Loads the WordPress Environment and Template */
require( dirname( __FILE__ ) . '/wp-blog-header.php' );
//echo '<script>console.log("WP_SITEURL: '. constant("WP_SITEURL") .'")</script>';
//echo '<script>console.log("WP_DEBUG_LOG: '. constant("WP_DEBUG_LOG") .'")</script>';
//echo '<script>console.log("pll_home_url: '. pll_home_url() .'")</script>';
//}
}
