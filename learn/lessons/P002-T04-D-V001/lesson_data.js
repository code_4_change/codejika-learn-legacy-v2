var file_lesson_data = 
{
    "defaultCode": "", // default code, if user has not already started coding
    // "kbLayout": "", //not currently in use
    // "loadJS": "", //not currently in use
    // "nextLessonSlug": "", //not currently in use
    "pageDesc": "Learn how to build your first website with these easy intro lessons to coding.", 
    "pageKeywords": "coding, code, learn to code, code website, my first website", 
    "pageTitle": "CodeJIKA - Project 2, Training 4",
    "total_slides": 57,
    "save_lesson_id": "P2Training4", // This is id that will be used to store save usercode in Firebase
    // "slides": [  //not currently in use
    // ],
    // "slug": "" //not currently in use
}

var check_points = {
  14:"((.|\n)\s*)(h3)((.|\n)*)\s*{((.|\n)*)\s*}((.|\n)*)\s*",
  15:"((.|\n)\s*)(h3)((.|\n)*)\s*{((.|\n)*)\s*(background-color)((.|\n)*)\s*:((.|\n)*)\s*(#ce54c7;)((.|\n)*)\s*}((.|\n)*)\s*",
  17:"((.|\n)\s*)(h3)((.|\n)*)\s*{((.|\n)*)\s*(background-color)((.|\n)*)\s*:((.|\n)*)\s*(#ce54c7;)((.|\n)*)\s*(padding)((.|\n)*)\s*:((.|\n)*)\s*(15px;)((.|\n)*)\s*}((.|\n)*)\s*",
  18:"((.|\n)\s*)(h3)((.|\n)*)\s*{((.|\n)*)\s*(background-color)((.|\n)*)\s*:((.|\n)*)\s*(#ce54c7;)((.|\n)*)\s*(padding)((.|\n)*)\s*:((.|\n)*)\s*(15px;)((.|\n)*)\s*(font-size)((.|\n)*)\s*:((.|\n)*)\s*(1.5em;)((.|\n)*)\s*}((.|\n)*)\s*",
  19:"((.|\n)\s*)(h3)((.|\n)*)\s*{((.|\n)*)\s*(background-color)((.|\n)*)\s*:((.|\n)*)\s*(#ce54c7;)((.|\n)*)\s*(padding)((.|\n)*)\s*:((.|\n)*)\s*(15px;)((.|\n)*)\s*(font-size)((.|\n)*)\s*:((.|\n)*)\s*(1.5em;)((.|\n)*)\s*(border-left)((.|\n)*)\s*:((.|\n)*)\s*(10px)((.|\n)*)\s*(solid)((.|\n)*)\s*(#d6d5cd);((.|\n)*)\s*}((.|\n)*)\s*",
  24:"((.|\n)\s*)(header)((.|\n)*)\s*(\,)((.|\n)*)\s*(h3)((.|\n)*)\s*{((.|\n)*)\s*(color)((.|\n)*)\s*(:)((.|\n)*)\s*(white)((.|\n)*)\s*(;)((.|\n)*)\s*}((.|\n)*)\s*",
  34:"((.|\n)\s*)(body)((.|\n)*)\s*({)((.|\n)*)\s*(font-family)((.|\n)*)\s*(:)((.|\n)*)\s*(helvetica)((.|\n)*)\s*(,)((.|\n)*)\s*(arial)((.|\n)*)\s*(,)((.|\n)*)\s*(san-serif)((.|\n)*)\s*(;)((.|\n)*)\s*}((.|\n)*)\s*",
  35:"((.|\n)\s*)(header)((.|\n)*)\s*(\,)((.|\n)*)\s*(h3)((.|\n)*)\s*{((.|\n)*)\s*(color)((.|\n)*)\s*(:)((.|\n)*)\s*(white)((.|\n)*)\s*(;)((.|\n)*)\s*(font-family)((.|\n)*)\s*(:)((.|\n)*)\s*(\"Arial Black\")((.|\n)*)\s*(,)((.|\n)*)\s*(Gadget)((.|\n)*)\s*(,)((.|\n)*)\s*(san-serif)((.|\n)*)\s*(;)((.|\n)*)\s*}((.|\n)*)\s*",
}

var hintsForCheckPonts = {
  /* 11:"Type your code below the inside the <strong>header{ }</strong> selector in style.", */
}
  
var hints_data =   `

  <p style="margin-bottom:0px;">Slide: 12</p>
    <pre style="margin-bottom:10px; color:#ccc;">&lt;h1&gt;Nomzamo Mbatha&lt;/h1&gt;</pre>
    <p style="margin-bottom:0px;">Slide: 14</p>
    <pre style="margin-bottom:10px;">&lt;p>Hi! I'm Nomzamo Mbatha, An Actress. Say Hello!&lt;/p&gt;</pre>
    <p style="margin-bottom:0px;">Slide: 19</p>
    <pre style="margin-bottom:10px;">&lt;input type="email"&gt;</pre>
    <p style="margin-bottom:0px;">Slide: 23</p>	  
    <pre style="margin-bottom:10px;">&lt;input type="submit" &gt;</pre>
    <p style="margin-bottom:0px;">Slide: 27</p>
    <pre>placeholder="Your email"</pre>

    ` 
  
var slides_data = ` 

  <div class="tab-pane tab-pane-slide active" id="slide1" role="tabpanel">
    <img src="../learn/lessons/P002-T04-D-V001/img/Slide1.PNG">
    <a class="btn btn-primary next" style="top:65%;">Start Slideshow →</a>
  </div>
  <div class="tab-pane tab-pane-slide" id="slide2" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T04-D-V001/img/Slide2.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide3" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T04-D-V001/img/Slide3.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide4" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T04-D-V001/img/Slide4.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide5" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T04-D-V001/img/Slide5.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide6" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T04-D-V001/img/Slide6.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide7" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T04-D-V001/img/Slide7.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide8" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T04-D-V001/img/Slide8.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide9" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T04-D-V001/img/Slide9.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide10" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T04-D-V001/img/Slide10.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide11" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T04-D-V001/img/Slide11.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide12" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T04-D-V001/img/Slide12.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide13" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T04-D-V001/img/Slide13.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide14" role="tabpanel">

    <div class="checkpoint">
      <h2>
        Challenge <br>
        <span class="challange-step"> Step 1 of 5</span>
      </h2>

      <ol class="actions">
        <li data-index="0">
          <span class="fa fa-check "></span>
          Create a h3 selector.<br>
          <span class="html-code">
            &nbsp;&nbsp; h3 {<br>
            &nbsp;&nbsp; }
          </span>
        </li>
      </ol>
      <a class="btn btn-primary next check" style="toXp:65%;">Skip this →</a>
      <div class="challenge-tip"><span>Tip:</span>
        In <span class="html-code"> &lt;style&gt;</span> section.
      </div>
    </div>

  </div>


  <div class="tab-pane tab-pane-slide" id="slide15" role="tabpanel">

    <div class="checkpoint">
      <h2>
        Challenge <br>
        <span class="challange-step"> Step 2 of 5</span>
      </h2>

      <ol class="actions">
        <li data-index="0">
          <span class="fa fa-check "></span>
          Add a background color to h3.<br>
          <span class="html-code">
            &nbsp;&nbsp; background-color: #ce54c7;
          </span>
        </li>
      </ol>
      <a class="btn btn-primary next check" style="toXp:65%;">Skip this →</a>
      <div class="challenge-tip"><span>Tip:</span>
        In <span class="html-code"> &lt;style&gt;</span> section.
      </div>
    </div>

  </div>

  <div class="tab-pane tab-pane-slide" id="slide16" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T04-D-V001/img/Slide16.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide17" role="tabpanel">

    <div class="checkpoint">
      <h2>
        Challenge <br>
        <span class="challange-step"> Step 3 of 5</span>
      </h2>

      <ol class="actions">
        <li data-index="0">
          <span class="fa fa-check "></span>
          Add 15px of padding to h3.<br>

        </li>
      </ol>
      <a class="btn btn-primary next check" style="toXp:65%;">Skip this →</a>

    </div>

  </div>


  <div class="tab-pane tab-pane-slide" id="slide18" role="tabpanel">

    <div class="checkpoint">
      <h2>
        Challenge <br>
        <span class="challange-step"> Step 4 of 5</span>
      </h2>

      <ol class="actions">
        <li data-index="0">
          <span class="fa fa-check "></span>
          Increase the h3 font size with em.<br>
          <span class="html-code">
            &nbsp;&nbsp; font-size: 1.5em;
          </span>
        </li>
      </ol>
      <a class="btn btn-primary next check" style="toXp:65%;">Skip this →</a>
    </div>

  </div>


  <div class="tab-pane tab-pane-slide" id="slide19" role="tabpanel">

    <div class="checkpoint">
      <h2>
        Challenge <br>
        <span class="challange-step"> Step 5 of 5</span>
      </h2>

      <ol class="actions">
        <li data-index="0">
          <span class="fa fa-check "></span>
          Add a left border with this color: #d6d5cd<br>
          <span class="html-code">
            &nbsp;&nbsp; border-left: 10px solid #d6d5cd;
          </span>
        </li>
      </ol>
      <a class="btn btn-primary next check" style="toXp:65%;">Skip this →</a>
      <div class="challenge-tip"><span>Tip:</span>
        You learned about borders in P1(Project 1.)
      </div>
    </div>

  </div>

  <div class="tab-pane tab-pane-slide" id="slide20" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T04-D-V001/img/Slide20.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide21" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T04-D-V001/img/Slide21.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide22" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T04-D-V001/img/Slide22.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide23" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T04-D-V001/img/Slide23.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide24" role="tabpanel">

    <div class="checkpoint">
      <h2>
        Challenge <br>
        <span class="challange-step"> Step 5 of 5</span>
      </h2>

      <ol class="actions">
        <li data-index="0">
          <span class="fa fa-check "></span>
          Make both &lt;header&gt; &amp; &lt;h3&gt; white using a single CSS element.
          <br>

          <span class="html-code">
            &nbsp;&nbsp; header, h3 {<br>
            color: white;<br>
            }
          </span>
        </li>
      </ol>
      <a class="btn btn-primary next check" style="toXp:65%;">Skip this →</a>
      <div class="challenge-tip"><span>Tip:</span>

      </div>
    </div>

  </div>

  <div class="tab-pane tab-pane-slide" id="slide25" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T04-D-V001/img/Slide25.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide26" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T04-D-V001/img/Slide26.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide27" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T04-D-V001/img/Slide27.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide28" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T04-D-V001/img/Slide28.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide29" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T04-D-V001/img/Slide29.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide30" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T04-D-V001/img/Slide30.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide31" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T04-D-V001/img/Slide31.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide32" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T04-D-V001/img/Slide32.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide33" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T04-D-V001/img/Slide33.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide34" role="tabpanel">

    <div class="checkpoint">
      <h2>
        Challenge <br>
        <span class="challange-step"></span>
      </h2>

      <ol class="actions">
        <li data-index="0">
          <span class="fa fa-check "></span>
          Use Helvetica, Arial and San-serif as font-family for body<br>
          <span class="html-code">
            &nbsp;&nbsp; body { <br>
            &nbsp;&nbsp;&nbsp;&nbsp; font-family: helvetica, arial, san-serif; <br>
            &nbsp;&nbsp; }
          </span>
        </li>
      </ol>
      <a class="btn btn-primary next check" style="toXp:65%;">Skip this →</a>

    </div>

  </div>


  <div class="tab-pane tab-pane-slide" id="slide35" role="tabpanel">

    <div class="checkpoint">
      <h2>
        Challenge <br>
        <span class="challange-step"> Step 2 of 5</span>
      </h2>

      <ol class="actions">
        <li data-index="0">
          <span class="fa fa-check "></span>
          Add Arial, Gadget and san serif as font-family for header &amp; h3.<br>
          <span class="html-code">
            &nbsp;&nbsp; header, h3 { <br>
            &nbsp;&nbsp;&nbsp;&nbsp; color: white; <br>
            &nbsp;&nbsp;&nbsp;&nbsp; font-family: “Arial Black”, Gadget, sans-serif;<br>
            &nbsp;&nbsp; }
          </span>
        </li>
      </ol>
      <a class="btn btn-primary next check" style="toXp:65%;">Skip this →</a>

    </div>

  </div>

  <div class="tab-pane tab-pane-slide" id="slide36" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T04-D-V001/img/Slide36.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide37" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T04-D-V001/img/Slide37.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide38" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T04-D-V001/img/Slide38.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide39" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T04-D-V001/img/Slide39.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide40" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T04-D-V001/img/Slide40.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide41" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T04-D-V001/img/Slide41.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide42" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T04-D-V001/img/Slide42.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide43" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T04-D-V001/img/Slide43.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide44" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T04-D-V001/img/Slide44.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide45" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T04-D-V001/img/Slide45.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide46" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T04-D-V001/img/Slide46.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide47" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T04-D-V001/img/Slide47.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide48" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T04-D-V001/img/Slide48.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide49" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T04-D-V001/img/Slide49.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide50" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T04-D-V001/img/Slide50.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide51" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T04-D-V001/img/Slide51.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide52" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T04-D-V001/img/Slide52.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide53" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T04-D-V001/img/Slide53.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide54" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T04-D-V001/img/Slide54.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide55" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T04-D-V001/img/Slide55.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide56" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T04-D-V001/img/Slide56.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide57" role="tabpanel">
    <img src="../learn/lessons/P002-T04-D-V001/img/Slide57.PNG">
    <a href="../learn/P2Training5" class="btn btn-primary" style="top:65%;">Start next training →</a>
  </div>
       
` 

/// Add custom JS for lesson below here