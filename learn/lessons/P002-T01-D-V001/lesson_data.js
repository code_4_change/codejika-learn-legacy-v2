var file_lesson_data = 
{
    "defaultCode": "", // default code, if user has not already started coding
    "kbLayout": "", //not currently in use
    "loadJS": "", //not currently in use
    "nextLessonSlug": "", //not currently in use
    "pageDesc": "Learn how to build your first website with these easy intro lessons to coding.", 
    "pageKeywords": "coding, code, learn to code, code website, my first website", 
    "pageTitle": "CodeJIKA - Project 2, Training 1",
    "total_slides": 36,
    "save_lesson_id": "P2Training1", // This is id that will be used to store save usercode in Firebase
    "slides": [ 
    ], //not currently in use
    "slug": "" //not currently in use
}

var check_points = {
  9:"(!Doctype html>)((.|\n)*)\s*(<head>|<head [^>]*>)((.|\n)*)\s*(<style>|<style [^>]*>)((.|\n)*)\s*<\/style>((.|\n)*)\s*<\/head>",
  10:"((.|\n)*)\s*<\/head>((.|\n)*)\s*(<body>|<body [^>]*>)((.|\n)*)\s*<\/body>",
  13:"((.|\n)*)\s*(<body>|<body [^>]*>)((.|\n)*)\s*(<header>|<header [^>]*>)((.|\n)*)\s*(<\/header>)((.|\n)*)\s*<\/body>",
  14:"((.|\n)*)\s*(<body>|<body [^>]*>)((.|\n)*)\s*(<header>|<header [^>]*>)((.|\n)*)\s*<h1>((.|\n)*)\s*<\/h1>((.|\n)*)\s*(<\/header>)((.|\n)*)\s*<\/body>",
  20:"((.|\n)*)\s*(<style>|<style [^>]*>)((.|\n)*)\s*header((.|\n)*)\s*{((.|\n)*)\s*}((.|\n)*)\s*<\/style>((.|\n)*)\s*",
  21:"(<style>|<style [^>]*>)((.|\n)*)header((.|\n)*)\s*{((.|\n)*)\s*(background|background-color)((.|\n)*)\s*:((.|\n)*)\s*lightblue;((.|\n)*)\s*<\/style>",
  22:"(<style>|<style [^>]*>)((.|\n)*)header((.|\n)*)\s*{((.|\n)*)\s*(border)((.|\n)*)\s*:((.|\n)*)\s*solid((.|\n)*)\s*blue;((.|\n)*)\s*<\/style>",
  25:"(<style>|<style [^>]*>)((.|\n)*)header((.|\n)*)\s*{((.|\n)*)\s*(border-width)((.|\n)*)\s*:((.|\n)*)\s*10px((.|\n)*)\s*0px((.|\n)*)\s*10px((.|\n)*)\s*0px;((.|\n)*)\s*<\/style>"
}

var hintsForCheckPonts = {
  /* 10:"Type your code below the closing <span class='html-code'><strong>&lt;\/head&gt;</strong></span> tag.",
  13:"<span class='html-code'><strong>&lt;body&gt;</strong></span> Type Code Here <span class='html-code'><strong>&lt;\/body&gt;</strong></span>",
  20:"<span class='html-code'><strong>&lt;style&gt;</strong></span> Type Code Here <span class='html-code'><strong>&lt;\/style&gt;</strong></span>" */
}
  
var hints_data =   `

  <p style="margin-bottom:0px;">Slide: 12</p>
    <pre style="margin-bottom:10px; color:#ccc;">&lt;h1&gt;Nomzamo Mbatha&lt;/h1&gt;</pre>
    <p style="margin-bottom:0px;">Slide: 14</p>
    <pre style="margin-bottom:10px;">&lt;p>Hi! I'm Nomzamo Mbatha, An Actress. Say Hello!&lt;/p&gt;</pre>
    <p style="margin-bottom:0px;">Slide: 19</p>
    <pre style="margin-bottom:10px;">&lt;input type="email"&gt;</pre>
    <p style="margin-bottom:0px;">Slide: 23</p>	  
    <pre style="margin-bottom:10px;">&lt;input type="submit" &gt;</pre>
    <p style="margin-bottom:0px;">Slide: 27</p>
    <pre>placeholder="Your email"</pre>

    ` 
  
var slides_data = ` 

  <div class="tab-pane tab-pane-slide active" id="slide1" role="tabpanel">
    <img src="../learn/lessons/P002-T01-D-V001/img/Slide1.PNG">
    <a class="btn btn-primary next" style="top:65%;">Start Slideshow →</a>
  </div>
  <div class="tab-pane tab-pane-slide" id="slide2" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T01-D-V001/img/Slide2.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide3" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T01-D-V001/img/Slide3.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide4" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T01-D-V001/img/Slide4.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide5" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T01-D-V001/img/Slide5.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide6" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T01-D-V001/img/Slide6.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide7" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T01-D-V001/img/Slide7.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide8" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T01-D-V001/img/Slide8.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide9" role="tabpanel">


    <div class="checkpoint">
      <h2>
        Challenge <br>
        <span class="challange-step"> Step 1</span>
      </h2>

      <ol class="actions">
        <li data-index="0">
          <span class="check-icon">/</span>
          Add this part:<br>
          <span class="html-code">
            &lt;!Doctype html&gt;<br>
            &lt;head&gt;<br>
            &nbsp;&nbsp;&lt;style&gt;
            &nbsp;&nbsp;&lt;/style&gt;<br>
            &lt;/head&gt;
          </span>
        </li>
      </ol>
      <a class="btn btn-primary next check" style="toXp:65%;">Skip this →</a>
    </div>

  </div>


  <div class="tab-pane tab-pane-slide" id="slide10" role="tabpanel">

    <div class="checkpoint">
      <h2>
        Challenge <br>
        <span class="challange-step"> Step 2</span>
      </h2>

      <ol class="actions">
        <li data-index="0">
          <span class="check-icon">/</span>
          Now add the <span class="html-code">&lt;body&gt;</span> section.
        </li>
      </ol>
      <a class="btn btn-primary next check" style="toXp:65%;">Skip this →</a>
    </div>

  </div>

  <div class="tab-pane tab-pane-slide" id="slide11" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T01-D-V001/img/Slide11.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide12" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T01-D-V001/img/Slide12.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide13" role="tabpanel">

    <div class="checkpoint">
      <h2>
        Challenge <br>
        <span class="challange-step"> Step 1</span>
      </h2>

      <ol class="actions">
        <li data-index="0">
          <span class="check-icon">/</span>
          Add a <span class="html-code">&lt;header&gt;</span>.<br>
          <span class="html-code">
            &lt;header&gt;<br>
            &lt;/header&gt;
          </span>
        </li>
      </ol>
      <a class="btn btn-primary next check" style="toXp:65%;">Skip this →</a>
    </div>

  </div>


  <div class="tab-pane tab-pane-slide" id="slide14" role="tabpanel">

    <div class="checkpoint">
      <h2>
        Challenge <br>
        <span class="challange-step"> Step 2</span>
      </h2>

      <ol class="actions">
        <li data-index="0">
          <span class="check-icon">/</span>
          Add your name in all-caps (capital letters) in an <span class="html-code">&lt;h1&gt;</span> in <span class="html-code">&lt;header&gt;</span> <br>
          <span class="html-code">
            &lt;header&gt; THANDI NDLOVU
            &lt;/header&gt;
          </span>
        </li>
      </ol>
      <a class="btn btn-primary next check" style="toXp:65%;">Skip this →</a>
    </div>

  </div>

  <div class="tab-pane tab-pane-slide" id="slide15" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T01-D-V001/img/Slide15.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide16" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T01-D-V001/img/Slide16.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide17" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T01-D-V001/img/Slide17.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide18" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T01-D-V001/img/Slide18.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide19" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T01-D-V001/img/Slide19.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide20" role="tabpanel">

    <div class="checkpoint">
      <h2>
        Challenge <br>
        <span class="challange-step"> Step 1</span>
      </h2>

      <ol class="actions">
        <li data-index="0">
          <span class="check-icon">/</span>
          Add a CSS selector called header.<br>
          <span class="html-code">
            &lt;style&gt;<br>
            &nbsp; header {<br>
            &nbsp;&nbsp;}<br>
            &lt;/style&gt;
          </span>
        </li>
      </ol>
      <a class="btn btn-primary next check" style="toXp:65%;">Skip this →</a>
    </div>

  </div>


  <div class="tab-pane tab-pane-slide" id="slide21" role="tabpanel">

    <div class="checkpoint">
      <h2>
        Challenge <br>
        <span class="challange-step"> Step 2</span>
      </h2>

      <ol class="actions">
        <li data-index="0">
          <span class="check-icon">/</span>
          Add a lightblue background to the header element.<br>
          <span class="html-code">
            &nbsp; background: lightblue;
          </span>
        </li>
      </ol>
      <a class="btn btn-primary next check" style="toXp:65%;">Skip this →</a>
    </div>

  </div>


  <div class="tab-pane tab-pane-slide" id="slide22" role="tabpanel">

    <div class="checkpoint">
      <h2>
        Challenge <br>
        <span class="challange-step"> Step 3</span>
      </h2>

      <ol class="actions">
        <li data-index="0">
          <span class="check-icon">/</span>
          Add a simple blue border to &lt;header&gt;.<br>
          <span class="html-code">
            &nbsp; border: solid blue;
          </span>
        </li>
      </ol>
      <a class="btn btn-primary next check" style="toXp:65%;">Skip this →</a>
    </div>

  </div>

  <div class="tab-pane tab-pane-slide" id="slide23" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T01-D-V001/img/Slide23.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide24" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T01-D-V001/img/Slide24.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide25" role="tabpanel">

    <div class="checkpoint">
      <h2>
        Challenge <br>
        <span class="challange-step"> Step 4</span>
      </h2>

      <ol class="actions">
        <li data-index="0">
          <span class="check-icon">/</span>
          Add a top and bottom border to header.<br>
          Both should be 10px wide.<br>
          <span class="html-code">
            &nbsp; border-width: 10px 0px 10px 0px;
          </span>
        </li>
      </ol>
      <a class="btn btn-primary next check" style="toXp:65%;">Skip this →</a>
    </div>

  </div>

  <div class="tab-pane tab-pane-slide" id="slide26" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T01-D-V001/img/Slide26.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide27" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T01-D-V001/img/Slide27.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide28" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T01-D-V001/img/Slide28.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide29" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T01-D-V001/img/Slide29.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide30" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T01-D-V001/img/Slide30.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide31" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T01-D-V001/img/Slide31.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide32" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T01-D-V001/img/Slide32.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide33" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T01-D-V001/img/Slide33.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide34" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T01-D-V001/img/Slide34.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide35" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T01-D-V001/img/Slide35.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide36" role="tabpanel">
    <img src="../learn/lessons/P002-T01-D-V001/img/Slide36.PNG">
    <a href="../learn/P2Training2" class="btn btn-primary" style="top:65%;">Start next training →</a>
  </div>
               
` 

/// Add custom JS for lesson below here