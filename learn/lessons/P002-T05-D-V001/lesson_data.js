var file_lesson_data = 
{
    "defaultCode": "", // default code, if user has not already started coding
    "kbLayout": "", //not currently in use
    "loadJS": "", //not currently in use
    "nextLessonSlug": "", //not currently in use
    "pageDesc": "Learn how to build your first website with these easy intro lessons to coding.", 
    "pageKeywords": "coding, code, learn to code, code website, my first website", 
    "pageTitle": "CodeJIKA - Project 2, Training 5",
    "total_slides": 45,
    "save_lesson_id": "P2Training5", // This is id that will be used to store save usercode in Firebase
    "slides": [ 
    ], //not currently in use
    "slug": "" //not currently in use
}

var check_points = {
  10:"((.|\n)\s*)<h3>((.|\n)*)\s*(ABOUT ME)((.|\n)*)\s*<\/h3>((.|\n)*)\s*",
  12:"((.|\n)\s*)<h3>((.|\n)*)\s*(ABOUT ME)((.|\n)*)\s*<\/h3>((.|\n)*)\s*<p>((.|\n)*)\s*<\/p>((.|\n)*)\s*",
  16:"((.|\n)\s*)<h3>((.|\n)*)\s*(EDUCATION)((.|\n)*)\s*<\/h3>((.|\n)*)\s*<p>((.|\n)*)\s*<\/p>((.|\n)*)\s*",
  18:"((.|\n)\s*)<h3>((.|\n)*)\s*(EXPERIENCES)((.|\n)*)\s*<\/h3>((.|\n)*)\s*<p>((.|\n)*)\s*<\/p>((.|\n)*)\s*",
  30:"((.|\n)\s*)<h3>((.|\n)*)\s*(SKILLS)((.|\n)*)\s*<\/h3>((.|\n)*)\s*<ul>((.|\n)*)\s*<li>((.|\n)*)\s*<\/li>((.|\n)*)\s*<\/ul>((.|\n)*)\s*",
  33:"((.|\n)\s*)(p)((.|\n)*)\s*(,)((.|\n)*)\s*(ul)((.|\n)*)\s*({)((.|\n)*)\s*(})((.|\n)*)\s*",
  34:"((.|\n)\s*)(p)((.|\n)*)\s*(,)((.|\n)*)\s*(ul)((.|\n)*)\s*({)((.|\n)*)\s*padding-left:((.|\n)*)\s*50px((.|\n)*)\s*;((.|\n)*)\s*(})((.|\n)*)\s*",
  35:"((.|\n)\s*)(p)((.|\n)*)\s*(,)((.|\n)*)\s*(ul)((.|\n)*)\s*({)((.|\n)*)\s*padding-left:((.|\n)*)\s*50px((.|\n)*)\s*;((.|\n)*)\s*font-size:((.|\n)*)\s*1.3em((.|\n)*)\s*;((.|\n)*)\s*(})((.|\n)*)\s*",
  40:"((.|\n)\s*)<h3>((.|\n)*)\s*(CONTACT)((.|\n)*)\s*<\/h3>((.|\n)*)\s*<p>((.|\n)*)\s*<\/p>((.|\n)*)\s*"
}

var hintsForCheckPonts = {
 /*  11:"Type your code below the inside the <strong>header{ }</strong> selector in style." */
}
  
var hints_data =   `

  <p style="margin-bottom:0px;">Slide: 12</p>
  <pre style="margin-bottom:10px; color:#ccc;">&lt;h1&gt;Nomzamo Mbatha&lt;/h1&gt;</pre>
  <p style="margin-bottom:0px;">Slide: 14</p>
  <pre style="margin-bottom:10px;">&lt;p>Hi! I'm Nomzamo Mbatha, An Actress. Say Hello!&lt;/p&gt;</pre>
  <p style="margin-bottom:0px;">Slide: 19</p>
  <pre style="margin-bottom:10px;">&lt;input type="email"&gt;</pre>
  <p style="margin-bottom:0px;">Slide: 23</p>	  
  <pre style="margin-bottom:10px;">&lt;input type="submit" &gt;</pre>
  <p style="margin-bottom:0px;">Slide: 27</p>
  <pre>placeholder="Your email"</pre>

    ` 
  
var slides_data = ` 

  <div class="tab-pane tab-pane-slide active" id="slide1" role="tabpanel">
    <img src="../learn/lessons/P002-T05-D-V001/img/Slide1.PNG">
    <a class="btn btn-primary next" style="top:65%;">Start Slideshow →</a>
  </div>
  <div class="tab-pane tab-pane-slide" id="slide2" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T05-D-V001/img/Slide2.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide3" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T05-D-V001/img/Slide3.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide4" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T05-D-V001/img/Slide4.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide5" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T05-D-V001/img/Slide5.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide6" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T05-D-V001/img/Slide6.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide7" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T05-D-V001/img/Slide7.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide8" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T05-D-V001/img/Slide8.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide9" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T05-D-V001/img/Slide9.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide10" role="tabpanel">


    <div class="checkpoint">
      <h2>
        Challenge <br>
        <span class="challange-step"> Step 1 of 4</span>
      </h2>

      <ol class="actions">
        <li data-index="0">
          <span class="fa fa-check "></span>
          Add an h3 with an “ABOUT ME” section.<br>
          <span class="html-code">
            &nbsp;&nbsp; &lt;h3&gt; <br>
            &nbsp;&nbsp;&nbsp;&nbsp; ABOUT ME<br>
            &nbsp;&nbsp; &lt;h3&gt;
          </span>
        </li>
      </ol>
      <a class="btn btn-primary next check" style="toXp:65%;">Skip this →</a>
      <div class="challenge-tip"><span>Where:</span>
        Below the “DETAILS” section.
      </div>
    </div>

  </div>

  <div class="tab-pane tab-pane-slide" id="slide11" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T05-D-V001/img/Slide11.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide12" role="tabpanel">


    <div class="checkpoint">
      <h2>
        Challenge <br>
        <span class="challange-step"> Step 2 of 4</span>
      </h2>

      <ol class="actions">
        <li data-index="0">
          <span class="fa fa-check "></span>
          Write about yourself in a <span class="html-code">&lt;p&gt;</span> <b>below the h3</b>.<br>
          Example: <br>
          <span class="html-code">
            &nbsp;&nbsp; &lt;p&gt; I love.... &lt;/p&gt;<br>
            &nbsp;&nbsp; &lt;p&gt; My dream is.... &lt;/p&gt;
          </span>
        </li>
      </ol>
      <a class="btn btn-primary next check" style="toXp:65%;">Skip this →</a>
      <div class="challenge-tip"><span>Tip:</span>
        You can add different lines by making a new <span class="html-code"> &lt;p&gt;</span> for each line.
      </div>
    </div>

  </div>

  <div class="tab-pane tab-pane-slide" id="slide13" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T05-D-V001/img/Slide13.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide14" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T05-D-V001/img/Slide14.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide15" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T05-D-V001/img/Slide15.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide16" role="tabpanel">


    <div class="checkpoint">
      <h2>
        Challenge <br>
        <span class="challange-step"> Step 3 of 4</span>
      </h2>

      <ol class="actions">
        <li data-index="0">
          <span class="fa fa-check "></span>
          Add an h3 “EDUCATION” section. Add the text in a few &lt;p&gt;s.
          <div class="example-code">
            Example text: <br>
            <span class="html-code">
              &nbsp;&nbsp; &lt;p&gt; 2011-2015: East-gate Primary, Boksburg... &lt;/p&gt;<br>
              &nbsp;&nbsp; &lt;p&gt; 2015-Ongoing Iterele-Zenzele Secondary... &lt;/p&gt;<br>
              &nbsp;&nbsp; &lt;p&gt; Fure: 2020-2024: BSC in Computer Science, University of Pretoria... &lt;/p&gt;
            </span>
          </div>
        </li>
      </ol>
      <a class="btn btn-primary next check" style="toXp:65%;">Skip this →</a>

    </div>

  </div>

  <div class="tab-pane tab-pane-slide" id="slide17" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T05-D-V001/img/Slide17.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide18" role="tabpanel">


    <div class="checkpoint">
      <h2>
        Challenge <br>
        <span class="challange-step">Step 4 of 4</span>
      </h2>

      <ol class="actions">
        <li data-index="0">
          <span class="fa fa-check "></span>
          Add an “EXPERIENCES” section. You can write about anything you want in the &lt;p&gt;.
          <div class="example-code">
            Example text: <br>
            <span class="html-code">
              &nbsp;&nbsp; &lt;p&gt; I was born in... &lt;/p&gt;<br>
              &nbsp;&nbsp; &lt;p&gt; I enjoy... &lt;/p&gt;<br>
              &nbsp;&nbsp; &lt;p&gt; I once saw... &lt;/p&gt;
            </span>
          </div>
        </li>
      </ol>
      <a class="btn btn-primary next check" style="toXp:65%;">Skip this →</a>
      <div class="challenge-tip"><span>Tip:</span>
        You can add different lines by making a new <span class="html-code"> &lt;p&gt;</span> for each line.
      </div>
    </div>

  </div>

  <div class="tab-pane tab-pane-slide" id="slide19" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T05-D-V001/img/Slide19.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide20" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T05-D-V001/img/Slide20.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide21" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T05-D-V001/img/Slide21.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide22" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T05-D-V001/img/Slide22.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide23" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T05-D-V001/img/Slide23.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide24" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T05-D-V001/img/Slide24.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide25" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T05-D-V001/img/Slide25.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide26" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T05-D-V001/img/Slide26.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide27" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T05-D-V001/img/Slide27.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide28" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T05-D-V001/img/Slide28.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide29" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T05-D-V001/img/Slide29.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide30" role="tabpanel">


    <div class="checkpoint">
      <h2>
        Challenge <br>
        <span class="challange-step">Step 1 of 4</span>
      </h2>

      <ol class="actions">
        <li data-index="0">
          <span class="fa fa-check "></span>
          Add an “SKILLS” section.
          <div class="example-code">
            Example: <br>
            <span class="html-code">
              &nbsp;&nbsp; &lt;h3&gt; SKILLS &lt;/h3&gt;<br>
              &nbsp;&nbsp; &lt;ul&gt;
              <br>
              &nbsp;&nbsp;&nbsp;&nbsp; &lt;li&gt;
              I can make anamazing spaghetti. &lt;/li&gt;

              <br>
              &nbsp;&nbsp;&nbsp;&nbsp; &lt;li&gt;
              Taught MichaelJackson to dance.
              &lt;/li&gt;


              &lt;/ul&gt;



            </span>
          </div>
        </li>
      </ol>
      <a class="btn btn-primary next check" style="toXp:65%;">Skip this →</a>
      <div class="challenge-tip"><span>Tip:</span>
        You can add different lines by making a new <span class="html-code"> &lt;p&gt;</span> for each line.
      </div>
    </div>

  </div>

  <div class="tab-pane tab-pane-slide" id="slide31" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T05-D-V001/img/Slide31.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide32" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T05-D-V001/img/Slide32.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide33" role="tabpanel">


    <div class="checkpoint">
      <h2>
        Challenge <br>
        <span class="challange-step">Step 2 of 4</span>
      </h2>

      <ol class="actions">
        <li data-index="0">
          <span class="fa fa-check "></span>
          Provide some padding and increase size of &lt;ul&gt; and &lt;p&gt;
          <div class="example-code">
            CODE: <br>
            <span class="html-code">
              p, ul {
              <br>
              }

            </span>
          </div>
        </li>
      </ol>
      <a class="btn btn-primary next check" style="toXp:65%;">Skip this →</a>
      <div class="challenge-tip"><span></span>

      </div>
    </div>

  </div>


  <div class="tab-pane tab-pane-slide" id="slide34" role="tabpanel">


    <div class="checkpoint">
      <h2>
        Challenge <br>
        <span class="challange-step">Step 3 of 4</span>
      </h2>

      <ol class="actions">
        <li data-index="0">
          <span class="fa fa-check "></span>
          Try 50px of padding on the left only.
          <div class="example-code">
            CODE: <br>
            <span class="html-code">
              p, ul {
              <br>
              padding-left: 50px;
              <br>
              }

            </span>
          </div>
        </li>
      </ol>
      <a class="btn btn-primary next check" style="toXp:65%;">Skip this →</a>
      <div class="challenge-tip"><span></span>

      </div>
    </div>

  </div>


  <div class="tab-pane tab-pane-slide" id="slide35" role="tabpanel">


    <div class="checkpoint">
      <h2>
        Challenge <br>
        <span class="challange-step">Step 3 of 4</span>
      </h2>

      <ol class="actions">
        <li data-index="0">
          <span class="fa fa-check "></span>
          Increase the size of the these selectors by 30% <br>
          Use em.
          <div class="example-code">
            CODE: <br>
            <span class="html-code">
              p, ul {
              <br>
              padding-left: 50px;
              <br>
              font-size: 1.3em;
              <br>
              }

            </span>
          </div>
        </li>
      </ol>
      <a class="btn btn-primary next check" style="toXp:65%;">Skip this →</a>
      <div class="challenge-tip"><span></span>

      </div>
    </div>

  </div>

  <div class="tab-pane tab-pane-slide" id="slide36" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T05-D-V001/img/Slide36.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide37" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T05-D-V001/img/Slide37.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide38" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T05-D-V001/img/Slide38.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide39" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T05-D-V001/img/Slide39.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide40" role="tabpanel">


    <div class="checkpoint">
      <h2>
        Challenge <br>
        <span class="challange-step">Step 3 of 4</span>
      </h2>

      <ol class="actions">
        <li data-index="0">
          <span class="fa fa-check "></span>
          Add a CONTACT section.
          <div class="example-code">
            CODE: <br>
            <span class="html-code">
              Add a paragraph with town, area and country you live in.<br>
              Add a phone number and email too.

            </span>
          </div>
        </li>
      </ol>
      <a class="btn btn-primary next check" style="toXp:65%;">Skip this →</a>
      <div class="challenge-tip"><span></span>

      </div>
    </div>

  </div>

  <div class="tab-pane tab-pane-slide" id="slide41" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T05-D-V001/img/Slide41.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide42" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T05-D-V001/img/Slide42.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide43" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T05-D-V001/img/Slide43.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide44" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T05-D-V001/img/Slide44.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide45" role="tabpanel">

    <a href="../learn/P2Training6" class="btn btn-primary" style="top:65%;">Start next training →</a>
  </div>
</div>            
` 

/// Add custom JS for lesson below here