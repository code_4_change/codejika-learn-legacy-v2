var file_lesson_data = 
{
    "defaultCode": "", // default code, if user has not already started coding
    "kbLayout": "", //not currently in use
    "loadJS": "", //not currently in use
    "nextLessonSlug": "", //not currently in use
    "pageDesc": "Learn how to build your first website with these easy intro lessons to coding.", 
    "pageKeywords": "coding, code, learn to code, code website, my first website", 
    "pageTitle": "CodeJIKA - Project 2, Training 2",
    "total_slides": 45,
    "save_lesson_id": "P2Training2", // This is id that will be used to store save usercode in Firebase
    "slides": [ 
    ], //not currently in use
    "slug": "" //not currently in use
}

var check_points = {
  11:"(<style>|<style [^>]*>)((.|\n)*)header((.|\n)*)\s*{((.|\n)*)\s*(margin)((.|\n)*)\s*:((.|\n)*)\s*0((.|\n)*)\s*auto((.|\n)*)\s*;((.|\n)*)\s*<\/style>",
  13:"(<style>|<style [^>]*>)((.|\n)*)header((.|\n)*)\s*{((.|\n)*)\s*(text-align)((.|\n)*)\s*:((.|\n)*)\s*center((.|\n)*)\s*;((.|\n)*)\s*<\/style>",
  19:"((.|\n)*)\s*<\/h1>((.|\n)*)\s*(<\/header>|<\/header [^>]*>)((.|\n)*)\s*",
  21:"((.|\n)*)\s*<i>((.|\n)*)\s*(<\/i>|<\/i [^>]*>)((.|\n)*)\s*",
  25:"(<style>|<style [^>]*>)((.|\n)*)h1((.|\n)*)\s*{((.|\n)*)\s*(font-size)((.|\n)*)\s*:((.|\n)*)\s*3em((.|\n)*)\s*;((.|\n)*)\s*<\/style>",
  29:"((.|\n)*)\s*(<\/i>)((.|\n)*)\s*(<br>)((.|\n)*)\s*(<br>)((.|\n)*)\s*<\/header>((.|\n)*)\s*(<br>)",
  33:"((.|\n)*)\s*<h3>((.|\n)*)\s*(DETAILS)((.|\n)*)\s*<\/h3>",
  34:"((.|\n)*)\s*<\/h3>((.|\n)*)\s*<p>((.|\n)*)\s*((.|\n)*)\s*<\/p>",
  36:"((.|\n)*)\s*<\/p>((.|\n)*)\s*<p>((.|\n)*)\s*((.|\n)*)\s*<\/p>",
  37:"((.|\n)*)\s*<p>((.|\n)*)\s*<\/p>((.|\n)*)\s*<p>((.|\n)*)\s*<\/p>((.|\n)*)\s*<p>((.|\n)*)\s*((.|\n)*)\s*<\/p>"
}

var hintsForCheckPonts = {
  /* 11:"Type your code below the inside the <strong>header{ }</strong> selector in style." */
}
  
var hints_data =   `

  <p style="margin-bottom:0px;">Slide: 12</p>
    <pre style="margin-bottom:10px; color:#ccc;">&lt;h1&gt;Nomzamo Mbatha&lt;/h1&gt;</pre>
    <p style="margin-bottom:0px;">Slide: 14</p>
    <pre style="margin-bottom:10px;">&lt;p>Hi! I'm Nomzamo Mbatha, An Actress. Say Hello!&lt;/p&gt;</pre>
    <p style="margin-bottom:0px;">Slide: 19</p>
    <pre style="margin-bottom:10px;">&lt;input type="email"&gt;</pre>
    <p style="margin-bottom:0px;">Slide: 23</p>	  
    <pre style="margin-bottom:10px;">&lt;input type="submit" &gt;</pre>
    <p style="margin-bottom:0px;">Slide: 27</p>
    <pre>placeholder="Your email"</pre>

    ` 
  
var slides_data = ` 

  <div class="tab-pane tab-pane-slide active" id="slide1" role="tabpanel">
    <img src="../learn/lessons/P002-T02-D-V001/img/Slide1.PNG">
    <a class="btn btn-primary next" style="top:65%;">Start Slideshow →</a>
  </div>
  <div class="tab-pane tab-pane-slide" id="slide2" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T02-D-V001/img/Slide2.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide3" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T02-D-V001/img/Slide3.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide4" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T02-D-V001/img/Slide4.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide5" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T02-D-V001/img/Slide5.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide6" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T02-D-V001/img/Slide6.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide7" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T02-D-V001/img/Slide7.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide8" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T02-D-V001/img/Slide8.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide9" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T02-D-V001/img/Slide9.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide10" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T02-D-V001/img/Slide10.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide11" role="tabpanel">


    <div class="checkpoint">
      <h2>
        Challenge <br>
        <span class="challange-step"> Step 1</span>
      </h2>

      <ol class="actions">
        <li data-index="0">
          <span class="fa fa-check "></span>
          Center the &lt;header&gt; using margin:<br>
          <span class="html-code">
            margin: 0 auto;
          </span>
        </li>
      </ol>
      <a class="btn btn-primary next check" style="toXp:65%;">Skip this →</a>

      <div class="challenge-tip"><span>Tip:</span>
        In the header { } selector in &lt;style&gt;.
      </div>
    </div>

  </div>

  <div class="tab-pane tab-pane-slide" id="slide12" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T02-D-V001/img/Slide12.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide13" role="tabpanel">


    <div class="checkpoint">
      <h2>
        Challenge <br>
        <span class="challange-step"> Step 2</span>
      </h2>

      <ol class="actions">
        <li data-index="0">
          <span class="fa fa-check "></span>
          Center text in the &lt;header&gt; section.<br>
          Use:<br>
          <span class="html-code">
            text-align:
          </span>
        </li>
      </ol>
      <a class="btn btn-primary next check" style="toXp:65%;">Skip this →</a>

      <div class="challenge-tip"><span>Tip:</span>
        Close the element with a ; (semi-colon).
      </div>
    </div>

  </div>

  <div class="tab-pane tab-pane-slide" id="slide14" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T02-D-V001/img/Slide14.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide15" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T02-D-V001/img/Slide15.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide16" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T02-D-V001/img/Slide16.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide17" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T02-D-V001/img/Slide17.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide18" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T02-D-V001/img/Slide18.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide19" role="tabpanel">


    <div class="checkpoint">
      <h2>
        Challenge <br>
        <span class="challange-step"> Step 2</span>
      </h2>

      <ol class="actions">
        <li data-index="0">
          <span class="fa fa-check "></span>
          Add your <b>dream job</b> or future profession. After the &lt;/h1&gt; in &lt;header&gt;.<br>
          Example:<br>
          <span class="html-code">
            &nbsp;&nbsp;&lt;/h1&gt;<br>
            &nbsp;&nbsp;Aspiring Mechanical Engineer<br>
            &nbsp;&lt;/header&gt;<br>
          </span>
        </li>
      </ol>
      <a class="btn btn-primary next check" style="toXp:65%;">Skip this →</a>

    </div>

  </div>

  <div class="tab-pane tab-pane-slide" id="slide20" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T02-D-V001/img/Slide20.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide21" role="tabpanel">


    <div class="checkpoint">
      <h2>
        Challenge <br>
        <span class="challange-step"> Step 2</span>
      </h2>

      <ol class="actions">
        <li data-index="0">
          <span class="fa fa-check "></span>
          Type: <i>italic</i> to your dream job by using &lt;i&gt;.<br>
          Example:<br>
          <span class="html-code">
            &nbsp;&nbsp;&lt;/h1&gt;<br>
            &nbsp;&nbsp;&lt;i&gt;<br>
            &nbsp;&nbsp;Aspiring Mechanical Engineer<br>
            &nbsp;&nbsp;&lt;/i&gt;<br>
            &nbsp;&lt;/header&gt;
          </span>
        </li>
      </ol>
      <a class="btn btn-primary next check" style="toXp:65%;">Skip this →</a>

    </div>

  </div>

  <div class="tab-pane tab-pane-slide" id="slide22" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T02-D-V001/img/Slide22.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide23" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T02-D-V001/img/Slide23.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide24" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T02-D-V001/img/Slide24.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide25" role="tabpanel">


    <div class="checkpoint">
      <h2>
        Challenge <br>
        <span class="challange-step"></span>
      </h2>

      <ol class="actions">
        <li data-index="0">
          <span class="fa fa-check "></span>
          Make h1 text lager. Create a h1 { } rule.<br> Use:<br> <b>font-size:</b>(to make it 3x the size.)
          <br>
          <span class="html-code">
            &nbsp;&nbsp; h1<br>
            &nbsp;&nbsp;&nbsp; { <br>
            &nbsp;&nbsp;&nbsp;&nbsp; font-size: 3em;<br>
            &nbsp;&nbsp;&nbsp; } <br>
          </span>
        </li>
      </ol>
      <a class="btn btn-primary next check" style="toXp:65%;">Skip this →</a>

    </div>

  </div>

  <div class="tab-pane tab-pane-slide" id="slide26" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T02-D-V001/img/Slide26.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide27" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T02-D-V001/img/Slide27.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide28" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T02-D-V001/img/Slide28.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide29" role="tabpanel">


    <div class="checkpoint">
      <h2>
        Challenge <br>
        <span class="challange-step"></span>
      </h2>

      <ol class="actions">
        <li data-index="0">
          <span class="fa fa-check "></span>
          Add <span class="html-code"> &lt;br&gt;</span> <br>
          1. After <span class="html-code"> &lt;header&gt;</span> opening tag.<br>
          2. <u>Twice</u> after <span class="html-code"> &lt;/i&gt;</span> <br>
          3. After <span class="html-code"> &lt;/header&gt; </span> closing tag <br>

        </li>
      </ol>
      <a class="btn btn-primary next check" style="toXp:65%;">Skip this →</a>

    </div>

  </div>

  <div class="tab-pane tab-pane-slide" id="slide30" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T02-D-V001/img/Slide30.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide31" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T02-D-V001/img/Slide31.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide32" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T02-D-V001/img/Slide32.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide33" role="tabpanel">


    <div class="checkpoint">
      <h2>
        Challenge <br>
        <span class="challange-step">Step 1</span>
      </h2>

      <ol class="actions">
        <li data-index="0">
          <span class="fa fa-check "></span>
          As a title, type “DETAILS” between an opening and closing <span class="html-code">&lt;h3&gt;</span> tag.<br>
          <span class="html-code">
            &nbsp;&lt;h3&gt;<br>
            &nbsp;&nbsp;&nbsp; DETAILS <br>
            &nbsp; &lt;/h3&gt;
          </span>
        </li>
      </ol>
      <a class="btn btn-primary next check" style="toXp:65%;">Skip this →</a>
      <div class="challenge-tip"><span>Tip:</span>
        Put this below the last <span class="html-code">&lt;br&gt;</span> and before the<span class="html-code"> &lt;body&gt;</span> closing tag.

      </div>

    </div>

  </div>


  <div class="tab-pane tab-pane-slide" id="slide34" role="tabpanel">


    <div class="checkpoint">
      <h2>
        Challenge <br>
        <span class="challange-step">Step 2</span>
      </h2>

      <ol class="actions">
        <li data-index="0">
          <span class="fa fa-check "></span>
          Add a <span class="html-code">&lt;p&gt;</span> with your Date of birth in it. <br>

          <span class="html-code">
            &nbsp;&lt;p&gt;Date of Birth: 27 July 2006 &lt;/p&gt;
          </span>
        </li>
      </ol>
      <a class="btn btn-primary next check" style="toXp:65%;">Skip this →</a>
      <div class="challenge-tip"><span>Where:</span>
        Below the <span class="html-code">&lt;h3&gt;</span> tag.
      </div>

    </div>

  </div>

  <div class="tab-pane tab-pane-slide" id="slide35" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T02-D-V001/img/Slide35.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide36" role="tabpanel">


    <div class="checkpoint">
      <h2>
        Challenge <br>
        <span class="challange-step">Step 3</span>
      </h2>

      <ol class="actions">
        <li data-index="0">
          <span class="fa fa-check "></span>
          Add another <span class="html-code">&lt;p&gt;</span> School Name in it. <br>

          <span class="html-code">
            &nbsp;&lt;p&gt;School: Sandringham Secondarry2006 &lt;/p&gt;
          </span>
        </li>
      </ol>
      <a class="btn btn-primary next check" style="toXp:65%;">Skip this →</a>


    </div>

  </div>


  <div class="tab-pane tab-pane-slide" id="slide37" role="tabpanel">

    <div class="checkpoint">
      <h2>
        Challenge <br>
        <span class="challange-step">Step 4</span>
      </h2>

      <ol class="actions">
        <li data-index="0">
          <span class="fa fa-check "></span>
          Add one last <span class="html-code">&lt;p&gt;</span> with what garde you are in. <br>

          <span class="html-code">
            &nbsp;&lt;p&gt;Grade: 9 &lt;/p&gt;
          </span>
        </li>
      </ol>
      <a class="btn btn-primary next check" style="toXp:65%;">Skip this →</a>


    </div>

  </div>

  <div class="tab-pane tab-pane-slide" id="slide38" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T02-D-V001/img/Slide38.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide39" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T02-D-V001/img/Slide39.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide40" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T02-D-V001/img/Slide40.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide41" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T02-D-V001/img/Slide41.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide42" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T02-D-V001/img/Slide42.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide43" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T02-D-V001/img/Slide43.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide44" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T02-D-V001/img/Slide44.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide45" role="tabpanel">
    <img src="../learn/lessons/P002-T02-D-V001/img/Slide45.PNG">
    <a href="../learn/P2Training3" class="btn btn-primary" style="top:65%;">Start next training →</a>
  </div>
              
` 

/// Add custom JS for lesson below here