var file_lesson_data = 
{
    "defaultCode": "", // default code, if user has not already started coding
    "kbLayout": "", // not currently in use
    "loadJS": "", // not currently in use
    "prevLessonID": "P1Training1", // Lesson ID of previous lesson where to load user's code
    "nextLessonSlug": "", //not currently in use
    "pageDesc": "Learn how to build your first website with these easy intro lessons to coding.", 
    "pageKeywords": "coding, code, learn to code, code website, my first website", 
    "pageTitle": "CodeJIKA - Project 1, Training 2",
    "total_slides": 55, // set how many slides in total
    "save_lesson_id": "P1Training2", // This is id that will be used to store save usercode in Firebase
    "slides": [ 
    ], // not currently in use
    "slug": "" // not currently in use
}

var check_points = {
  12:"(<style>|<style [^>]*>)((.|\n)*)\s*<\/style>",
  22:"(<style>|<style [^>]*>)((.|\n)*)\s*h1((.|\n)*)\s*{((.|\n)*)\s*}((.|\n)*)\s*<\/style>",
  23:"(<style>|<style [^>]*>)((.|\n)*)\s*h1((.|\n)*)\s*{((.|\n)*)\s*font-size((.|\n)*)\s*75px;((.|\n)*)\s*}((.|\n)*)\s*<\/style>",
  27:"(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>((.|\n)*)\s*Motivation",
  28:"(<h3>|<h3 [^>]*>)((.|\n)*)\s*(Motivation|MOTIVATION)((.|\n)*)<\/h3>",
  //28:"<h3>|<h3 [^>]*>)((.|\n)*)(\s*Motivation)((.|\n)*)\s*<\/h3>",
  31:"(<h3>|<h3 [^>]*>)((.|\n)*)\s*Motivation((.|\n)*)\s*<\/h3>",
  37:"(<h3>|<h3 [^>]*>)((.|\n)*)\s*Motivation((.|\n)*)\s*(<i>|<i [^>]*>)((.|\n)*)\s*<\/i>((.|\n)*)\s*<\/h3>",
  39:"(<style>|<style [^>]*>)((.|\n)*)\s*i((.|\n)*)\s*{((.|\n)*)\s*font-size((.|\n)*)\s*:((.|\n)*)\s*25px((.|\n)*)\s*;((.|\n)*)\s*}((.|\n)*)\s*<\/style>",
  44:"(<h3>|<h3 [^>]*>)((.|\n)*)\s*(Motivation:|Motivation)((.|\n)*)\s*(<br>|<br [^>]*>)((.|\n)*)\s*(<br>|<br [^>]*>)((.|\n)*)\s*<\/h3>"
}

var hintsForCheckPonts = {
  /* 12:"Inside the &lt;head&gt; <strong>Type Code Here</strong> &lt;\/head&gt; tags",
  22:"Inside the &lt;style&gt; <strong>Type Code Here</strong> &lt;\/style&gt; section",
  23:"Remember to wrap your content inside the curley braces h1<strong>{Type Code Here}</strong>" */
}
  
var hints_data =   `

  <p style="margin-bottom:0px;">Slide: 12</p>
    <pre style="margin-bottom:10px; color:#ccc;">&lt;h1&gt;Nomzamo Mbatha&lt;/h1&gt;</pre>
    <p style="margin-bottom:0px;">Slide: 14</p>
    <pre style="margin-bottom:10px;">&lt;p>Hi! I'm Nomzamo Mbatha, An Actress. Say Hello!&lt;/p&gt;</pre>
    <p style="margin-bottom:0px;">Slide: 19</p>
    <pre style="margin-bottom:10px;">&lt;input type="email"&gt;</pre>
    <p style="margin-bottom:0px;">Slide: 23</p>	  
    <pre style="margin-bottom:10px;">&lt;input type="submit" &gt;</pre>
    <p style="margin-bottom:0px;">Slide: 27</p>
    <pre>placeholder="Your email"</pre>

    ` 
  
var slides_data = ` 

  <div class="tab-pane tab-pane-slide active" id="slide1" role="tabpanel">
    <img src="/learn/lessons/P001-T02-D-V001/img/Slide1.PNG">
    <a class="btn btn-primary next" style="top:65%;">Start Slideshow →</a>
  </div>
  <div class="tab-pane tab-pane-slide" id="slide2" role="tabpanel">
    <img src="/learn/lessons/P001-T02-D-V001/img/Slide2.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide3" role="tabpanel">
    <img src="/learn/lessons/P001-T02-D-V001/img/Slide3.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide4" role="tabpanel">
    <img src="/learn/lessons/P001-T02-D-V001/img/Slide4.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide5" role="tabpanel">
    <img src="/learn/lessons/P001-T02-D-V001/img/Slide5.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide6" role="tabpanel">
    <img src="/learn/lessons/P001-T02-D-V001/img/Slide6.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide7" role="tabpanel">
    <img src="/learn/lessons/P001-T02-D-V001/img/Slide7.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide8" role="tabpanel">
    <img src="/learn/lessons/P001-T02-D-V001/img/Slide8.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide9" role="tabpanel">
    <img src="/learn/lessons/P001-T02-D-V001/img/Slide9.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide10" role="tabpanel">
    <img class="lozad" data-src="/learn/lessons/P001-T02-D-V001/img/Slide10.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide11" role="tabpanel">
    <img class="lozad" data-src="/learn/lessons/P001-T02-D-V001/img/Slide11.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide12" role="tabpanel">

    <div class="checkpoint">
      <h2>
        Challenge
      </h2>

      <ol class="actions">
        <li data-index="0">
          <i class="far fa-circle square-icon"></i>
          <span class="fa fa-check"></span>

          Add a <b>&lt;style&gt;</b> section.
        </li>
      </ol>
      <a class="btn btn-primary next check" style="toXp:65%;">Skip this →</a>

      <div class="challenge-tip"><span>Tip:</span>
        Remember to close <b>&lt;style&gt;</b> tag.
      </div>
    </div>
  </div>

  <div class="tab-pane tab-pane-slide" id="slide13" role="tabpanel">
    <img class="lozad" data-src="/learn/lessons/P001-T02-D-V001/img/Slide13.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide14" role="tabpanel">
    <img class="lozad" data-src="/learn/lessons/P001-T02-D-V001/img/Slide14.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide15" role="tabpanel">
    <img class="lozad" data-src="/learn/lessons/P001-T02-D-V001/img/Slide15.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide16" role="tabpanel">
    <img class="lozad" data-src="/learn/lessons/P001-T02-D-V001/img/Slide16.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide17" role="tabpanel">
    <img class="lozad" data-src="/learn/lessons/P001-T02-D-V001/img/Slide17.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide18" role="tabpanel">
    <img class="lozad" data-src="/learn/lessons/P001-T02-D-V001/img/Slide18.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide19" role="tabpanel">
    <img class="lozad" data-src="/learn/lessons/P001-T02-D-V001/img/Slide19.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide20" role="tabpanel">
    <img class="lozad" data-src="/learn/lessons/P001-T02-D-V001/img/Slide20.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide21" role="tabpanel">
    <img class="lozad" data-src="/learn/lessons/P001-T02-D-V001/img/Slide21.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide22" role="tabpanel">

    <div class="checkpoint">
      <h2>
        Challenge
      </h2>
      <span class="challange-step" style="color:black;"> Step 1</span>
      <ol class="actions">
        <li data-index="0">
          <i class="far fa-circle square-icon"></i>
          <span class="fa fa-check"></span>

          Add a <b>h1</b> selector.
        </li>
      </ol>
      <a class="btn btn-primary next check" style="toXp:65%;">Skip this →</a>

      <div class="challenge-tip"><span>Tip:</span>
        In &lt;style&gt; section.
      </div>
    </div>
  </div>


  <div class="tab-pane tab-pane-slide" id="slide23" role="tabpanel">

    <div class="checkpoint">
      <h2>
        Challenge
      </h2>
      <span class="challange-step" style="color:black;"> Step 2</span>
      <ol class="actions">
        <li data-index="0">
          <i class="far fa-circle square-icon"></i>
          <span class="fa fa-check"></span>

          Add a <b>font-size: 75px;</b>
        </li>
      </ol>
      <a class="btn btn-primary next check" style="toXp:65%;">Skip this →</a>

      <div class="challenge-tip"><span>Tip:</span>
        Between the curly brackets.
      </div>
    </div>
  </div>

  <div class="tab-pane tab-pane-slide" id="slide24" role="tabpanel">
    <img class="lozad" data-src="/learn/lessons/P001-T02-D-V001/img/Slide24.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide25" role="tabpanel">
    <img class="lozad" data-src="/learn/lessons/P001-T02-D-V001/img/Slide25.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide26" role="tabpanel">
    <img class="lozad" data-src="/learn/lessons/P001-T02-D-V001/img/Slide26.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide27" role="tabpanel">

    <div class="checkpoint">
      <h2>
        Challenge
      </h2>
      <span class="challange-step" style="color:black;"> Step 1</span>
      <ol class="actions">
        <li data-index="0">
          <i class="far fa-circle square-icon"></i>
          <span class="fa fa-check"></span>

          Type: <b>Motivation:</b> <br>Place it below the &lt;h1&gt; tags

        </li>
      </ol>
      <a class="btn btn-primary next check" style="toXp:65%;">Skip this →</a>

    </div>
  </div>


  <div class="tab-pane tab-pane-slide" id="slide28" role="tabpanel">


    <div class="checkpoint">
      <h2>
        Challenge
      </h2>
      <span class="challange-step" style="color:black;"> Step 2</span>
      <ol class="actions">
        <li data-index="0">
          <i class="far fa-circle square-icon"></i>
          <span class="fa fa-check"></span>

          Wrap it in &lt;h3&gt; tags, <br> &lt;h3&gt;<br>&nbsp;&nbsp;Motivation: <br>&lt;/h3&gt;

        </li>
      </ol>
      <a class="btn btn-primary next check" style="toXp:65%;">Skip this →</a>

      <div class="challenge-tip"><span>Tip:</span>
        Between the &lt;body&gt; tags.
      </div>
    </div>
  </div>

  <div class="tab-pane tab-pane-slide" id="slide29" role="tabpanel">
    <img class="lozad" data-src="/learn/lessons/P001-T02-D-V001/img/Slide29.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide30" role="tabpanel">
    <img class="lozad" data-src="/learn/lessons/P001-T02-D-V001/img/Slide30.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide31" role="tabpanel">

    <div class="checkpoint">
      <h2>
        Challenge
      </h2>
      <span class="challange-step" style="color:black;"> Step 1</span>
      <ol class="actions">
        <li data-index="0">
          <i class="far fa-circle square-icon"></i>
          <span class="fa fa-check"></span>

          Type a <u>short sentence</u> about why you are learning coding.

        </li>
      </ol>
      <a class="btn btn-primary next check" style="toXp:65%;">Skip this →</a>

      <div class="challenge-tip"><span>Where:</span>
        Below “Motivation” inside &lt;h3&gt; tags.
      </div>
    </div>
  </div>

  <div class="tab-pane tab-pane-slide" id="slide32" role="tabpanel">
    <img class="lozad" data-src="/learn/lessons/P001-T02-D-V001/img/Slide32.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide33" role="tabpanel">
    <img class="lozad" data-src="/learn/lessons/P001-T02-D-V001/img/Slide33.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide34" role="tabpanel">
    <img class="lozad" data-src="/learn/lessons/P001-T02-D-V001/img/Slide34.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide35" role="tabpanel">
    <img class="lozad" data-src="/learn/lessons/P001-T02-D-V001/img/Slide35.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide36" role="tabpanel">
    <img class="lozad" data-src="/learn/lessons/P001-T02-D-V001/img/Slide36.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide37" role="tabpanel">

    <div class="checkpoint">
      <h2>
        Challenge
      </h2>

      <ol class="actions">
        <li data-index="0">
          <i class="far fa-circle square-icon"></i>
          <span class="fa fa-check"></span>

          Code: Put your sentence into <i>italics</i> with the <b>&lt;i&gt;</b> tag.<br>
          &lt;h3&gt; <br>&nbsp;&nbsp;&nbsp;Motivation:<br>
          &lt;i&gt; <br>&nbsp;&nbsp;&nbsp;Sentence <br>&lt;i&gt; <br>
          &lt;/h3&gt;

        </li>
      </ol>
      <a class="btn btn-primary next check" style="toXp:65%;">Skip this →</a>


    </div>
  </div>

  <div class="tab-pane tab-pane-slide" id="slide38" role="tabpanel">
    <img class="lozad" data-src="/learn/lessons/P001-T02-D-V001/img/Slide38.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide39" role="tabpanel">

    <div class="checkpoint">
      <h2>
        Challenge
      </h2>

      <ol class="actions">
        <li data-index="0">
          <i class="far fa-circle square-icon"></i>
          <span class="fa fa-check"></span>

          Using CSS reduce the size of the italics text to 25px.

        </li>
      </ol>
      <a class="btn btn-primary next check" style="toXp:65%;">Skip this →</a>

      <div class="challenge-tip"><span>Tip:</span>
        Start with i{ } in style.
      </div>
    </div>
  </div>

  <div class="tab-pane tab-pane-slide" id="slide40" role="tabpanel">
    <img class="lozad" data-src="/learn/lessons/P001-T02-D-V001/img/Slide40.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide41" role="tabpanel">
    <img class="lozad" data-src="/learn/lessons/P001-T02-D-V001/img/Slide41.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide42" role="tabpanel">
    <img class="lozad" data-src="/learn/lessons/P001-T02-D-V001/img/Slide42.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide43" role="tabpanel">
    <img class="lozad" data-src="/learn/lessons/P001-T02-D-V001/img/Slide43.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide44" role="tabpanel">

    <div class="checkpoint">
      <h2>
        Challenge
      </h2>

      <ol class="actions">
        <li data-index="0">
          <i class="far fa-circle square-icon"></i>
          <span class="fa fa-check"></span>

          Add <b>&lt;br&gt;</b> <u>twice</u> immediately after <b>MOTIVATION:</b><br>
          &lt;br&gt;<br>
          &lt;br&gt;

        </li>
      </ol>
      <a class="btn btn-primary next check" style="toXp:65%;">Skip this →</a>

    </div>
  </div>

  <div class="tab-pane tab-pane-slide" id="slide45" role="tabpanel">
    <img class="lozad" data-src="/learn/lessons/P001-T02-D-V001/img/Slide45.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide46" role="tabpanel">
    <img class="lozad" data-src="/learn/lessons/P001-T02-D-V001/img/Slide46.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide47" role="tabpanel">
    <img class="lozad" data-src="/learn/lessons/P001-T02-D-V001/img/Slide47.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide48" role="tabpanel">
    <img class="lozad" data-src="/learn/lessons/P001-T02-D-V001/img/Slide48.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide49" role="tabpanel">
    <img class="lozad" data-src="/learn/lessons/P001-T02-D-V001/img/Slide49.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide50" role="tabpanel">
    <img class="lozad" data-src="/learn/lessons/P001-T02-D-V001/img/Slide50.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide51" role="tabpanel">
    <img class="lozad" data-src="/learn/lessons/P001-T02-D-V001/img/Slide51.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide52" role="tabpanel">
    <img class="lozad" data-src="/learn/lessons/P001-T02-D-V001/img/Slide52.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide53" role="tabpanel">
    <img class="lozad" data-src="/learn/lessons/P001-T02-D-V001/img/Slide53.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide54" role="tabpanel">
    <img class="lozad" data-src="/learn/lessons/P001-T02-D-V001/img/Slide54.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide55" role="tabpanel">
    <img class="lozad" data-src="/learn/lessons/P001-T02-D-V001/img/Slide55.PNG">
    <a href="/learn/P1Training3" class="btn btn-primary" style="top:65%;">Start next training →</a>
  </div>
                       
` 

/// Add custom JS for lesson below here