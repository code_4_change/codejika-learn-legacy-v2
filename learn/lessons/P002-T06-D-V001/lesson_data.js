var file_lesson_data = 
{
    "defaultCode": "", // default code, if user has not already started coding
    // "kbLayout": "", //not currently in use
    // "loadJS": "", //not currently in use
    // "nextLessonSlug": "", //not currently in use
    "pageDesc": "Learn how to build your first website with these easy intro lessons to coding.", 
    "pageKeywords": "coding, code, learn to code, code website, my first website", 
    "pageTitle": "CodeJIKA - Project 2, Training 6",
    "total_slides": 46,
    "save_lesson_id": "P2Training6", // This is id that will be used to store save usercode in Firebase
    // "slides": [  //not currently in use
    // ],
    // "slug": "" //not currently in use
}

var check_points = {
   12:"((.|\n)*)\s*<div((.|\n)*)\s*class=\"((.|\n)*)\s*circle((.|\n)*)\s*\"((.|\n)*)\s*>((.|\n)*)\s*<div((.|\n)*)\s*>((.|\n)*)\s*((.|\n)*)\s*<\/div((.|\n)*)\s*>((.|\n)*)\s*<\/div((.|\n)*)\s*>((.|\n)*)\s*",
   13:"((.|\n)*)\s*<div((.|\n)*)\s*class=\"((.|\n)*)\s*circle((.|\n)*)\s*\"((.|\n)*)\s*>((.|\n)*)\s*<div((.|\n)*)\s*>((.|\n)*)\s*&#x1F60E((.|\n)*)\s*<\/div((.|\n)*)\s*>((.|\n)*)\s*<\/div((.|\n)*)\s*>((.|\n)*)\s*",
   17:"((.|\n)\s*)<style>((.|\n)*)\s*(.emoji)((.|\n)*)\s*{((.|\n)*)\s*(font-size)((.|\n)*)\s*:((.|\n)*)\s*(8em)((.|\n)*)\s*}((.|\n)*)\s*<\/style>((.|\n)*)\s*",
   18:"((.|\n)*)\s*<div((.|\n)*)\s*class=\"((.|\n)*)\s*circle((.|\n)*)\s*\"((.|\n)*)\s*>((.|\n)*)\s*<div((.|\n)*)\s*class=\"((.|\n)*)\s*emoji((.|\n)*)\s*\"((.|\n)*)\s*>((.|\n)*)\s*&#x1F60E((.|\n)*)\s*<\/div((.|\n)*)\s*>((.|\n)*)\s*<\/div((.|\n)*)\s*>((.|\n)*)\s*",
   23:"((.|\n)\s*)<h3>((.|\n)*)\s*(&#x1F60E)((.|\n)*)\s*(DETAILS)((.|\n)*)\s*<\/h3>((.|\n)*)\s*<p>((.|\n)*)\s*<\/p>((.|\n)*)\s*",
   30:"((.|\n)\s*)<style>((.|\n)*)\s*(p)((.|\n)*)\s*(,)((.|\n)*)\s*(ul)((.|\n)*)\s*{((.|\n)*)\s*(list-style)((.|\n)*)\s*:((.|\n)*)\s*(none)((.|\n)*)\s*;((.|\n)*)\s*(padding-left)((.|\n)*)\s*:((.|\n)*)\s*(50px)((.|\n)*)\s*;((.|\n)*)\s*(font-size)((.|\n)*)\s*:((.|\n)*)\s*(1.3em)((.|\n)*)\s*;((.|\n)*)\s*}((.|\n)*)\s*<\/style>((.|\n)*)\s*",
   37:"((.|\n)*)\s*<br>((.|\n)*)\s*(&copy)((.|\n)*)\s*<\/body>((.|\n)*)\s*",
 
}

var hintsForCheckPonts = {
  /* 11:"Type your code below the inside the <strong>header{ }</strong> selector in style.", */
}
  
var hints_data =   `

  <p style="margin-bottom:0px;">Slide: 12</p>
    <pre style="margin-bottom:10px; color:#ccc;">&lt;h1&gt;Nomzamo Mbatha&lt;/h1&gt;</pre>
    <p style="margin-bottom:0px;">Slide: 14</p>
    <pre style="margin-bottom:10px;">&lt;p>Hi! I'm Nomzamo Mbatha, An Actress. Say Hello!&lt;/p&gt;</pre>
    <p style="margin-bottom:0px;">Slide: 19</p>
    <pre style="margin-bottom:10px;">&lt;input type="email"&gt;</pre>
    <p style="margin-bottom:0px;">Slide: 23</p>	  
    <pre style="margin-bottom:10px;">&lt;input type="submit" &gt;</pre>
    <p style="margin-bottom:0px;">Slide: 27</p>
    <pre>placeholder="Your email"</pre>

    ` 
  
var slides_data = ` 

  <div class="tab-pane tab-pane-slide active" id="slide1" role="tabpanel">
    <img src="../learn/lessons/P002-T06-D-V001/img/Slide1.PNG">
    <a class="btn btn-primary next" style="top:65%;">Start Slideshow →</a>
  </div>
  <div class="tab-pane tab-pane-slide" id="slide2" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T06-D-V001/img/Slide2.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide3" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T06-D-V001/img/Slide3.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide4" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T06-D-V001/img/Slide4.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide5" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T06-D-V001/img/Slide5.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide6" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T06-D-V001/img/Slide6.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide7" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T06-D-V001/img/Slide7.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide8" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T06-D-V001/img/Slide8.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide9" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T06-D-V001/img/Slide9.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide10" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T06-D-V001/img/Slide10.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide11" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T06-D-V001/img/Slide11.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide12" role="tabpanel">


    <div class="checkpoint">
      <h2>
        Challenge <br>
        <span class="challange-step"> Step 1 of 4</span>
      </h2>

      <ol class="actions">
        <li data-index="0">
          <span class="fa fa-check "></span>
          Add a <u>new</u> div.<br>
          <span class="html-code">
            &nbsp;&nbsp; &lt;header&gt; <br>
            &nbsp;&nbsp;&nbsp;&nbsp; &lt;div class="circle"&gt; <br>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &lt;div&gt; <br>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &lt;/div&gt; <br>
            &nbsp;&nbsp;&nbsp;&nbsp; &lt;/div&gt; <br>
            &nbsp;&nbsp; &lt;header&gt;
          </span>
        </li>
      </ol>
      <a class="btn btn-primary next check" style="toXp:65%;">Skip this →</a>
      <div class="challenge-tip"><span>Where:</span>
        <u>Inside</u> the &lt;div&gt; in the &lt;header&gt;.
      </div>
    </div>

  </div>


  <div class="tab-pane tab-pane-slide" id="slide13" role="tabpanel">


    <div class="checkpoint">
      <h2>
        Challenge <br>
        <span class="challange-step"> Step 2 of 4</span>
      </h2>

      <ol class="actions">
        <li data-index="0">
          <span class="fa fa-check "></span>
          Place an emoji into the new &lt;div&gt;.<br>
          Use this one: &amp;#x1F60E; <br>
          <span class="html-code">
            &nbsp;&nbsp; &lt;header&gt; <br>
            &nbsp;&nbsp;&nbsp;&nbsp; &lt;div class="circle"&gt; <br>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &lt;div&gt; <br>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>&amp;#x1F60E; </strong><br>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &lt;/div&gt; <br>
            &nbsp;&nbsp;&nbsp;&nbsp; &lt;/div&gt; <br>
            &nbsp;&nbsp; &lt;header&gt;
          </span>
        </li>
      </ol>
      <a class="btn btn-primary next check" style="toXp:65%;">Skip this →</a>
    </div>

  </div>

  <div class="tab-pane tab-pane-slide" id="slide14" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T06-D-V001/img/Slide14.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide15" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T06-D-V001/img/Slide15.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide16" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T06-D-V001/img/Slide16.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide17" role="tabpanel">


    <div class="checkpoint">
      <h2>
        Challenge <br>
        <span class="challange-step"> Step 3 of 4</span>
      </h2>

      <ol class="actions">
        <li data-index="0">
          <span class="fa fa-check "></span>
          Create a new CSS Class: .emoji <br>
          and increase the font size to 8em. <br>
          <span class="html-code">
            .emoji { <br>
            font-size: 8em; <br>
            }
          </span>
        </li>
      </ol>
      <a class="btn btn-primary next check" style="toXp:65%;">Skip this →</a>

    </div>

  </div>


  <div class="tab-pane tab-pane-slide" id="slide18" role="tabpanel">


    <div class="checkpoint">
      <h2>
        Challenge <br>
        <span class="challange-step"> Step 4 of 4</span>
      </h2>

      <ol class="actions">
        <li data-index="0">
          <span class="fa fa-check "></span>
          Link the .emoji class to the <br>
          new &lt;div&gt; <br>
          <span class="html-code">
            &nbsp;&nbsp; &lt;header&gt; <br>
            &nbsp;&nbsp;&nbsp;&nbsp; &lt;div <strong>class="circle"</strong>&gt; <br>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &lt;div <strong>class="emoji"</strong>&gt; <br>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &amp;#x1F60E;<br>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &lt;/div&gt; <br>
            &nbsp;&nbsp;&nbsp;&nbsp; &lt;/div&gt; <br>
            &nbsp;&nbsp; &lt;header&gt;
          </span>
        </li>
      </ol>
      <a class="btn btn-primary next check" style="toXp:65%;">Skip this →</a>

    </div>

  </div>

  <div class="tab-pane tab-pane-slide" id="slide19" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T06-D-V001/img/Slide19.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide20" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T06-D-V001/img/Slide20.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide21" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T06-D-V001/img/Slide21.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide22" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T06-D-V001/img/Slide22.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide23" role="tabpanel">


    <div class="checkpoint">
      <h2>
        Challenge <br>
        <span class="challange-step"> Step 1 of 2</span>
      </h2>

      <ol class="actions">
        <li data-index="0">
          <span class="fa fa-check "></span>
          Insert an emoji before your h3 text <br>
          <span class="html-code">
            &nbsp;&nbsp; &lt;h3&gt; <br>
            &nbsp;&nbsp;&nbsp; <strong>&amp;#x1F60E;</strong> DETAILS<br>
            &nbsp;&nbsp; &lt;/h3&gt; <br>
          </span>
        </li>
      </ol>
      <a class="btn btn-primary next check" style="toXp:65%;">Skip this →</a>

    </div>

  </div>

  <div class="tab-pane tab-pane-slide" id="slide24" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T06-D-V001/img/Slide24.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide25" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T06-D-V001/img/Slide25.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide26" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T06-D-V001/img/Slide26.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide27" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T06-D-V001/img/Slide27.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide28" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T06-D-V001/img/Slide28.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide29" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T06-D-V001/img/Slide29.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide30" role="tabpanel">


    <div class="checkpoint">
      <h2>
        Challenge <br>
        <span class="challange-step"> Step 1 of 2</span>
      </h2>

      <ol class="actions">
        <li data-index="0">
          <span class="fa fa-check "></span>
          Add list-style: none; to the p, ul CSS <br>
          This removes the black bullet-points. <br>
          <span class="html-code">
            p, ul { <br>
            list-style: none; <br>
            padding-left: 50px; <br>
            font-size: 1.3em; <br>
            }
          </span>
        </li>
      </ol>
      <a class="btn btn-primary next check" style="toXp:65%;">Skip this →</a>

    </div>

  </div>

  <div class="tab-pane tab-pane-slide" id="slide31" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T06-D-V001/img/Slide31.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide32" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T06-D-V001/img/Slide32.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide33" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T06-D-V001/img/Slide33.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide34" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T06-D-V001/img/Slide34.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide35" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T06-D-V001/img/Slide35.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide36" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T06-D-V001/img/Slide36.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide37" role="tabpanel">


    <div class="checkpoint">
      <h2>
        Challenge <br>
        <span class="challange-step"> Step 1 of 2</span>
      </h2>

      <ol class="actions">
        <li data-index="0">
          <span class="fa fa-check "></span>
          Add a copyright line at the end of <br>
          your website right before &lt;/body&gt;. <br>
          <span class="html-code">
            <br> &lt;br&gt;
            © Copyright 2021 Thandi Ndlovu Inc.
          </span>
        </li>
      </ol>
      <a class="btn btn-primary next check" style="toXp:65%;">Skip this →</a>

    </div>

  </div>

  <div class="tab-pane tab-pane-slide" id="slide38" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T06-D-V001/img/Slide38.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide39" role="tabpanel">


    <div class="checkpoint">
      <h2>
        Challenge <br>
        <span class="challange-step"> Step 2 of 2</span>
      </h2>

      <ol class="actions">
        <li data-index="0">
          <span class="fa fa-check "></span>
          Insert an emoji before your h3 text <br>
          <span class="html-code">
            &nbsp;&nbsp; &lt;center&gt; <br>
            &nbsp;&nbsp;&nbsp; &lt;br&gt; <br>
            &nbsp;&nbsp;&nbsp; ©Copyright 2021... <br>
            &nbsp;&nbsp; &lt;/center&gt; <br>
          </span>
        </li>
      </ol>
      <a class="btn btn-primary next check" style="toXp:65%;">Skip this →</a>

    </div>

  </div>

  <div class="tab-pane tab-pane-slide" id="slide40" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T06-D-V001/img/Slide40.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide41" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T06-D-V001/img/Slide41.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide42" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T06-D-V001/img/Slide42.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide43" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T06-D-V001/img/Slide43.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide44" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T06-D-V001/img/Slide44.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide45" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T06-D-V001/img/Slide45.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide46" role="tabpanel">

    <a href="../learn/P3Training1" class="btn btn-primary" style="top:65%;">Start next training →</a>
  </div>

                       
` 

/// Add custom JS for lesson below here