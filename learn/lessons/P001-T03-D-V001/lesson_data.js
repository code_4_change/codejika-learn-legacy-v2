var file_lesson_data = 
{
    "defaultCode": "", // default code, if user has not already started coding
    "kbLayout": "", //not currently in use
    "loadJS": "", //not currently in use
    "nextLessonSlug": "", //not currently in use
    "pageDesc": "Learn how to build your first website with these easy intro lessons to coding.", 
    "pageKeywords": "coding, code, learn to code, code website, my first website", 
    "pageTitle": "CodeJIKA - Project 1, Training 3",
    "total_slides": 56, // set how many slides in total
    "save_lesson_id": "P1Training3", // This is id that will be used to store save usercode in Firebase
    "slides": [ 
    ], //not currently in use
    "slug": "" //not currently in use
}

var check_points = {
  18:"(<header>|<header [^>]*>)((.|\n)*)\s*(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>((.|\n)*)\s*<\/header>",
  24:"(<style>|<style [^>]*>)((.|\n)*)\s*header((.|\n)*)\s*{((.|\n)*)\s*}((.|\n)*)\s*<\/style>",
  25:"(<style>|<style [^>]*>)((.|\n)*)\s*header((.|\n)*)\s*{((.|\n)*)\s*background((.|\n)*)\s*:((.|\n)*)\s*linear-gradient((.|\n)*)\s*}((.|\n)*)\s*<\/style>",
  26:"(<style>|<style [^>]*>)((.|\n)*)\s*header((.|\n)*)\s*{((.|\n)*)\s*background((.|\n)*)\s*:((.|\n)*)\s*linear-gradient((.|\n)*)\s*[(]100deg((.|\n)*)\s*,((.|\n)*)\s*yellow((.|\n)*)\s*40%((.|\n)*)\s*,((.|\n)*)\s*pink((.|\n)*)\s*40%((.|\n)*)\s*[)]((.|\n)*)\s*;((.|\n)*)\s*}((.|\n)*)\s*<\/style>",
  34:"(<section>|<section [^>]*>)((.|\n)*)\s*(<h3>|<h3 [^>]*>)((.|\n)*)\s*motivation((.|\n)*)\s*<\/h3>((.|\n)*)\s*<\/section>",
  35:"(<style>|<style [^>]*>)((.|\n)*)section((.|\n)*)\s*{((.|\n)*)\s*(background|background-color)((.|\n)*)\s*:((.|\n)*)\s*lightgrey((.|\n)*)\s*<\/style>",
  39:"(<footer>|<footer [^>]*>)((.|\n)*)\s*<\/footer>",
  42:"(<footer>|<footer [^>]*>)((.|\n)*)\s*&copy((.|\n)*)\s*<\/footer>",
  44:"(<style>|<style [^>]*>)((.|\n)*)footer((.|\n)*)\s*{((.|\n)*)\s*background((.|\n)*)\s*black((.|\n)*)\s*color((.|\n)*)\s*white((.|\n)*)\s*}((.|\n)*)\s*<\/style>"
}

var hintsForCheckPonts = {
/*   18:"Open it before the h1 tag and close it after the closing p tag" */
} 

  
var hints_data =   `

  <p style="margin-bottom:0px;">Slide: 12</p>
    <pre style="margin-bottom:10px; color:#ccc;">&lt;h1&gt;Nomzamo Mbatha&lt;/h1&gt;</pre>
    <p style="margin-bottom:0px;">Slide: 14</p>
    <pre style="margin-bottom:10px;">&lt;p>Hi! I'm Nomzamo Mbatha, An Actress. Say Hello!&lt;/p&gt;</pre>
    <p style="margin-bottom:0px;">Slide: 19</p>
    <pre style="margin-bottom:10px;">&lt;input type="email"&gt;</pre>
    <p style="margin-bottom:0px;">Slide: 23</p>	  
    <pre style="margin-bottom:10px;">&lt;input type="submit" &gt;</pre>
    <p style="margin-bottom:0px;">Slide: 27</p>
    <pre>placeholder="Your email"</pre>

    ` 
  
var slides_data = ` 

  <div class="tab-pane tab-pane-slide active" id="slide1" role="tabpanel">
    <img src="../learn/lessons/P001-T03-D-V001/img/Slide1.PNG">
    <a class="btn btn-primary next" style="top:65%;">Start Slideshow →</a>
  </div>
  <div class="tab-pane tab-pane-slide" id="slide2" role="tabpanel">
    <img src="../learn/lessons/P001-T03-D-V001/img/Slide2.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide3" role="tabpanel">
    <img src="../learn/lessons/P001-T03-D-V001/img/Slide3.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide4" role="tabpanel">
    <img src="../learn/lessons/P001-T03-D-V001/img/Slide4.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide5" role="tabpanel">
    <img src="../learn/lessons/P001-T03-D-V001/img/Slide5.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide6" role="tabpanel">
    <img src="../learn/lessons/P001-T03-D-V001/img/Slide6.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide7" role="tabpanel">
    <img src="../learn/lessons/P001-T03-D-V001/img/Slide7.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide8" role="tabpanel">
    <img src="../learn/lessons/P001-T03-D-V001/img/Slide8.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide9" role="tabpanel">
    <img src="../learn/lessons/P001-T03-D-V001/img/Slide9.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide10" role="tabpanel">
    <img class="lozad" data-src="../learn/lessons/P001-T03-D-V001/img/Slide10.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide11" role="tabpanel">
    <img class="lozad" data-src="../learn/lessons/P001-T03-D-V001/img/Slide11.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide12" role="tabpanel">
    <img class="lozad" data-src="../learn/lessons/P001-T03-D-V001/img/Slide12.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide13" role="tabpanel">
    <img class="lozad" data-src="../learn/lessons/P001-T03-D-V001/img/Slide13.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide14" role="tabpanel">
    <img class="lozad" data-src="../learn/lessons/P001-T03-D-V001/img/Slide14.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide15" role="tabpanel">
    <img class="lozad" data-src="../learn/lessons/P001-T03-D-V001/img/Slide15.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide16" role="tabpanel">
    <img class="lozad" data-src="../learn/lessons/P001-T03-D-V001/img/Slide16.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide17" role="tabpanel">
    <img class="lozad" data-src="../learn/lessons/P001-T03-D-V001/img/Slide17.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide18" role="tabpanel">

    <div class="checkpoint">
      <h2>
        Challenge
      </h2>

      <ol class="actions">
        <li data-index="0">
          <i class="far fa-circle square-icon"></i>
          <span class="fa fa-check"></span>

          Insert a <b>&lt;header&gt;</b>.

        </li>
      </ol>
      <a class="btn btn-primary next check" style="toXp:65%;">Skip this →</a>

      <div class="challenge-tip"><span>Where:</span>
        Open it before &lt;h1&gt; and Close it after &lt;/p&gt;.
      </div>
    </div>
  </div>

  <div class="tab-pane tab-pane-slide" id="slide19" role="tabpanel">
    <img class="lozad" data-src="../learn/lessons/P001-T03-D-V001/img/Slide19.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide20" role="tabpanel">
    <img class="lozad" data-src="../learn/lessons/P001-T03-D-V001/img/Slide20.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide21" role="tabpanel">
    <img class="lozad" data-src="../learn/lessons/P001-T03-D-V001/img/Slide21.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide22" role="tabpanel">
    <img class="lozad" data-src="../learn/lessons/P001-T03-D-V001/img/Slide22.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide23" role="tabpanel">
    <img class="lozad" data-src="../learn/lessons/P001-T03-D-V001/img/Slide23.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide24" role="tabpanel">

    <div class="checkpoint">
      <h2>
        Challenge
      </h2>
      <span class="challange-step" style="color:black;"> Step 1</span>
      <ol class="actions">
        <li data-index="0">
          <i class="far fa-circle square-icon"></i>
          <span class="fa fa-check"></span>

          Add a header selector in &lt;style&gt;<br>
          header{ <br>
          &nbsp;}

        </li>
      </ol>
      <a class="btn btn-primary next check" style="toXp:65%;">Skip this →</a>

    </div>
  </div>


  <div class="tab-pane tab-pane-slide" id="slide25" role="tabpanel">

    <div class="checkpoint">
      <h2>
        Challenge
      </h2>
      <span class="challange-step" style="color:black;"> Step 2</span>
      <ol class="actions">
        <li data-index="0">
          <i class="far fa-circle square-icon"></i>
          <span class="fa fa-check"></span>

          Add this <u>first half</u> of the rule:<br>
          header{ <br>
          &nbsp;&nbsp; background: linear-gradient <br>
          &nbsp;}

        </li>
      </ol>
      <a class="btn btn-primary next check" style="toXp:65%;">Skip this →</a>

    </div>
  </div>


  <div class="tab-pane tab-pane-slide" id="slide26" role="tabpanel">

    <div class="checkpoint">
      <h2>
        Challenge
      </h2>
      <span class="challange-step" style="color:black;"> Step 3</span>
      <ol class="actions">
        <li data-index="0">
          <i class="far fa-circle square-icon"></i>
          <span class="fa fa-check"></span>

          Complete the rule with this line:<br>
          header{ <br>
          &nbsp;&nbsp; background: linear-gradient(100deg, yellow 40%, pink 40%); <br>
          &nbsp;}

        </li>
      </ol>
      <a class="btn btn-primary next check" style="toXp:65%;">Skip this →</a>

      <div class="challenge-tip"><span>Tip:</span>
        No space between <b>gradient &amp; (100deg</b>
      </div>
    </div>
  </div>

  <div class="tab-pane tab-pane-slide" id="slide27" role="tabpanel">
    <img class="lozad" data-src="../learn/lessons/P001-T03-D-V001/img/Slide27.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide28" role="tabpanel">
    <img class="lozad" data-src="../learn/lessons/P001-T03-D-V001/img/Slide28.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide29" role="tabpanel">
    <img class="lozad" data-src="../learn/lessons/P001-T03-D-V001/img/Slide29.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide30" role="tabpanel">
    <img class="lozad" data-src="../learn/lessons/P001-T03-D-V001/img/Slide30.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide31" role="tabpanel">
    <img class="lozad" data-src="../learn/lessons/P001-T03-D-V001/img/Slide31.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide32" role="tabpanel">
    <img class="lozad" data-src="../learn/lessons/P001-T03-D-V001/img/Slide32.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide33" role="tabpanel">
    <img class="lozad" data-src="../learn/lessons/P001-T03-D-V001/img/Slide33.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide34" role="tabpanel">

    <div class="checkpoint">
      <h2>
        Challenge
      </h2>
      <span class="challange-step" style="color:black;"> Step 1</span>
      <ol class="actions">
        <li data-index="0">
          <i class="far fa-circle square-icon"></i>
          <span class="fa fa-check"></span>

          Place your “Motivation” &lt;h3&gt; within <b>&lt;section&gt;</b> tags.

        </li>
      </ol>
      <a class="btn btn-primary next check" style="toXp:65%;">Skip this →</a>


    </div>
  </div>


  <div class="tab-pane tab-pane-slide" id="slide35" role="tabpanel">

    <div class="checkpoint">
      <h2>
        Challenge
      </h2>
      <span class="challange-step" style="color:black;"> Step 2</span>
      <ol class="actions">
        <li data-index="0">
          <i class="far fa-circle square-icon"></i>
          <span class="fa fa-check"></span>

          <b>Style &lt;section&gt;</b> with a grey background.<br>
          section{ <br>
          &nbsp;&nbsp; background: lightgrey; <br>
          &nbsp;}

        </li>
      </ol>
      <a class="btn btn-primary next check" style="toXp:65%;">Skip this →</a>


    </div>
  </div>

  <div class="tab-pane tab-pane-slide" id="slide36" role="tabpanel">
    <img class="lozad" data-src="../learn/lessons/P001-T03-D-V001/img/Slide36.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide37" role="tabpanel">
    <img class="lozad" data-src="../learn/lessons/P001-T03-D-V001/img/Slide37.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide38" role="tabpanel">
    <img class="lozad" data-src="../learn/lessons/P001-T03-D-V001/img/Slide38.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide39" role="tabpanel">

    <div class="checkpoint">
      <h2>
        Challenge
      </h2>
      <span class="challange-step" style="color:black;"> Step 1</span>
      <ol class="actions">
        <li data-index="0">
          <i class="far fa-circle square-icon"></i>
          <span class="fa fa-check"></span>
          Insert a <b>&lt;footer&gt;</b> section before the closing <b>&lt;/body&gt;</b> tag.

        </li>
      </ol>
      <a class="btn btn-primary next check" style="toXp:65%;">Skip this →</a>


    </div>
  </div>

  <div class="tab-pane tab-pane-slide" id="slide40" role="tabpanel">
    <img class="lozad" data-src="../learn/lessons/P001-T03-D-V001/img/Slide40.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide41" role="tabpanel">
    <img class="lozad" data-src="../learn/lessons/P001-T03-D-V001/img/Slide41.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide42" role="tabpanel">

    <div class="checkpoint">
      <h2>
        Challenge
      </h2>
      <span class="challange-step" style="color:black;"> Step 2</span>
      <ol class="actions">
        <li data-index="0">
          <i class="far fa-circle square-icon"></i>
          <span class="fa fa-check"></span>
          Add a copyright line inside the <b>&lt;footer&gt;</b>.
          section. <br>
          &amp; copy 2021 My name<br>
          &lt;/footer&gt;
        </li>
      </ol>
      <a class="btn btn-primary next check" style="toXp:65%;">Skip this →</a>

      <div class="challenge-tip"><span>Tip:</span>
        No space between <b>“&amp;” and “copy”</b>
      </div>

    </div>
  </div>

  <div class="tab-pane tab-pane-slide" id="slide43" role="tabpanel">
    <img class="lozad" data-src="../learn/lessons/P001-T03-D-V001/img/Slide43.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide44" role="tabpanel">

    <div class="checkpoint">
      <h2>
        Challenge
      </h2>
      <span class="challange-step" style="color:black;"> Step 3</span>
      <ol class="actions">
        <li data-index="0">
          <i class="far fa-circle square-icon"></i>
          <span class="fa fa-check"></span>
          Using a <b>footer { }</b> selector in CSS, make your footer background black and the text white.
        </li>
      </ol>
      <a class="btn btn-primary next check" style="toXp:65%;">Skip this →</a>


    </div>
  </div>

  <div class="tab-pane tab-pane-slide" id="slide45" role="tabpanel">
    <img class="lozad" data-src="../learn/lessons/P001-T03-D-V001/img/Slide45.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide46" role="tabpanel">
    <img class="lozad" data-src="../learn/lessons/P001-T03-D-V001/img/Slide46.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide47" role="tabpanel">
    <img class="lozad" data-src="../learn/lessons/P001-T03-D-V001/img/Slide47.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide48" role="tabpanel">
    <img class="lozad" data-src="../learn/lessons/P001-T03-D-V001/img/Slide48.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide49" role="tabpanel">
    <img class="lozad" data-src="../learn/lessons/P001-T03-D-V001/img/Slide49.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide50" role="tabpanel">
    <img class="lozad" data-src="../learn/lessons/P001-T03-D-V001/img/Slide50.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide51" role="tabpanel">
    <img class="lozad" data-src="../learn/lessons/P001-T03-D-V001/img/Slide51.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide52" role="tabpanel">
    <img class="lozad" data-src="../learn/lessons/P001-T03-D-V001/img/Slide52.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide53" role="tabpanel">
    <img class="lozad" data-src="../learn/lessons/P001-T03-D-V001/img/Slide53.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide54" role="tabpanel">
    <img class="lozad" data-src="../learn/lessons/P001-T03-D-V001/img/Slide54.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide55" role="tabpanel">
    <img class="lozad" data-src="../learn/lessons/P001-T03-D-V001/img/Slide55.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide56" role="tabpanel">
    <img class="lozad" data-src="../learn/lessons/P001-T03-D-V001/img/Slide56.PNG">
    <a href="../learn/P1Training4" class="btn btn-primary" style="top:65%;">Start next training →</a>
  </div>
                         
` 

/// Add custom JS for lesson below here