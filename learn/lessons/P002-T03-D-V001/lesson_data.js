var file_lesson_data = 
{
    "defaultCode": "", // default code, if user has not already started coding
    "kbLayout": "", //not currently in use
    "loadJS": "", //not currently in use
    "nextLessonSlug": "", //not currently in use
    "pageDesc": "Learn how to build your first website with these easy intro lessons to coding.", 
    "pageKeywords": "coding, code, learn to code, code website, my first website", 
    "pageTitle": "CodeJIKA - Project 2, Training 3",
    "total_slides": 44,
    "save_lesson_id": "P2Training3", // This is id that will be used to store save usercode in Firebase
    "slides": [ 
    ], //not currently in use
    "slug": "" //not currently in use
}

var check_points = {
  11:"((.|\n)\s*)(.box)((.|\n)*)\s*{((.|\n)*)\s*(height)((.|\n)*)\s*:((.|\n)*)\s*250px;((.|\n)*)\s*width((.|\n)*)\s*:((.|\n)*)\s*250px;((.|\n)*)\s*",
  12:"((.|\n)*)\s*(<div((.)*)\s* class=\"box\"((.|\n)*)\s*>)((.|\n)*)\s*(<\/div>)((.|\n)*)\s*(<h1>)",
  14:"((.|\n)\s*)(.box)((.|\n)*)\s*{((.|\n)*)\s*(height)((.|\n)*)\s*:((.|\n)*)\s*250px;((.|\n)*)\s*width((.|\n)*)\s*:((.|\n)*)\s*250px;((.|\n)*)\s*background((.|\n)*)\s*:((.|\n)*)\s*blue;((.|\n)*)\s*",
  24:"((.|\n)*)\s*width((.|\n)*)\s*:((.|\n)*)\s*250px;((.|\n)*)\s*background((.|\n)*)\s*:((.|\n)*)\s*blue;((.|\n)*)\s*border-radius((.|\n)*)\s*:((.|\n)*)\s*50%;((.|\n)*)\s*",
  25:"((.|\n)*)\s*(<div((.)*)\s* class=\"circle\"((.|\n)*)\s*>)((.|\n)*)\s*(<\/div>)((.|\n)*)\s*(<h1>)",
  28:"((.|\n)*)\s*width((.|\n)*)\s*:((.|\n)*)\s*250px;((.|\n)*)\s*background((.|\n)*)\s*:((.|\n)*)\s*blue;((.|\n)*)\s*border-radius((.|\n)*)\s*:((.|\n)*)\s*50%;((.|\n)*)\s*border((.|\n)*)\s*:((.|\n)*)\s*10px((.|\n)*)\s*solid((.|\n)*)\s*white;((.|\n)*)\s*",
  30:"((.|\n)*)\s*width((.|\n)*)\s*:((.|\n)*)\s*250px;((.|\n)*)\s*background((.|\n)*)\s*:((.|\n)*)\s*blue;((.|\n)*)\s*border-radius((.|\n)*)\s*:((.|\n)*)\s*50%;((.|\n)*)\s*border((.|\n)*)\s*:((.|\n)*)\s*10px((.|\n)*)\s*solid((.|\n)*)\s*white;((.|\n)*)\s*display((.|\n)*)\s*:((.|\n)*)\s*inline-block;((.|\n)*)\s*"
}

var hintsForCheckPonts = {
 /*  11:"Type your code below the inside the <strong>header{ }</strong> selector in style." */
}
  
var hints_data =   `

  <p style="margin-bottom:0px;">Slide: 12</p>
    <pre style="margin-bottom:10px; color:#ccc;">&lt;h1&gt;Nomzamo Mbatha&lt;/h1&gt;</pre>
    <p style="margin-bottom:0px;">Slide: 14</p>
    <pre style="margin-bottom:10px;">&lt;p>Hi! I'm Nomzamo Mbatha, An Actress. Say Hello!&lt;/p&gt;</pre>
    <p style="margin-bottom:0px;">Slide: 19</p>
    <pre style="margin-bottom:10px;">&lt;input type="email"&gt;</pre>
    <p style="margin-bottom:0px;">Slide: 23</p>	  
    <pre style="margin-bottom:10px;">&lt;input type="submit" &gt;</pre>
    <p style="margin-bottom:0px;">Slide: 27</p>
    <pre>placeholder="Your email"</pre>

    ` 
  
var slides_data = ` 

  <div class="tab-pane tab-pane-slide active" id="slide1" role="tabpanel">
    <img src="../learn/lessons/P002-T03-D-V001/img/Slide1.PNG">
    <a class="btn btn-primary next" style="top:65%;">Start Slideshow →</a>
  </div>
  <div class="tab-pane tab-pane-slide" id="slide2" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T03-D-V001/img/Slide2.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide3" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T03-D-V001/img/Slide3.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide4" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T03-D-V001/img/Slide4.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide5" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T03-D-V001/img/Slide5.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide6" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T03-D-V001/img/Slide6.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide7" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T03-D-V001/img/Slide7.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide8" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T03-D-V001/img/Slide8.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide9" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T03-D-V001/img/Slide9.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide10" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T03-D-V001/img/Slide10.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide11" role="tabpanel">


    <div class="checkpoint">
      <h2>
        Challenge <br>
        <span class="challange-step"> Step 1</span>
      </h2>

      <ol class="actions">
        <li data-index="0">
          <span class="fa fa-check "></span>
          1. Make a .box CSS class.<br>
          2. Make it 250px wide &amp; high.<br>
          <span class="html-code">
            &nbsp;&nbsp; .box {<br>
            &nbsp;&nbsp;&nbsp;&nbsp; height: 250px;<br>
            &nbsp;&nbsp;&nbsp;&nbsp; width: 250px; <br>
            &nbsp;&nbsp;}
          </span>
        </li>
      </ol>
      <a class="btn btn-primary next check" style="toXp:65%;">Skip this →</a>
      <div class="challenge-tip"><span>Tip:</span>
        In <span class="html-code">&lt;style&gt;</span> section
      </div>
    </div>

  </div>


  <div class="tab-pane tab-pane-slide" id="slide12" role="tabpanel">


    <div class="checkpoint">
      <h2>
        Challenge <br>
        <span class="challange-step"> Step 1</span>
      </h2>

      <ol class="actions">
        <li data-index="0">
          <span class="fa fa-check "></span>
          1. Make a &lt;div&gt; in the &lt;header&gt; section above &lt;h1&gt;.<br>
          2. Add the .box class to the &lt;div&gt;<br>
          <span class="html-code">
            &nbsp;&nbsp; &lt;div class="box"&gt;<br>
            &nbsp;&nbsp; &lt;/div&gt;}
          </span>
        </li>
      </ol>
      <a class="btn btn-primary next check" style="toXp:65%;">Skip this →</a>
      <div class="challenge-tip"><span>Tip:</span>
        In <span class="html-code"> &lt;header&gt;</span> above <span class="html-code"> &lt;h1&gt;</span>.
      </div>
    </div>

  </div>

  <div class="tab-pane tab-pane-slide" id="slide13" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T03-D-V001/img/Slide13.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide14" role="tabpanel">


    <div class="checkpoint">
      <h2>
        Challenge <br>
        <span class="challange-step"> Step 3</span>
      </h2>

      <ol class="actions">
        <li data-index="0">
          <span class="fa fa-check "></span>
          Add this code to the .box class:<br>
          <span class="html-code">
            &nbsp;&nbsp; background: blue;
          </span>
        </li>
      </ol>
      <a class="btn btn-primary next check" style="toXp:65%;">Skip this →</a>
      <div class="challenge-tip"><span>Tip:</span>
        In .box{ }
      </div>
    </div>

  </div>

  <div class="tab-pane tab-pane-slide" id="slide15" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T03-D-V001/img/Slide15.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide16" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T03-D-V001/img/Slide16.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide17" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T03-D-V001/img/Slide17.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide18" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T03-D-V001/img/Slide18.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide19" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T03-D-V001/img/Slide19.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide20" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T03-D-V001/img/Slide20.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide21" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T03-D-V001/img/Slide21.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide22" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T03-D-V001/img/Slide22.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide23" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T03-D-V001/img/Slide23.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide24" role="tabpanel">


    <div class="checkpoint">
      <h2>
        Challenge <br>
        <span class="challange-step"> Step 1</span>
      </h2>

      <ol class="actions">
        <li data-index="0">
          <span class="fa fa-check "></span>
          Add border-radius: to .box class with a value of 50%.<br>
          <span class="html-code">
            &nbsp;&nbsp; border-radius: 50%;
          </span>
        </li>
      </ol>
      <a class="btn btn-primary next check" style="toXp:65%;">Skip this →</a>

    </div>

  </div>


  <div class="tab-pane tab-pane-slide" id="slide25" role="tabpanel">


    <div class="checkpoint">
      <h2>
        Challenge <br>
        <span class="challange-step"> Step 3</span>
      </h2>

      <ol class="actions">
        <li data-index="0">
          <span class="fa fa-check "></span>
          1. Change CSS Class .box to .circle<br>
          2. Replace “box” in: &lt;div class='box'&gt; with circle.
          <span class="html-code">
            &nbsp;&nbsp; &lt;div class='box'&gt;
          </span>
        </li>
      </ol>
      <a class="btn btn-primary next check" style="toXp:65%;">Skip this →</a>

    </div>

  </div>

  <div class="tab-pane tab-pane-slide" id="slide26" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T03-D-V001/img/Slide26.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide27" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T03-D-V001/img/Slide27.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide28" role="tabpanel">


    <div class="checkpoint">
      <h2>
        Challenge <br>
        <span class="challange-step"> Step 3</span>
      </h2>

      <ol class="actions">
        <li data-index="0">
          <span class="fa fa-check "></span>
          Add a solid, 10px wide, white border to the .circle CSS clase.<br>
          <span class="html-code">
            &nbsp;&nbsp; border: 10px solid white;
          </span>
        </li>
      </ol>
      <a class="btn btn-primary next check" style="toXp:65%;">Skip this →</a>

    </div>

  </div>

  <div class="tab-pane tab-pane-slide" id="slide29" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T03-D-V001/img/Slide29.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide30" role="tabpanel">


    <div class="checkpoint">
      <h2>
        Challenge <br>
        <span class="challange-step"> Step 4</span>
      </h2>

      <ol class="actions">
        <li data-index="0">
          <span class="fa fa-check "></span>
          Add to the circle class:<br>
          <span class="html-code">
            &nbsp;&nbsp; display: inline-block;
          </span>
        </li>
      </ol>
      <a class="btn btn-primary next check" style="toXp:65%;">Skip this →</a>
      <div class="challenge-tip"><span>Tip:</span>
        In .circle{ }.
      </div>

    </div>

  </div>

  <div class="tab-pane tab-pane-slide" id="slide31" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T03-D-V001/img/Slide31.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide32" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T03-D-V001/img/Slide32.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide33" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T03-D-V001/img/Slide33.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide34" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T03-D-V001/img/Slide34.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide35" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T03-D-V001/img/Slide35.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide36" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T03-D-V001/img/Slide36.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide37" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T03-D-V001/img/Slide37.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide38" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T03-D-V001/img/Slide38.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide39" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T03-D-V001/img/Slide39.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide40" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T03-D-V001/img/Slide40.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide41" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T03-D-V001/img/Slide41.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide42" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T03-D-V001/img/Slide42.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide43" role="tabpanel">
    <img class="lazy" src="../learn/lessons/P002-T03-D-V001/img/Slide43.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide44" role="tabpanel">

    <a href="../learn/P2Training4" class="btn btn-primary" style="top:65%;">Start next training →</a>
  </div>
                 
` 

/// Add custom JS for lesson below here