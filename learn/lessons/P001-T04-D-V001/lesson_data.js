var file_lesson_data = 
{
    "defaultCode": "", // default code, if user has not already started coding
    "kbLayout": "", //not currently in use
    "loadJS": "", //not currently in use
    "nextLessonSlug": "", //not currently in use
    "pageDesc": "Learn how to build your first website with these easy intro lessons to coding.", 
    "pageKeywords": "coding, code, learn to code, code website, my first website", 
    "pageTitle": "CodeJIKA - Project 1, Training 4",
    "total_slides": 50,
    "save_lesson_id": "P1Training4", // This is id that will be used to store save usercode in Firebase
    "slides": [ 
    ], //not currently in use
    "slug": "" //not currently in use
}

var check_points = {
  9:"(<header>|<header [^>]*>)((.|\n)*)\s*(<div>|<div [^>]*>)((.|\n)*)\s*(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>((.|\n)*)\s*<\/div>((.|\n)*)\s*<\/header>",
  12:"(<section>|<section [^>]*>)((.|\n)*)\s*(<div>|<div [^>]*>)((.|\n)*)\s*(<h3>|<h3 [^>]*>)((.|\n)*)\s*<\/h3>((.|\n)*)\s*<\/div>((.|\n)*)\s*<\/section>",
  16:"(<footer>|<footer [^>]*>)((.|\n)*)\s*(<div>|<div [^>]*>)((.|\n)*)\s*<\/div>((.|\n)*)\s*<\/footer>",
  24:"(<style>|<style [^>]*>)((.|\n)*)\s*div((.|\n)*)\s*{((.|\n)*)\s*}((.|\n)*)\s*<\/style>",
  26:"(<style>|<style [^>]*>)((.|\n)*)\s*div((.|\n)*)\s*{((.|\n)*)\s*text-align((.|\n)*)\s*center((.|\n)*)\s*}((.|\n)*)\s*<\/style>",
  29:"(<style>|<style [^>]*>)((.|\n)*)\s*div((.|\n)*)\s*{((.|\n)*)\s*padding((.|\n)*)\s*:((.|\n)*)\s*40px((.|\n)*)\s*}((.|\n)*)\s*<\/style>",
  33:"(<style>|<style [^>]*>)((.|\n)*)\s*h3((.|\n)*)\s*{((.|\n)*)\s*}((.|\n)*)\s*<\/style>",
  36:"(<style>|<style [^>]*>)((.|\n)*)\s*h3((.|\n)*)\s*{((.|\n)*)\s*border((.|\n)*)\s*:((.|\n)*)\s*((white((.|\n)*)\s*solid((.|\n)*)\s*2px)|(solid((.|\n)*)\s*2px((.|\n)*)\s*white)|(2px((.|\n)*)\s*solid((.|\n)*)\s*white))((.|\n)*)\s*}((.|\n)*)\s*<\/style>",
  37:"(<style>|<style [^>]*>)((.|\n)*)\s*h3((.|\n)*)\s*{((.|\n)*)\s*font-size((.|\n)*)\s*45px((.|\n)*)\s*padding((.|\n)*)\s*15px((.|\n)*)\s*}((.|\n)*)\s*<\/style>",
  39:"(<style>|<style [^>]*>)((.|\n)*)\s*h3((.|\n)*)\s*{((.|\n)*)\s*margin((.|\n)*)\s*auto((.|\n)*)\s*}((.|\n)*)\s*<\/style>",
  41:"(<style>|<style [^>]*>)((.|\n)*)\s*h3((.|\n)*)\s*{((.|\n)*)\s*max-width((.|\n)*)\s*400px((.|\n)*)\s*}((.|\n)*)\s*<\/style>",
  45:"(<style>|<style [^>]*>)((.|\n)*)\s*h1((.|\n)*)\s*{((.|\n)*)\s*color((.|\n)*)\s*white((.|\n)*)\s*}((.|\n)*)\s*<\/style>"
}

var hintsForCheckPonts = {
  /* 9:"The &lt;div&gt; tag defines a division or a section in an HTML document.",
  16:"<span class='html-code'><strong>&lt;footer&gt;</strong></span> Insert div here <span class='html-code'><strong>&lt;\/footer&gt;</strong></span>",
  26:"Use the <strong>text-align</strong> property", */
}
  
var hints_data =   `

  <p style="margin-bottom:0px;">Slide: 12</p>
    <pre style="margin-bottom:10px; color:#ccc;">&lt;h1&gt;Nomzamo Mbatha&lt;/h1&gt;</pre>
    <p style="margin-bottom:0px;">Slide: 14</p>
    <pre style="margin-bottom:10px;">&lt;p>Hi! I'm Nomzamo Mbatha, An Actress. Say Hello!&lt;/p&gt;</pre>
    <p style="margin-bottom:0px;">Slide: 19</p>
    <pre style="margin-bottom:10px;">&lt;input type="email"&gt;</pre>
    <p style="margin-bottom:0px;">Slide: 23</p>	  
    <pre style="margin-bottom:10px;">&lt;input type="submit" &gt;</pre>
    <p style="margin-bottom:0px;">Slide: 27</p>
    <pre>placeholder="Your email"</pre>

    ` 
  
var slides_data = ` 

  <div class="tab-pane tab-pane-slide active" id="slide1" role="tabpanel">
    <img src="../learn/lessons/P001-T04-D-V001/img/Slide1.PNG">
    <a class="btn btn-primary next" style="top:65%;">Start Slideshow →</a>
  </div>
  <div class="tab-pane tab-pane-slide" id="slide2" role="tabpanel">
    <img src="../learn/lessons/P001-T04-D-V001/img/Slide2.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide3" role="tabpanel">
    <img src="../learn/lessons/P001-T04-D-V001/img/Slide3.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide4" role="tabpanel">
    <img src="../learn/lessons/P001-T04-D-V001/img/Slide4.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide5" role="tabpanel">
    <img src="../learn/lessons/P001-T04-D-V001/img/Slide5.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide6" role="tabpanel">
    <img src="../learn/lessons/P001-T04-D-V001/img/Slide6.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide7" role="tabpanel">
    <img src="../learn/lessons/P001-T04-D-V001/img/Slide7.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide8" role="tabpanel">
    <img src="../learn/lessons/P001-T04-D-V001/img/Slide8.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide9" role="tabpanel">

    <div class="checkpoint">
      <h2>
        Challenge
      </h2>

      <ol class="actions">
        <li data-index="0">
          <i class="far fa-circle square-icon"></i>
          <span class="fa fa-check"></span>

          <div class="challenge-content">
            Insert a <b>&lt;div&gt;</b> in &lt;header&gt;
          </div>

        </li>
      </ol>
      <a class="btn btn-primary next check" style="toXp:65%;">Skip this →</a>

      <div class="challenge-tip"><span>Where:</span><br>
        1. Open it before &lt;h1&gt;.<br>
        2. Close it after the &lt;p&gt; tags.
      </div>
    </div>
  </div>

  <div class="tab-pane tab-pane-slide" id="slide10" role="tabpanel">
    <img class="lozad" data-src="../learn/lessons/P001-T04-D-V001/img/Slide10.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide11" role="tabpanel">
    <img class="lozad" data-src="../learn/lessons/P001-T04-D-V001/img/Slide11.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide12" role="tabpanel">

    <div class="checkpoint">
      <h2>
        Challenge
      </h2>

      <ol class="actions">
        <li data-index="0">
          <i class="far fa-circle square-icon"></i>
          <span class="fa fa-check"></span>

          <div class="challenge-content">
            Insert a <b>&lt;div&gt;</b> in &lt;section&gt;
          </div>

        </li>
      </ol>
      <a class="btn btn-primary next check" style="toXp:65%;">Skip this →</a>

      <div class="challenge-tip"><span>Where:</span><br>
        1. Open the &lt;div&gt; before your &lt;h3&gt;.<br>
        2. Close it after the &lt;h3&gt; tags.
      </div>
    </div>
  </div>

  <div class="tab-pane tab-pane-slide" id="slide13" role="tabpanel">
    <img class="lozad" data-src="../learn/lessons/P001-T04-D-V001/img/Slide13.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide14" role="tabpanel">
    <img class="lozad" data-src="../learn/lessons/P001-T04-D-V001/img/Slide14.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide15" role="tabpanel">
    <img class="lozad" data-src="../learn/lessons/P001-T04-D-V001/img/Slide15.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide16" role="tabpanel">

    <div class="checkpoint">
      <h2>
        Challenge
      </h2>

      <ol class="actions">
        <li data-index="0">
          <i class="far fa-circle square-icon"></i>
          <span class="fa fa-check"></span>

          <div class="challenge-content">
            Wrap the content in <b>&lt;footer&gt;</b> with a &lt;div&gt;
          </div>

        </li>
      </ol>
      <a class="btn btn-primary next check" style="toXp:65%;">Skip this →</a>

    </div>
  </div>

  <div class="tab-pane tab-pane-slide" id="slide17" role="tabpanel">
    <img class="lozad" data-src="../learn/lessons/P001-T04-D-V001/img/Slide17.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide18" role="tabpanel">
    <img class="lozad" data-src="../learn/lessons/P001-T04-D-V001/img/Slide18.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide19" role="tabpanel">
    <img class="lozad" data-src="../learn/lessons/P001-T04-D-V001/img/Slide19.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide20" role="tabpanel">
    <img class="lozad" data-src="../learn/lessons/P001-T04-D-V001/img/Slide20.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide21" role="tabpanel">
    <img class="lozad" data-src="../learn/lessons/P001-T04-D-V001/img/Slide21.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide22" role="tabpanel">
    <img class="lozad" data-src="../learn/lessons/P001-T04-D-V001/img/Slide22.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide23" role="tabpanel">
    <img class="lozad" data-src="../learn/lessons/P001-T04-D-V001/img/Slide23.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide24" role="tabpanel">

    <div class="checkpoint">
      <h2>
        Challenge
      </h2>
      <span class="challange-step" style="color:black;"> Step 1</span>
      <ol class="actions">
        <li data-index="0">
          <i class="far fa-circle square-icon"></i>
          <span class="fa fa-check"></span>

          <div class="challenge-content">

            Add a <b>div { }</b> selector in the <b>&lt;style&gt;</b> section.<br>
            <span class="code">
              div{ <br>
              &nbsp;}
            </span>
          </div>

        </li>
      </ol>
      <a class="btn btn-primary next check" style="toXp:65%;">Skip this →</a>

    </div>
  </div>

  <div class="tab-pane tab-pane-slide" id="slide25" role="tabpanel">
    <img class="lozad" data-src="../learn/lessons/P001-T04-D-V001/img/Slide25.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide26" role="tabpanel">

    <div class="checkpoint">
      <h2>
        Challenge
      </h2>
      <span class="challange-step" style="color:black;"> Step 2</span>
      <ol class="actions">
        <li data-index="0">
          <i class="far fa-circle square-icon"></i>
          <span class="fa fa-check"></span>

          <div class="challenge-content">
            Center align all the content in the &lt;div&gt;.
          </div>

        </li>
      </ol>
      <a class="btn btn-primary next check" style="toXp:65%;">Skip this →</a>

    </div>
  </div>

  <div class="tab-pane tab-pane-slide" id="slide27" role="tabpanel">
    <img class="lozad" data-src="../learn/lessons/P001-T04-D-V001/img/Slide27.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide28" role="tabpanel">
    <img class="lozad" data-src="../learn/lessons/P001-T04-D-V001/img/Slide28.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide29" role="tabpanel">

    <div class="checkpoint">
      <h2>
        Challenge
      </h2>
      <span class="challange-step" style="color:black;"> Step 2</span>
      <ol class="actions">
        <li data-index="0">
          <i class="far fa-circle square-icon"></i>
          <span class="fa fa-check"></span>

          <div class="challenge-content">
            Using CSS, add <b>40px padding</b> to div.
          </div>

        </li>
      </ol>
      <a class="btn btn-primary next check" style="toXp:65%;">Skip this →</a>

    </div>
  </div>

  <div class="tab-pane tab-pane-slide" id="slide30" role="tabpanel">
    <img class="lozad" data-src="../learn/lessons/P001-T04-D-V001/img/Slide30.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide31" role="tabpanel">
    <img class="lozad" data-src="../learn/lessons/P001-T04-D-V001/img/Slide31.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide32" role="tabpanel">
    <img class="lozad" data-src="../learn/lessons/P001-T04-D-V001/img/Slide32.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide33" role="tabpanel">

    <div class="checkpoint">
      <h2>
        Challenge
      </h2>
      <span class="challange-step" style="color:black;"> Step 1</span>
      <ol class="actions">
        <li data-index="0">
          <i class="far fa-circle square-icon"></i>
          <span class="fa fa-check"></span>

          <div class="challenge-content">
            Add the h3 Selector to &lt;style&gt; section.
          </div>

        </li>
      </ol>
      <a class="btn btn-primary next check" style="toXp:65%;">Skip this →</a>

    </div>
  </div>

  <div class="tab-pane tab-pane-slide" id="slide34" role="tabpanel">
    <img class="lozad" data-src="../learn/lessons/P001-T04-D-V001/img/Slide34.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide35" role="tabpanel">
    <img class="lozad" data-src="../learn/lessons/P001-T04-D-V001/img/Slide35.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide36" role="tabpanel">

    <div class="checkpoint">
      <h2>
        Challenge
      </h2>
      <span class="challange-step" style="color:black;"> Step 2</span>
      <ol class="actions">
        <li data-index="0">
          <i class="far fa-circle square-icon"></i>
          <span class="fa fa-check"></span>

          <div class="challenge-content">
            Add a white solid 2px border to <b>h3 { }</b>
          </div>

        </li>
      </ol>
      <a class="btn btn-primary next check" style="toXp:65%;">Skip this →</a>

    </div>
  </div>


  <div class="tab-pane tab-pane-slide" id="slide37" role="tabpanel">

    <div class="checkpoint">
      <h2>
        Challenge
      </h2>
      <span class="challange-step" style="color:black;"> Step 3</span>
      <ol class="actions">
        <li data-index="0">
          <i class="far fa-circle square-icon"></i>
          <span class="fa fa-check"></span>

          <div class="challeng-content">
            Add a font-size 45px &amp; a padding of 15px to <b>h3 { }</b>.<br>

            <span class="code">
              &nbsp; font-size: 45px; <br>
              &nbsp; padding: 15px;
            </span>

          </div>
        </li>
      </ol>
      <a class="btn btn-primary next check" style="toXp:65%;">Skip this →</a>

    </div>
  </div>

  <div class="tab-pane tab-pane-slide" id="slide38" role="tabpanel">
    <img class="lozad" data-src="../learn/lessons/P001-T04-D-V001/img/Slide38.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide39" role="tabpanel">

    <div class="checkpoint">
      <h2>
        Challenge
      </h2>
      <span class="challange-step" style="color:black;"> Step 4</span>
      <ol class="actions">
        <li data-index="0">
          <i class="far fa-circle square-icon"></i>
          <span class="fa fa-check"></span>
          <div class="challenge-content">

            Center your <b>h3 { } </b> by using the <b>margin: auto;</b> trick.<br>

            <span class="code">
              &nbsp; margin: auto;
            </span>

          </div>
        </li>
      </ol>
      <a class="btn btn-primary next check" style="toXp:65%;">Skip this →</a>

    </div>
  </div>

  <div class="tab-pane tab-pane-slide" id="slide40" role="tabpanel">
    <img class="lozad" data-src="../learn/lessons/P001-T04-D-V001/img/Slide40.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide41" role="tabpanel">

    <div class="checkpoint">
      <h2>
        Challenge
      </h2>
      <span class="challange-step" style="color:black;"> Step 5</span>
      <ol class="actions">
        <li data-index="0">
          <i class="far fa-circle square-icon"></i>
          <span class="fa fa-check"></span>
          <div class="challenge-content">

            Beautify your h3 border by giving it a max-width of 400px.<br>

            <span class="code">
              &nbsp; max-width: 400px;
            </span>

          </div>
        </li>
      </ol>
      <a class="btn btn-primary next check" style="toXp:65%;">Skip this →</a>

    </div>
  </div>

  <div class="tab-pane tab-pane-slide" id="slide42" role="tabpanel">
    <img class="lozad" data-src="../learn/lessons/P001-T04-D-V001/img/Slide42.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide43" role="tabpanel">
    <img class="lozad" data-src="../learn/lessons/P001-T04-D-V001/img/Slide43.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide44" role="tabpanel">
    <img class="lozad" data-src="../learn/lessons/P001-T04-D-V001/img/Slide44.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide45" role="tabpanel">

    <div class="checkpoint">
      <h2>
        Challenge
      </h2>
      <span class="challange-step" style="color:black;"> Step 6</span>
      <ol class="actions">
        <li data-index="0">
          <i class="far fa-circle square-icon"></i>
          <span class="fa fa-check"></span>
          <div class="challenge-content">
            Make your h1 text white.
          </div>
        </li>
      </ol>
      <a class="btn btn-primary next check" style="toXp:65%;">Skip this →</a>

    </div>
  </div>

  <div class="tab-pane tab-pane-slide" id="slide46" role="tabpanel">
    <img class="lozad" data-src="../learn/lessons/P001-T04-D-V001/img/Slide46.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide47" role="tabpanel">
    <img class="lozad" data-src="../learn/lessons/P001-T04-D-V001/img/Slide47.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide48" role="tabpanel">
    <img class="lozad" data-src="../learn/lessons/P001-T04-D-V001/img/Slide48.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide49" role="tabpanel">
    <img class="lozad" data-src="../learn/lessons/P001-T04-D-V001/img/Slide49.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide50" role="tabpanel">
    <img class="lozad" data-src="../learn/lessons/P001-T04-D-V001/img/Slide50.PNG">
  </div>
               
` 

/// Add custom JS for lesson below here