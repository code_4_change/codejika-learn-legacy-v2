<?php
//Header('Vary: User-Agent, Accept');
$home_url = 'https://'.$_SERVER['SERVER_NAME'];
?>
<!doctype html>
<html lang='en'>

<head>
  <meta charset='utf-8' />
  <!-- <script src="/js/confirmDialog.jquery.js"></script> -->
  <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script> -->
  <script src="/libraries/codemirror/lib/codemirror.js"></script>
  <link rel='apple-touch-icon' sizes='76x76' href='assets/img/apple-icon.png'>
  <link rel='icon' type='image/png' href='assets/img/favicon.png'>
  <meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1' />
  <title>Learn to code HTML, CSS, and JavaScript with CodeJIKA</title>
  <meta name="description" content="CodeJIKA is a fun and free online course that teaches you how to make websites through simple projects you can do right in your browser. Get started today!">
  <meta name="keywords" content="code, jika, school, free, coding, learn online">
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />

  <script src="/js/fontawesome.min.js" type="text/javascript"></script>

  <!-- fontawesome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
  <!-- <script src="https://www.gstatic.com/firebasejs/4.12.0/firebase.js" id="js-firebase" async></script>		 -->
  <!--     Fonts and icons     -->
  <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700,200' rel='stylesheet' />
  <link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css' />
  <!-- CSS Files -->
  <link href='/assets/css/bootstrap.min.css' rel='stylesheet' />
  <link href='/css/now-ui-kit.css?v=1.1.0' rel='stylesheet' />
  <!-- <link rel='stylesheet' href='/doc/docs.css'> -->
  <link rel='stylesheet' href='/css/codemirror.css'>
  <!--link rel="stylesheet" href="/addon/hint/show-hint.css">
	<link rel="stylesheet" href="/addon/lint/lint.css"-->
  <link rel='stylesheet' href='/css/theme/base16-dark.css'>
  <link rel='stylesheet' href='https://unpkg.com/simplebar@latest/dist/simplebar.css'>
  <script type="text/javascript">
    //<![CDATA[
    function randTxt() {
      var text = "";
      var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

      for (var i = 0; i < 5; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));
      return text;
    }

    var randText = randTxt();

    document.write(" <link href='/css/desktop-lesson.css?" + randText + "' rel='stylesheet' />");
    //]]>
  </script>
</head>

<body class='template-page sidXebar-collapse h-100 lesson'>
  <!-- Navbar -->
  <nav class="navbar navbar-expand-lg  nav-header fixed-top " color-on-scroll="400">
    <div class="container-fluid">
      <div class="navbar-translate">
        <button class="navbar-toggler navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="true" aria-label="Toggle navigation">
          <span class="navbar-toggler-bar bar1"></span>
          <span class="navbar-toggler-bar bar2"></span>
          <span class="navbar-toggler-bar bar3"></span>
        </button>
      </div>
      <div class="navbar-expand justify-content-end " id="navigation" style="" data-nav-image="/assets/img/blurred-image-1.jpg">
        <ul class="navbar-nav justify-content-end ">
          <li class="nav-item" style="padding-right: 10px;">
            <a class="nav-link" data-toggle="modal" data-target="#galleryModal">
              Gallery
            </a>
          </li>
          <li class="nav-item" style="padding-right: 10px;">
            <a class="nav-link" data-toggle="modal" data-target="#mainMenuModal">
              Menu
            </a>
          </li>
          <li class="nav-item"><a class="nav-link" style="color: #fff;">Tell your friends&nbsp;&nbsp;
              <div class="sharethis-inline-share-buttons st-right  st-inline-share-buttons st-animated" id="st-1" style="display: inline-block;">

                <div class="st-btn st-first    st-remove-label" data-network="facebook" style="display: inline-block;">
                  <img src="https://platform-cdn.sharethis.com/img/facebook.svg">
                </div>
                <div class="st-btn    st-remove-label" data-network="twitter" style="display: none;">
                  <img src="https://platform-cdn.sharethis.com/img/twitter.svg">
                </div>
                <div class="st-btn st-last    st-remove-label" data-network="sharethis" style="display: inline-block;">
                  <img src="https://platform-cdn.sharethis.com/img/sharethis.svg">
                </div>
              </div></a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
  <!-- End Navbar -->
  <!-- End Navbar 
		==========	-->

  <!-- Strat project and trainings 
		=========================	-->
  <div class='flex-row d-flex flex-fill no-gutters flex-nowrap' id="main_page">
    <div class='container-fluid' style="padding:0;">
      <div class='flex-row d-flex no-gutters flex-nowrap overflow-hidden resizer-parent' style='height: 100%;'>
        <div class='flex-column d-flex content split-horizontal  col-lesson-code ' style=' '>

          <div class='no-gutter lesson-skills'>


            <div class="card">
              <a class="main-logo" rel="tooltip" title="" data-placement="bottom" data-original-title="Return to Code JIKA homepage">
                <img src="/img/logo-beta-1.png">
              </a>
              <ul id="submenu" class="nav nav-tabs nav-tabs-neutral justify-content-center" role="tablist">
                <li role="submenu" class="nav-item">
                  <a class="nav-link active " data-toggle="tab" href="#lesson_tab" role="tab_menu">Lesson</a>
                </li>
                <li role="submenu" class="nav-item">
                  <a class="nav-link " data-toggle="tab" href="#forum_tab" role="tab_menu">Q&A Forum</a>
                </li>
                <li role="submenu" class="nav-item d-none">
                  <a class="nav-link" data-toggle="tab" href="#resources_tab" role="tab_menu">Hints</a>
                </li>
              </ul>
              <div class="card-body">
                <!-- Tab panes -->
                <div class="tab-content">

                  <div class="tab-pane active " id="lesson_tab" role="tabpanel">
                    <ul id='tab-slides' class='nav nav-tabs justify-content-center' role='tablist'>
                    </ul>
                    <div class="tab-content">


                    </div>

                    <div class="row pagination">
                      <div class="col-sm-12 no-gutter">
                        <div class="progress">
                          <div id="lessonProgressBar" class="progress-bar progress-bar-success" role="progressbar" style="">
                          </div>
                        </div>
                      </div>
                      <div class="col-sm-12">
                        <div class="row remove-all-margin">
                          <div class="col-sm-4 text-left no-gutter">
                            <a class="prev disabled" href="#" aria-label="Previous Slide">
                              < Previous</span>
                            </a>
                          </div>
                          <div class="col-sm-4 text-center no-gutter">
                            <div class="lessonProgressText"></div>
                          </div>
                          <div class="col-sm-4 text-right no-gutter">
                            <a class="next" href="#" aria-label="Next Slide">Start Slideshow
                            </a>
                          </div>
                        </div>
                      </div>

                      <div style="position: relative; width: 100%;">
                        <div class="previous-overlay-btn-container">
                          <a class="prev disabled" href="#" aria-label="Previous Slide">

                          </a>
                        </div>
                        <div class="next-overlay-btn-container">
                          <a class="next" href="#" aria-label="Next Slide" t>
                          </a>
                        </div>
                      </div>
                    </div>




                  </div>
                  <div class="tab-pane text-center" id="forum_tab" role="tabpanel">
                    <h2 style="user-select: text;">Stuck?</h2>
                    <p style="user-select: text;">Ask the community for help in our Facebook group!</p>
                    <a href="https://www.facebook.com/groups/626270557761252/" target="_blank" class="button">Launch the Facebook Group</a>
                  </div>
                  <div class="tab-pane" id="skills_tab" role="tabpanel">

                  </div>
                  <div class="tab-pane" id="resources_tab" role="tabpanel" style="text-align: left;">

                  </div>
                </div>
              </div>
            </div>
            <div class="hint-popup-checked"></div>


          </div>
          <div class='no-gutter code-editor' style=''>
            <!--<div class=""><div class=""></div>
											<a onclick="close_hint()" class="close-hint-popup" style="display:none"><i class="fas fa-times-circle fa-lg"></i></a>
										</div>-->
            <textarea id='myeditor' name='myeditor' style="display: none;">

										</textarea>
          </div>

        </div>
        <div class='flex-column d-flex content split-horizontal  col-preview ' style=''>
          <div class='' style='    height: 100%;'>
            <div id="cloak" class="hint-popup">
              <div class="hint-text"></div>
              <a onclick="close_hint()" class="close-hint-popup"><i class="fas fa-times-circle fa-lg"></i></a>
            </div>
            <textarea id='myeditor' name='myeditor' style="display: none;">

										</textarea>
            <iframe id='preview' class="">


            </iframe>

          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- ************** gallery page start -->


  <!-- ************* gallery page end -->

  <!-- Button trigger modal -->


  <!-- mainMenuModal Modal -->
  <div class="modal fadeX" id="galleryModal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <a type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </a>
        <div id="gallery_section">
          <div class="loading" id="loadingIcon">
            <div class="spinner">
              <div class="rect1"></div>
              <div class="rect2"></div>
              <div class="rect3"></div>
              <div class="rect4"></div>
              <div class="rect5"></div>
            </div>
          </div>
          <div class="container">
            <h2 class="page_title">Projects</h2>

            <div class="dropdown">
              <!--<button class="btn btn-secondary dropdown-toggle primary" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						Search By
						</button> -->
              <select id="selectOptions" onchange="FilterData(this.event)">
                <option class="dropdown-item" id="projects">All Projects</option>
                <option class="dropdown-item" id="recent">Recent</option>
                <option class="dropdown-item" id="likes" value="MostLiked">Most Liked</option>
              </select>
            </div>
            <nav aria-label="Page navigation example">
              <ul class="pagination justify-content-center">
                <li class="page-item " id="previous">
                  <a class="page-link" tabindex="-1" onclick="go_previous()">Previous</a>
                </li>
                <li class="page-item"><a class="page-link" href="#" id="pageNumber">1</a></li>

                <li class="page-item" id="next2">
                  <a class="page-link" onclick="go_next()">Next</a>
                </li>
              </ul>
            </nav>
            <div>
              <div class="gallery-container" id="galleryContainer">
                <div class="" id="gallery_page">
                  <div class="container" style="height:100%; padding: 23px;overflow-y: scroll;overscroll-behavior-y: contain;">
                    <!-- <h2 class="">Projects</h2>
					  <div class="search-continer">
						<div class="search-list">
						  <div class="">
							<h6>Search By:</h6>
						  </div>
						  <ul>
							<li>
							  <select name="" id="selectOptions" onchange="FilterData(this.event)">
								<option value="Projects" id="projects">All Projects</option>
								<option value="Recent" id="recent"> Recent</option>
								<option value="MostLiked" id="likes">Most Liked</option>
							  </select>
							</li>
						  </ul>
						</div>
					  </div> 
					  <div class="pagination-section">
						<ul class="pagination">
						  <li class="page-item previous" id="previous"><a onclick="go_previous()" class="page-link">Previous</a></li>
						  <li class="page-item"><a class="page-link" id="pageNumber"></a></li>
						  <li class="page-item next" id="next2"><a onclick="go_next()" class="page-link">Next</a></li>
						</ul>
					  </div> -->
                    <div class="loading" id="loadingIcon">
                      <div class="spinner">
                        <div class="rect1"></div>
                        <div class="rect2"></div>
                        <div class="rect3"></div>
                        <div class="rect4"></div>
                        <div class="rect5"></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="gallery-container" id="galleryLikes">

              </div>
              <div class="gallery-container" id="recentProjects">
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>

  <!-- mainMenuModal Modal -->
  <div class="modal fadeX" id="mainMenuModal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div id="profile_page" class=" ">
          <div class="container">
            <a type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </a>
            <i class="fas fa-user-circle"></i>
            <h2 class="profile_name">Hello</h2>
            <div class="menu">
              <ul>
                <li class="pointer">
                  <a class="divLink  pointer" href="https://<?php echo $_SERVER['SERVER_NAME'] ?>"></a>
                  <i class="fas fa-home 3x"></i> Homepage
                </li>
                <li class="pointer ">
                  <a class="divLink" href="https://<?php echo $_SERVER['SERVER_NAME'] ?>/learn/projects"></a>
                  <i class="fas fa-list-alt 3x"></i> Projects
                </li>
                <li class="reset-lesson pointer d-nXone" data-dismiss="modal">
                  <i class="fas fa-undo"></i> Reset Current Lesson
                </li>
                <li class="pointer">
                  <i class="fas fa-chalkboard-teacher"></i> Instructor
                  <div class="option_selected select_robot" style="margin-left: 5px;">Robot</div>
                  <div class="option_unselected select_lego">Lego</div>
                </li>
                <li class="login-profile pointer d-noXne" data-dismiss="modal" onclick="loginModal()">
                  <i class="fas fa-sign-in-alt"></i> Login / Sign up
                </li>
                <li class="logout-profile pointer d-none" data-dismiss="modal" style="">
                  <i class="fas fa-external-link-alt pointer"></i> Logout
                </li>

                <li id="share-link" class="hide" style="display:none;">
                  <i class="icon-assignment fs2"></i> Share on:
                  <div class="option_unselected facXebook-share" data-js="facebook-share" style="margin-left: 5px;">FB</div>
                  <div class="option_unselected twitXter-share" style="margin-left: 5px;" data-js="twitter-share">TW</div>
                  <a id="whatsapp" href="">
                    <div class="option_unselected ">WA</div>
                  </a>
                </li>
                <center><a href="" id="switch-device"> View as Mobile</a></center>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


  <!-- modal jquery file -->
  <div class="modalPlaceholder"></div>

</body>
<!--   Core JS Files   -->


<script src='/js/core/jquery.3.2.1.min.js' type='text/javascript'></script>
<script src='/assets/js/vendor/popper.min.js' type='text/javascript'></script>
<script src='/assets/js/bootstrap.min.js' type='text/javascript'></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/lozad/dist/lozad.min.js"></script>
<!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
<script src='/js/plugins/bootstrap-switch.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/split.js/1.5.10/split.min.js'></script>
<!-- Control Center for Now Ui Kit: parallax effects, scripts for the example pages etc -->
<!-- <script src='/js/now-ui-kit.js?v=1.1.0' type='text/javascript'></script> -->

<!-- firebase -->
<script src="https://www.gstatic.com/firebasejs/4.12.0/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.12.0/firebase-database.js"></script>



<script src="/libraries/codemirror/mode/javascript/javascript.js"></script>
<script src="/libraries/codemirror/mode/xml/xml.js"></script>
<script src="/libraries/codemirror/mode/css/css.js"></script>
<script src="/libraries/codemirror/mode/htmlmixed/htmlmixed.js"></script>
<script src="/libraries/codemirror/addon/edit/closetag.js"></script>
<script src="/libraries/codemirror/addon/edit/matchbrackets.js"></script>
<script src="/libraries/codemirror/addon/search/match-highlighter.js"></script>
<script src="/libraries/codemirror/htmlhint.js"></script>
<script src="/libraries/codemirror/csslint.js"></script>
<script src="/libraries/codemirror/jshint.js"></script>
<script src="/libraries/codemirror/addon/lint/lint.js"></script>
<script src="/libraries/codemirror/addon/lint/html-lint.js"></script>
<script src="/libraries/codemirror/addon/lint/css-lint.js"></script>
<script src="/libraries/codemirror/addon/lint/javascript-lint.js"></script>
<script src="/libraries/codemirror/markdown.js"></script>
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=59d266dad184b0001230f752&product=inline-share-buttons"></script>

<?php
function get_all_get()
{
        $output = "?"; 
        $firstRun = true; 
        foreach($_GET as $key=>$val) { 
        if($key != $parameter) { 
            if(!$firstRun) { 
                $output .= "&"; 
            } else { 
                $firstRun = false; 
            } 
            $output .= $key."=".$val;
         } 
    } 

    return $output;
} 
//echo get_all_get();
?>

<script type="text/javascript">
  var lessons_data = {
    "P1Training1": 'P001-T01-D-V001',
    "P1Training2": 'P001-T02-D-V001',
    "P1Training3": 'P001-T03-D-V001',
    "P1Training4": 'P001-T04-D-V001',
    "P2Training1": 'P002-T01-D-V001',
    "P2Training2": 'P002-T02-D-V001',
    "P2Training3": 'P002-T03-D-V001',
    "P2Training4": 'P002-T04-D-V001',
    "P2Training5": 'P002-T05-D-V001',
    "P2Training6": 'P002-T06-D-V001',
  };

  // var switch_url = $(location).attr("href").replace(/&mobile|&desktop/gi,'') + "&mobile"
  //  console.log("se2: " + switch_url);
  // $("#switch-mobile").attr("href", switch_url)



  $('#switch-device').click(function() {
    //  $.cookie('devicetype', 'mobile');
    document.cookie = "devicetype=mobile; domain=.<?php echo$_SERVER['SERVER_NAME']; ?>;path=/; secure";
    //console.log("se1: " + $(location).attr("href"));
    //window.location.reload()
  })

  var lessson_url = "<?php echo htmlspecialchars($_GET["lesson_id"]); ?>";
  //var lessson_url = "P1Training1";
  var set_user_id = "<?php echo htmlspecialchars($_GET["user_id"]); ?>";

  var lesson_id = lessons_data[lessson_url];
  //var lesson_id = 'P001-L00-M-V004-B'
  //var save_lesson_id = "5-minute-website"


  console.log("PHP GET/query string: <?php echo get_all_get(); ?>" );
  console.log("lesson_url: " + lessson_url);
  console.log("lesson_id: " + lesson_id);
  console.log("set_user_id: " + set_user_id);

  <!-- Global Site Tag (gtag.js) - Google Analytics -->

  (function(i, s, o, g, r, a, m) {
    i['GoogleAnalyticsObject'] = r;
    i[r] = i[r] || function() {
    (i[r].q = i[r].q || []).push(arguments)
    }, i[r].l = 1 * new Date();
    a = s.createElement(o),
    m = s.getElementsByTagName(o)[0];
    a.async = 1;
    a.src = g;
    m.parentNode.insertBefore(a, m)
  })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

  ga('create', 'UA-63106610-3', 'auto');
  ga('send', 'pageview');


  document.write("<link href='lessons/" + lesson_id + "/custom.css?" + randText + "' rel='stylesheet' />");
  document.write("<script type='text/javascript' src='lessons/" + lesson_id + "/lesson_data.js?" + randText + "'><\/script>");
  document.write("<script type='text/javascript' src='/js/desktop-lesson.js?" + randText + "'><\/script>");


</script>



</html>