$(document).ready(function() {

  if(window.location.href.indexOf('#signup') != -1) {
   $("a.signup").trigger("click");
   console.log("signup");
  }
  if(window.location.href.indexOf('#codejika-labs') != -1) {
   $("a.codejika-labs").trigger("click");
   console.log("codejika-labs");
  }
  
  $("#custom-nav-toggle").on('click',function(){
    $("#toggle-section-menu").addClass("show-menu-section");
    $("html").addClass("overflow-hidden");
  });
  
  $("#custom-nav-cross").on('click',function(){
		$("#toggle-section-menu").removeClass("show-menu-section");
     $("html").removeClass("overflow-hidden");
  });

  $(".mobile-header-pop #custom-nav-toggle").on('click',function(){
    $(".mobile-header-pop #toggle-section-menu").addClass("show-menu-section");
    $("html").addClass("overflow-hidden");
  });
  
  $(".mobile-header-pop #custom-nav-cross").on('click',function(){
    $(".mobile-header-pop #toggle-section-menu").removeClass("show-menu-section");
     $("html").removeClass("overflow-hidden");
  });

  if($(".mobile-header-pop").is(":visible")){
      $("#masthead").find("#curriculum_section").attr("id","");
  }

});
