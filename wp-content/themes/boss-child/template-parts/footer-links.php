<div id="footer-links">

	<?php if ( has_nav_menu( 'secondary-menu' ) ) : ?>
		<ul class="footer-menu">
			<?php wp_nav_menu( array( 'container' => false, 'menu_id' => 'nav', 'theme_location' => 'secondary-menu', 'items_wrap' => '%3$s', 'depth' => -1 ) ); ?>
		</ul>
	<?php endif; ?>

	<?php get_template_part( 'template-parts/footer-social-links' ); ?>

	<?php if ( boss_get_option( 'boss_layout_switcher' ) ) { ?>
  	<?php
if($_COOKIE['switch_mode'] == 'mobile') {
  $device_mode = 'desktop';
} else {
  $device_mode = 'mobile';
}
?>
			<div id="switch_submit" style="cursor: pointer;line-height: 20px;padding: 10px;" >View as <?php echo $device_mode;?></div>


	<?php } else { ?>

		<a href="#scroll-to" class="to-top fa fa-angle-up scroll"></a>

	<?php } ?>

</div>
  <script type="text/javascript">

      $('#switch_submit').click(function () {
      console.log("se1: " + $(location).attr("href"));
        document.cookie = "devicetype=<?php echo $device_mode;?>; domain=."+window.location.hostname+"; ?>;path=/; secure";
        document.cookie = "switch_mode=<?php echo $device_mode;?>; domain="+window.location.hostname+"; ?>;path=/; secure";
        window.location.reload()
      })
    </script>