=== Zapier for LearnDash ===
Author: LearnDash
Author URI: https://learndash.com 
Plugin URI: https://learndash.com/add-on/zapier-integration/
LD Requires at least: 2.5
Slug: learndash-zapier
Tags: integration, zapier,
Requires at least: 4.9
Tested up to: 4.9.4
Requires PHP: 7
Stable tag: 1.1.0

Integrate LearnDash LMS with Zapier.

== Description ==

Integrate LearnDash LMS with Zapier.


Zapier is a service that makes it easy for you to connect two applications without the need to know code, currently with a library of over 300 applications. Zapier calls these connections Zaps, and this integration lets you create Zaps that include LearnDash activities.

= Integration Features = 

* Perform actions in over 300 applications based on seven specific LearnDash activities
* Supports both global and specific LearnDash activity
* Easily connect LearnDash to the popular Zapier program without code

See the [Add-on](https://learndash.com/add-on/zapier/) page for more information.

== Installation ==

If the auto-update is not working, verify that you have a valid LearnDash LMS license via LEARNDASH LMS > SETTINGS > LMS LICENSE. 

Alternatively, you always have the option to update manually. Please note, a full backup of your site is always recommended prior to updating. 

1. Deactivate and delete your current version of the add-on.
1. Download the latest version of the add-on from our [support site](https://support.learndash.com/article-categories/free/).
1. Upload the zipped file via PLUGINS > ADD NEW, or to wp-content/plugins.
1. Activate the add-on plugin via the PLUGINS menu.

== Changelog ==

= 1.1.0 =
* Added Course drop-down menu for Course enrollment trigger so that a specific course can be chosen instead of using the Zapier filter feature
* Added support for Course enrollment trigger when enrolled via a LearnDash Group
* Added support for first and last name in data sent to Zapier
* Updated menu logic so that the Zapier menu option appears under LearnDash LMS menu instead of Settings

View the full changelog [here](https://www.learndash.com/add-on/zapier/).