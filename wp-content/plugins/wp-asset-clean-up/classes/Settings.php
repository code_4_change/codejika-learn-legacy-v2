<?php
namespace WpAssetCleanUp;

/**
 * Class Settings
 * @package WpAssetCleanUp
 */
class Settings
{
	/**
	 * @var array
	 */
	public $settingsKeys = array(
		// Stored in 'wpassetcleanup_settings'
        'frontend_show',
        'dashboard_show',
        'dom_get_type',
        'disable_emojis',

		// Stored in 'wpassetcleanup_global_unload' option
        'disable_jquery_migrate',
        'disable_comment_reply'
    );

    /**
     * @var array
     */
    public $currentSettings = array();

    /**
     * @var array
     */
    public $status = array(
        'updated' => false
    );

    /**
     *
     */
    public function init()
    {
        add_action('admin_init', array($this, 'saveSettings'), 1);

        if (array_key_exists('page', $_GET) && $_GET['page'] === 'wpassetcleanup_settings') {
	        add_action('admin_notices', array($this, 'notices'));
	        add_action('admin_head', array($this, 'inlineHead'));
        }
    }

	/**
	 *
	 */
	public function notices()
    {
    	$settings = $this->getAll();

    	// When all ways to manage the assets are not enabled
    	if ($settings['dashboard_show'] != 1 && $settings['frontend_show'] != 1) {
		    ?>
		    <div class="notice notice-warning">
				<p><span style="color: #ffb900;" class="dashicons dashicons-info"></span>&nbsp;<?php _e('It looks like you have both "Manage in the Dashboard?" and "Manage in the Front-end?" inactive. The plugin works fine with any settings that were applied. However, if you want to manage the assets in any page, you need to have at least one of them active.', WPACU_PLUGIN_NAME); ?></p>
		    </div>
		    <?php
	    }

	    // After "Save changes" is clicked
        if ($this->status['updated']) {
            ?>
            <div class="notice notice-success">
                <?php _e('The settings were successfully updated.', WPACU_PLUGIN_NAME); ?>
            </div>
            <?php
        }
    }

    /**
     *
     */
    public function saveSettings()
    {
        if (! empty($_POST) && array_key_exists('wpacu_settings_page', $_POST)) {
            $data = isset($_POST[WPACU_PLUGIN_NAME.'_settings']) ? $_POST[WPACU_PLUGIN_NAME.'_settings'] : array();

            $this->update($data);
        }
    }

    /**
     *
     */
    public function settingsPage()
    {
        $data = $this->getAll();

        foreach ($this->settingsKeys as $settingKey) {
            // Special check for plugin versions < 1.2.4.4
            if ($settingKey === 'frontend_show') {
                $data['frontend_show'] = $this->showOnFrontEnd();
            }
        }

        $globalUnloadList = Main::instance()->getGlobalUnload();

        if (in_array('jquery-migrate', $globalUnloadList['scripts'])) {
            $data['disable_jquery_migrate'] = 1;
        }

	    if (in_array('comment-reply', $globalUnloadList['scripts'])) {
		    $data['disable_comment_reply'] = 1;
	    }

        Main::instance()->parseTemplate('settings-plugin', $data, true);
    }

    /**
     * @return bool
     */
    public function showOnFrontEnd()
    {
        $settings = $this->getAll();

        if ($settings['frontend_show'] == 1) {
            return true;
        }

        // Prior to 1.2.4.4
        if (get_option(WPACU_PLUGIN_NAME.'_frontend_show') == 1) {
            // Put it in the main settings option
            $settings = $this->getAll();
            $settings['frontend_show'] = 1;
            $this->update($settings);

            delete_option(WPACU_PLUGIN_NAME.'_frontend_show');
            return true;
        }

        return false;
    }

    /**
     * @return array
     */
    public function getAll()
    {
        if (! empty($this->currentSettings)) {
            return $this->currentSettings;
        }

        $settingsOption = get_option(WPACU_PLUGIN_NAME.'_settings');

        if ($settingsOption != '' && is_string($settingsOption)) {
            $settings = (array)json_decode($settingsOption);

            if (json_last_error() == JSON_ERROR_NONE) {
                // Make sure all the keys are there even if no value is attached to them
                // To avoid writing extra checks in other parts of the code and prevent PHP notice errors
                foreach ($this->settingsKeys as $settingsKey) {
                    if (! array_key_exists($settingsKey, $settings)) {
                        $settings[$settingsKey] = '';
                    }
                }

                $this->currentSettings = $settings;

                return $this->currentSettings;
            }
        }

        // Keep the keys with empty values
        $list = array();

        foreach ($this->settingsKeys as $settingsKey) {
            $list[$settingsKey] = '';
        }

        return $list;
    }

    /**
     * @param $settings
     */
    public function update($settings)
    {
	    $wpacuUpdate = new Update;

	    $disableJQueryMigrate = (isset($_POST[WPACU_PLUGIN_NAME.'_global_unloads']['disable_jquery_migrate']));
	    $disableCommentReply = (isset($_POST[WPACU_PLUGIN_NAME.'_global_unloads']['disable_comment_reply']));

	    /*
	     * Add element(s) to the global unload rules
	     */
        if ($disableJQueryMigrate || $disableCommentReply) {
            $unloadList = array();

	        // Add jQuery Migrate to the global unload rules
            if ($disableJQueryMigrate) {
	            $unloadList[] = 'jquery-migrate';
            }

	        // Add Comment Reply to the global unload rules
	        if ($disableCommentReply) {
		        $unloadList[] = 'comment-reply';
	        }

	        $wpacuUpdate->saveToEverywhereUnloads(array(), $unloadList);
        }

        /*
         * Remove element(s) from the global unload rules
         */
        if (! $disableJQueryMigrate || ! $disableCommentReply) {
	        $removeFromUnloadList = array();

	        // Remove jQuery Migrate from global unload rules
	        if (! $disableJQueryMigrate) {
		        $removeFromUnloadList['jquery-migrate'] = 'remove';
	        }

	        // Remove Comment Reply from global unload rules
	        if (! $disableCommentReply) {
		        $removeFromUnloadList['comment-reply'] = 'remove';
	        }

	        $wpacuUpdate->removeEverywhereUnloads(array(), $removeFromUnloadList);
        }

        update_option(WPACU_PLUGIN_NAME . '_settings', json_encode($settings), 'no');
        $this->status['updated'] = true;
    }

	/**
	 *
	 */
	public function inlineHead()
    {
        ?>
        <style type="text/css">
            .wpacu_switch {
                position: relative;
                display: inline-block;
                width: 60px;
                height: 34px;
            }

            .wpacu_switch input {
                display: none;
            }

            .wpacu_slider {
                position: absolute;
                cursor: pointer;
                top: 0;
                left: 0;
                right: 0;
                bottom: 0;
                background-color: #ccc;
                -webkit-transition: .4s;
                transition: .4s;
            }

            .wpacu_slider:before {
                position: absolute;
                content: "";
                height: 26px;
                width: 26px;
                left: 4px;
                bottom: 4px;
                background-color: white;
                -webkit-transition: .4s;
                transition: .4s;
            }

            .toplevel_page_wpassetcleanup_settings input:checked + .wpacu_slider {
                background-color: #52af00;
            }

            .toplevel_page_wpassetcleanup_settings input:focus + .wpacu_slider {
                box-shadow: 0 0 1px #52af00;
            }

            .toplevel_page_wpassetcleanup_settings input:checked + .wpacu_slider:before {
                -webkit-transform: translateX(26px);
                -ms-transform: translateX(26px);
                transform: translateX(26px);
            }

            /* Rounded sliders */
            .toplevel_page_wpassetcleanup_settings .wpacu_slider.wpacu_round {
                border-radius: 34px;
            }

            .toplevel_page_wpassetcleanup_settings .wpacu_slider.wpacu_round:before {
                border-radius: 50%;
            }
        </style>
        <?php
    }
}
