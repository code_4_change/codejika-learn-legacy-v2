var save_lesson_id = "P001-L01";
var auth_id = localStorage.getItem("userid");
console.log("auth_id: "+auth_id);
if (auth_id == null){
  auth_id = Math.random().toString(36).replace('0.', '');
  localStorage.setItem("userid", auth_id);
}

var lessson_url = "nomzamos-website-01.php";

var checkpoint_id = 0;

function findIndexOfKeys(keys, value){
  var index = 1
  for(var key in keys){
    if(value == key){
      return index
    }else{
      index++
    }
  }
}

///lazy loading images
$(function() {
  const observer = lozad(); // lazy loads elements with default selector as '.lozad'
  observer.observe();
});

(function($) {
  $.createDialog = function(options) {
    $(".modal , .modal-backdrop").remove();
    // console.log("clicked " + options.modalName);
    $(".modalPlaceholder").after(
      '<div class="modal fade" id="' +
        options.modalName +
        '">\n' +
        '<div class="modal-dialog ' +
        options.popupStyle +
        '">\n' +
        '<div class="modal-content">\n' +
        '<div class="modal-body">\n' +
        options.htmlContent +
        "</div>\n" +
        "</div>\n" +
        "</div>\n" +
        "</div>\n"
    );
    $("#action_button").bind("click", options.actionButton);
    $("#close_button, .modal-backdrop").bind("click", function(e) {
      $("#" + options.modalName).modal("hide");
      $(".modal , .modal-backdrop").remove();
      $(".blocker").css("display",'none')
      $(this).removeClass('blocker')
      //inactivityTime();
      // console.log("closed " + options.modalName);
    });
    $("#" + options.modalName).modal("show");
  };
})(jQuery);

// thumnail modal
(function($) {
  $.galleryDialog = function(options) {
    $(".modal , .modal-backdrop").remove();
    console.log("clicked " + options.modalName);
    $(".modalPlaceholder").after(
      '<div class=" fade" id="' +
        options.modalName +
        '">\n' +
        '<div class="' +
        options.popupStyle +
        '">\n' +
        '<div class="modal-content">\n' +
        '<div class="">\n' +
        options.htmlContent
    );
    $("#action_button").bind("click", options.actionButton);
    $("#close_button, .modal-backdrop").bind("click", function(e) {
      $("#" + options.modalName).modal("hide");
      $(".modal , .modal-backdrop").remove();
      $(".blocker").css("display",'none')
      $(this).removeClass('blocker')
      //inactivityTime();
      console.log("closed " + options.modalName);
    });
    $("#" + options.modalName).modal("show");
  };
})(jQuery);





var lesson_mappings = {
    "coding1-t1": "P1Training1",
    "coding1-t2": "P1Training2",
    "coding1-t3": "P1Training3",
    "coding1-t4": "P1Training4",

    "coding2-t1": "P2Training1",
    "coding2-t2": "P2Training2",
    "coding2-t3": "P2Training3",
    "coding2-t4": "P2Training4",
    "coding2-t5": "P2Training5",
    "coding2-t6": "P2Training6",

    "coding3-t1": "P3Training1",
    "coding3-t2": "P3Training2",
    "coding3-t3": "P3Training3",
    "coding3-t4": "P3Training4",
    "coding3-t5": "P3Training5",
    "coding3-t6": "P3Training6",

    "lesson-m": "P1Training1",
    "P1Training1": "P1Training1",
    "P1Training2": "P1Training2",
}


function getCurrentSlideNumber() {

    var current_lesson = window.location.href;

   console.log(" lesson_mappings:  "+lesson_mappings);
    for (var key in lesson_mappings) {
        if (current_lesson.includes(key)) {
            var lesson_id = lesson_mappings[key];
            firebase.database().ref('/user_profile/' + auth_id + '/lesson_progress/' + lesson_id + '/user_checkpoint/').once('value').then(function (snapshot) {
                data = snapshot.val()
                if(data){
                  var keys = Object.keys(data)
                  var k = keys[Object.keys(data).length - 1]
                  if(data[k].user_code){
                    editor.getDoc().setValue(data[k].user_code)
                  }
                }
            })
        }
    }
}

 save_pending = false;

var current_avatar = 'robot';

function loginModal() {
  $('.modal ').remove();
  $(".blocker").css("display",'none')
  $.createDialog({
    modalName: 'login-modal',
    popupStyle: 'speech-bubble top-align ' + current_avatar + ' happy',
    // actionButton: saveLogin,
    htmlContent:
      '<div id="confirm_dialog" style="display: block;">' +
      '\t<h2 id="confirm_title">Type your name and date of birth to login.</h2>' +
      date_form +
      '\t<div id="confirm_actions">' +
      '\t\t<input id="action_button login-button" class="btn btn-encouraging light action" type="submit" value="Login">' +
      '\t</div>' +
      '<p class="sign-up register-button">Dont have an account? <span class="a-link register-button">Sign Up</span></p>' +
      '</div>'
  })
  $('.register-button').click(function () {
    registerModal()
  })

  // close modal on save
  $('.action').click(function () {
    console.log('Here is the click event');
    var first_name = document.getElementById('f_name').value
    var last_name = document.getElementById('l_name').value
    // var nick_name = document.getElementById('n_name').value
    var day = document.getElementById('B-day').value
    var month = document.getElementById('B-month').value
    var year = document.getElementById('B-year').value
    console.log(first_name, last_name, day, month, year)

    if (first_name == '') {
      document.querySelector('#f_name', '').classList.add('error-border')
      return false
    }
    else if (last_name == '') {
      document.querySelector('#l_name', '').classList.add('error-border')
      return false
    } else {

            // store to users storage
        // var ref = firebase.database().ref('/users/' + auth_id)
        var ref = firebase.database().ref('/users/')
        ref.orderByChild("first_name").equalTo(first_name).once('value').then((snapshot)=> {
          data = snapshot.val()
          var matched = false;
          if(data){
            Object.keys(data).forEach(function(k){
              var in_data = data[k];
              Fname = in_data.hasOwnProperty('first_name') ? in_data['first_name'] : '';
              Lname = in_data.hasOwnProperty('last_name') ? in_data['last_name'] : '';
              v_day = in_data.hasOwnProperty('D_O_B') ? in_data.D_O_B.hasOwnProperty('day') ? in_data.D_O_B['day'] : '' : '';
              v_month = in_data.hasOwnProperty('D_O_B') ? in_data.D_O_B.hasOwnProperty('month') ? in_data.D_O_B['month'] : '' : '';
              v_year = in_data.hasOwnProperty('D_O_B') ? in_data.D_O_B.hasOwnProperty('year') ? in_data.D_O_B['year'] : '' : '';
              console.log(Fname, Lname, v_day, v_month, v_year, "From firebase");
              console.log(first_name, last_name, day, month, year, "Inputs");
              if(Fname == first_name && Lname == last_name && v_day == day && v_month == month && v_year == year ){
                 $('#login-modal').modal('hide')
                $('.modal , .modal-backdrop').remove()
                $('.blocker').css('display', 'none');
                successProfileDetails('Login Successfull', 'Welcome back ' + Fname)
                matched = true;
                console.log("Came inside");
                localStorage.setItem('userid', k)
                auth_id = k
                var newurl =
                  window.location.protocol +
                  '//' +
                  window.location.host +
                  window.location.pathname +
                  '?' +
                  auth_id
                console.log('newurl: ' + newurl)
                window.history.pushState({ path: newurl }, '', newurl)
                getCurrentSlideNumber();
                checkUserKey()
                // break;
              }

          });
          }


          if(! matched){
            alert('Invalid credentials')
          }
        })

    }

  })
}

// CodeMirror HTMLHint Integration
(function(mod) {
  if (typeof exports == "object" && typeof module == "object") // CommonJS
    mod(require("../../lib/codemirror"));
  else if (typeof define == "function" && define.amd) // AMD
    define(["../../lib/codemirror"], mod);
  else // Plain browser env
    mod(CodeMirror);
})

(function(CodeMirror) {
  "use strict";

  CodeMirror.registerHelper("lint", "html", function(text) {
    var found = [], message;
    if (!window.HTMLHint) return found;
    var messages = HTMLHint.verify(text, ruleSets);
    for ( var i = 0; i < messages.length; i++) {
      message = messages[i];
      var startLine = message.line -1, endLine = message.line -1, startCol = message.col -1, endCol = message.col;
      found.push({
        from: CodeMirror.Pos(startLine, startCol),
        to: CodeMirror.Pos(endLine, endCol),
        message: message.message,
        severity : message.type
      });
    }
    return found;
  });
});

// ruleSets for HTMLLint
var ruleSets = {
  "tagname-lowercase": true,
  "attr-lowercase": true,
  "attr-value-double-quotes": true,
  "doctype-first": false,
  "tag-pair": true,
  "spec-char-escape": true,
  "id-unique": true,
  "src-not-empty": true,
  "attr-no-duplication": true
};

var delay;
// console.log(document.getElementById('myeditor').value);
// Initialize CodeMirror editor
var editor = CodeMirror.fromTextArea(document.getElementById('myeditor'), {
  mode: "htmlmixed",
  tabMode: "indent",
  theme: 'base16-dark',
  styleActiveLine: true,
  lineNumbers: true,
  lineWrapping: true,
  autoCloseTags: false,
  foldGutter: true,
  dragDrop : true,
  lint: true,
  gutters: ["CodeMirror-linenumbers", "CodeMirror-foldgutter", "CodeMirror-lint-markers"]
});
// console.log(editor);

// Live preview
editor.on('change', function() {
  clearTimeout(delay);
  delay = setTimeout(updatePreview, 300);
});
function updatePreview() {
  var previewFrame = document.getElementById('preview');
  var preview =  previewFrame.contentDocument ||  previewFrame.contentWindow.document;
  preview.open();
  preview.write(editor.getValue());
  preview.close();
	save_pending = true;

  //validateCheckPoint();
  validatorLessonChallenges();

	/*if (preg_match('%(<p[^>]*>.*?</p>)%i', $subject, $regs)) {
    $result = $regs[1];
} else {
    $result = "";
}*/
//console.log($("#editor").text().match('<p[^>]*>.*?</p>'));

//$("div:match('/[^a-zA-Z0-9]/')")
}
function checkIfTagExists(src, tag, betweenTag) {
   if (!betweenTag) { betweenTag = "(.*?)";}

   var re = "<"+tag+"[^>]*>\s*"+betweenTag+"\s*<\\/"+tag+">";


	//	console.log(re);
		//console.log(re2);

    return new RegExp(re).test(src);
}




//Function to return correct Lesson validator for given lesson
function validatorLessonChallenges(){
  let searchParams = new URLSearchParams(window.location.search);
  if(searchParams.has('lesson')){

    //get lesson number/name if is provided on URL
      lessonName = searchParams.get('lesson');

      //call corresponding validator method
      if(lessonName == "lesson002"){
        validateCheckPointLesson2();
      }
      else if(lessonName == "project1"){
        validateCheckPointProject1();
      }
      else if(lessonName == "p1-t01"){
        validateCheckPointP01_T001_D();
      }
      else if(lessonName == "p1-t02"){
        validateCheckPointP01_T002_D();
      }
      else if(lessonName == "p1-t03"){
        validateCheckPointP01_T003_D();
      }
      else if(lessonName == "p1-t04"){
        validateCheckPointP01_T004_D();
      }
      else if(lessonName == "p2-t01"){
        validateCheckPointP02_T001_D();
      }
      else if(lessonName == "p2-t02"){
        validateCheckPointP02_T002_D();
      }
      else if(lessonName == "p2-t03"){
        validateCheckPointP02_T003_D();
      }
      else if(lessonName == "p2-t04"){
        validateCheckPointP02_T004_D();
      }
      else if(lessonName == "p2-t05"){
        validateCheckPointP02_T005_D();
      }
      else if(lessonName == "p2-t06"){
        validateCheckPointP02_T006_D();
      }
      else if(lessonName == "p3-t01"){
        validateCheckPointP03_T001_D();
      }
      else if(lessonName == "p3-t02"){
        validateCheckPointP03_T002_D();
      }
      else if(lessonName == "p3-t03"){
        validateCheckPointP03_T003_D();
      }
      else if(lessonName == "p3-t04"){
        validateCheckPointP03_T004_D();
      }
      else if(lessonName == "p3-t05"){
        validateCheckPointP03_T005_D();
      }
      else if(lessonName == "p3-t06"){
        validateCheckPointP03_T006_D();
      }
  }
  else{
      //get lesson1(default) validator if lesson is not provided on URL
      validateCheckPoint();
  }
}

/*========== Lesson named P00-T00-D meaning project, taining, and desktop platform ====
  ========== validator functions for each training =====*/

//Function to validate challenges on P01-T001-D

function validateCheckPointP01_T001_D() {
    
	var check_points = {
    16:"(<head>|<head [^>]*>)((.|\n)*)\s*<\/head>",
    17: "(<head>|<head [^>]*>)((.|\n)*)\s*<\/head>((.|\n)*)\s*(<body>|<body [^>]*>)((.|\n)*)\s*<\/body>",
    23: "(<head>|<head [^>]*>)((.|\n)*)\s*<\/head>((.|\n)*)\s*(<body>|<body [^>]*>)((.|\n)*)\s*(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>((.|\n)*)\s*<\/body>",
    24: "(<head>|<head [^>]*>)((.|\n)*)\s*<\/head>((.|\n)*)\s*(<body>|<body [^>]*>)((.|\n)*)\s*(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>((.|\n)*)\s*<\/body>",
    32: "(<head>|<head [^>]*>)((.|\n)*)\s*<\/head>((.|\n)*)\s*(<body>|<body [^>]*>)((.|\n)*)\s*(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>((.|\n)*)\s*(<h3>|<h3 [^>]*>)((.|\n)*)\s*Soon...((.|\n)*)\s*<\/h3>((.|\n)*)\s*<\/body>",
    33: "(<head>|<head [^>]*>)((.|\n)*)\s*<\/head>((.|\n)*)\s*(<body>|<body [^>]*>)((.|\n)*)\s*(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>((.|\n)*)\s*(<h3>|<h3 [^>]*>)((.|\n)*)\s*<\/h3>((.|\n)*)\s*<\/body>",
    36:"(<p>|<p [^>]*>)((.|\n)*)\s*(2019|2020|2021|2022|2023|2024|2025|2026|2027|2028|2029|2030)((.|\n)*)\s*<\/p>"
         // 14: "(<p>|<p [^>]*>)\s*(.*?)\s*<\/p>",
		// 19: "(<input( \s*|[^]* )\s*type\s*=\s*\"\s*email\s*\"\s*[^>]*>)",
		// 23: "(<input( \s*|[^]* )\s*type\s*=\s*\"\s*submit\s*\"\s*[^>]*>)",
		// 27: "(?=(<input( \s*|[^]* )\s*type\s*=\s*\"\s*email\s*\"\s*[^>]*>))(?=(<input( \s*|[^]* )\s*placeholder\s*=\s*\"\s*Your\s* \s*email\s*\"\s*[^>]*>))",
		// 219: "<h2 [^>]*>\s*Hi\s*Tembi\s*<\\/h2>",
		// 318: "^\s*Date\s*\(\s*\)\s*$"
	}
  var hintsForCheckPonts = {
    16:"A closing tag has a forward slash <strong>/<strong>",
    17:"Remember to open <span class='html-code'> &ldquo;&lt;&gt;&rdquo;</span> and close <span class='html-code'>&ldquo;&lt;/&gt;&rdquo;</span> your tag. Refer to the previous challenge(step 1) on how to open and close a tag.",
    23:"In the body section. Hint given as tip on bottom of the slide.",
    24:"Example: <strong><span class='html-code'>&lt;h1&gt;</span>John Doe <span class='html-code'>&lt/h1&gt</span></strong>",
    32:"Type your code below the closing <strong><span class='html-code'>&lt/h1&gt;</span></strong> tag.",
    36:"Just below the <strong><span class='html-code'>&lt;/h3&gt;</span></strong> tag"
  }
  if(hintsForCheckPonts[currentStep] === undefined ){
      $(".hint-popup").hide();
  }
	if( check_points[currentStep] !== undefined ) {

    // do something
		// console.log('Run validation only on '+currentStep);
		var re = check_points[currentStep];
		var reResults = new RegExp(re, "i").test(editor.getValue());
	 		console.log('Run validation results '+reResults);
       if (reResults) {
        $("#slide"+currentStep+" .check-icon").html('/');
        $("#slide"+currentStep+" .check").html('I did it →').addClass('passed');
        $("#slide"+currentStep+" .actions li").addClass('correct');
             // $(".pagination .next").addClass('disabled');
        $(".pagination .next").removeClass('disabled');
       saveUserData(true, findIndexOfKeys(check_points, currentStep), "P1Training1");
      } else {
        if(!$(".hint-popup-checked").hasClass("checked"+currentStep)){
          $(".hint-popup-checked").addClass("checked"+currentStep);
          if(!$("#slide"+currentStep+" .check").hasClass('passed')){
            if( hintsForCheckPonts[currentStep] !== undefined ){
              htmlTagToString = hintsForCheckPonts[currentStep];
              $(".hint-text").html(htmlTagToString);
              $(".hint-popup").delay(5000).fadeIn().delay(3000).fadeOut(5000);
              //$(".hint-popup").show().delay(10000).fadeOut();
             // $(".hint-popup").show().delay(5000).fadeOut();
              

              // $(".hint-popup").mouseover(function(){
              //   $(this).stop();
              // });
              // $(".hint-popup").mouseout(function(){
              //   $(this).show().delay(10000).fadeOut();
              // });

              $(".close-hint-popup").click(function(){
                  $(".hint-popup").clearQueue();
                  $(".hint-popup").fadeOut();
                  
              });

              $(".hint-popup").click(function(){
                $(".hint-popup").clearQueue();
                $(".hint-popup").fadeOut();
            });

              
            }
          }
        }

        $("#slide"+currentStep+" .check-icon").html('\\');
        $("#slide"+currentStep+" .actions li").removeClass('correct');
        $("#slide"+currentStep+" .check").html('Skip this →').removeClass('passed');;
        $(".pagination .next").addClass('disabled');
      }
  }


	var unlockSkills = {
		16: "head",
    17: "body",
    23: "h1-h6",
		36: "p",
		24: "submit-input"
  }

	if( unlockSkills[currentStep] !== undefined ) {
				$("#"+unlockSkills[currentStep]+"-skill").addClass('has-skill-true');
				$("#"+unlockSkills[currentStep]+"-skill").removeClass('has-skill-false');
		//console.log('Unlocked skill '+unlockSkills[currentStep]);
	//	console.log("#"+currentStep+"-skill");

	  $("#"+unlockSkills[currentStep]+"-skill").attr('data-original-title', $("#"+unlockSkills[currentStep]+"-skill").attr('data-title-text')).tooltip('show').tooltip('hide');
	 //$("#"+unlockSkills[currentStep]+"-skill").attr('data-original-title',"passwords did not match").tooltip('show').tooltip('hide');
	  //	  console.log("#"+unlockSkills[currentStep]+"-skill");
	  //console.log($("#"+unlockSkills[currentStep]+"-skill").attr('data-title-text'));
	}
}

//Function to validate challenges on P01-T002-D
function validateCheckPointP01_T002_D() {
	var check_points = {
    12:"(<style>|<style [^>]*>)((.|\n)*)\s*<\/style>",
    22:"(<style>|<style [^>]*>)((.|\n)*)\s*h1((.|\n)*)\s*{((.|\n)*)\s*}((.|\n)*)\s*<\/style>",
    23:"(<style>|<style [^>]*>)((.|\n)*)\s*h1((.|\n)*)\s*{((.|\n)*)\s*font-size((.|\n)*)\s*75px;((.|\n)*)\s*}((.|\n)*)\s*<\/style>",
    27:"(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>((.|\n)*)\s*Motivation",
    28:"(<h3>|<h3 [^>]*>)((.|\n)*)\s*(Motivation|MOTIVATION)((.|\n)*)<\/h3>",
    //28:"<h3>|<h3 [^>]*>)((.|\n)*)(\s*Motivation)((.|\n)*)\s*<\/h3>",
    31:"(<h3>|<h3 [^>]*>)((.|\n)*)\s*Motivation((.|\n)*)\s*<\/h3>",
    37:"(<h3>|<h3 [^>]*>)((.|\n)*)\s*Motivation((.|\n)*)\s*(<i>|<i [^>]*>)((.|\n)*)\s*<\/i>((.|\n)*)\s*<\/h3>",
    39:"(<style>|<style [^>]*>)((.|\n)*)\s*i((.|\n)*)\s*{((.|\n)*)\s*font-size((.|\n)*)\s*:((.|\n)*)\s*25px((.|\n)*)\s*;((.|\n)*)\s*}((.|\n)*)\s*<\/style>",
    44:"(<h3>|<h3 [^>]*>)((.|\n)*)\s*(Motivation:|Motivation)((.|\n)*)\s*(<br>|<br [^>]*>)((.|\n)*)\s*(<br>|<br [^>]*>)((.|\n)*)\s*<\/h3>"
  }

  var hintsForCheckPonts = {
    12:"Inside the &lt;head&gt; <strong>Type Code Here</strong> &lt;\/head&gt; tags",
    22:"Inside the &lt;style&gt; <strong>Type Code Here</strong> &lt;\/style&gt; section",
    23:"Remember to wrap your content inside the curley braces h1<strong>{Type Code Here}</strong>"
  }
  if(hintsForCheckPonts[currentStep] === undefined ){
      $(".hint-popup").hide();
  }

	if( check_points[currentStep] !== undefined ) {
    // do something
		// console.log('Run validation only on '+currentStep);
		var re = check_points[currentStep];
		var reResults = new RegExp(re, "i").test(editor.getValue());
	 		console.log('Run validation results '+reResults);
       if (reResults) {
        $("#slide"+currentStep+" .check-icon").html('/');
        $("#slide"+currentStep+" .check").html('I did it →').addClass('passed');
        $("#slide"+currentStep+" .actions li").addClass('correct');
             // $(".pagination .next").addClass('disabled');
        $(".pagination .next").removeClass('disabled');
            console.log('Run validation using '+check_points[currentStep]);
       saveUserData(true, findIndexOfKeys(check_points, currentStep), "P1Training2");
      } else {

        if(!$(".hint-popup-checked").hasClass("checked"+currentStep)){
          $(".hint-popup-checked").addClass("checked"+currentStep);
          if(!$("#slide"+currentStep+" .check").hasClass('passed')){
            if( hintsForCheckPonts[currentStep] !== undefined ){
              htmlTagToString = hintsForCheckPonts[currentStep];
              $(".hint-text").html(htmlTagToString);
              //$(".hint-popup").show().delay(10000).fadeOut();
              $(".hint-popup").show().delay(5000).fadeOut();

              $(".close-hint-popup").click(function(){
                  $(".hint-popup").clearQueue();
                  $(".hint-popup").fadeOut();
              });
            }
          }
        }

        $("#slide"+currentStep+" .check-icon").html('\\');
        $("#slide"+currentStep+" .actions li").removeClass('correct');
        $("#slide"+currentStep+" .check").html('Skip this →').removeClass('passed');;
        $(".pagination .next").addClass('disabled');
      }
	}

	var unlockSkills = {
		12: "h1-h6",
		15: "p",
		20: "email-input",
		24: "submit-input"
	}

	if( unlockSkills[currentStep] !== undefined ) {
				$("#"+unlockSkills[currentStep]+"-skill").addClass('has-skill-true');
				$("#"+unlockSkills[currentStep]+"-skill").removeClass('has-skill-false');
		//console.log('Unlocked skill '+unlockSkills[currentStep]);
	//	console.log("#"+currentStep+"-skill");

	  $("#"+unlockSkills[currentStep]+"-skill").attr('data-original-title', $("#"+unlockSkills[currentStep]+"-skill").attr('data-title-text')).tooltip('show').tooltip('hide');
	 //$("#"+unlockSkills[currentStep]+"-skill").attr('data-original-title',"passwords did not match").tooltip('show').tooltip('hide');
	  //	  console.log("#"+unlockSkills[currentStep]+"-skill");
	  //console.log($("#"+unlockSkills[currentStep]+"-skill").attr('data-title-text'));
	}
}

//Function to validate challenges on P01-T003-D
function validateCheckPointP01_T003_D() {
	var check_points = {
    18:"(<header>|<header [^>]*>)((.|\n)*)\s*(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>((.|\n)*)\s*<\/header>",
    24:"(<style>|<style [^>]*>)((.|\n)*)\s*header((.|\n)*)\s*{((.|\n)*)\s*}((.|\n)*)\s*<\/style>",
    25:"(<style>|<style [^>]*>)((.|\n)*)\s*header((.|\n)*)\s*{((.|\n)*)\s*background((.|\n)*)\s*:((.|\n)*)\s*linear-gradient((.|\n)*)\s*}((.|\n)*)\s*<\/style>",
    26:"(<style>|<style [^>]*>)((.|\n)*)\s*header((.|\n)*)\s*{((.|\n)*)\s*background((.|\n)*)\s*:((.|\n)*)\s*linear-gradient((.|\n)*)\s*[(]100deg((.|\n)*)\s*,((.|\n)*)\s*yellow((.|\n)*)\s*40%((.|\n)*)\s*,((.|\n)*)\s*pink((.|\n)*)\s*40%((.|\n)*)\s*[)]((.|\n)*)\s*;((.|\n)*)\s*}((.|\n)*)\s*<\/style>",
    34:"(<section>|<section [^>]*>)((.|\n)*)\s*(<h3>|<h3 [^>]*>)((.|\n)*)\s*motivation((.|\n)*)\s*<\/h3>((.|\n)*)\s*<\/section>",
    35:"(<style>|<style [^>]*>)((.|\n)*)section((.|\n)*)\s*{((.|\n)*)\s*(background|background-color)((.|\n)*)\s*:((.|\n)*)\s*lightgrey((.|\n)*)\s*<\/style>",
    39:"(<footer>|<footer [^>]*>)((.|\n)*)\s*<\/footer>",
    42:"(<footer>|<footer [^>]*>)((.|\n)*)\s*&copy((.|\n)*)\s*<\/footer>",
    44:"(<style>|<style [^>]*>)((.|\n)*)footer((.|\n)*)\s*{((.|\n)*)\s*background((.|\n)*)\s*black((.|\n)*)\s*color((.|\n)*)\s*white((.|\n)*)\s*}((.|\n)*)\s*<\/style>"
  }
  var hintsForCheckPonts = {
    18:"Open it before the h1 tag and close it after the closing p tag"
  } 
  if(hintsForCheckPonts[currentStep] === undefined ){
      $(".hint-popup").hide();
  }
	if( check_points[currentStep] !== undefined ) {
    // do something
		// console.log('Run validation only on '+currentStep);
		var re = check_points[currentStep];
		var reResults = new RegExp(re, "i").test(editor.getValue());
	 		console.log('Run validation results '+reResults);
       if (reResults) {
        $("#slide"+currentStep+" .check-icon").html('/');
        $("#slide"+currentStep+" .check").html('I did it →').addClass('passed');
        $("#slide"+currentStep+" .actions li").addClass('correct');
             // $(".pagination .next").addClass('disabled');
        $(".pagination .next").removeClass('disabled');
            console.log('Run validation using '+check_points[currentStep]);
            saveUserData(true, findIndexOfKeys(check_points, currentStep), "P1Training3");
      } else {
        if(!$(".hint-popup-checked").hasClass("checked"+currentStep)){
          $(".hint-popup-checked").addClass("checked"+currentStep);
          if(!$("#slide"+currentStep+" .check").hasClass('passed')){
            if( hintsForCheckPonts[currentStep] !== undefined ){
              htmlTagToString = hintsForCheckPonts[currentStep];
              $(".hint-text").html(htmlTagToString);
              $(".hint-popup").show().delay(5000).fadeOut();
              //$(".hint-popup").show().delay(10000).fadeOut();
              $(".close-hint-popup").click(function(){
                  $(".hint-popup").clearQueue();
                  $(".hint-popup").fadeOut();
              });
            }
          }
        }

        $("#slide"+currentStep+" .check-icon").html('\\');
        $("#slide"+currentStep+" .actions li").removeClass('correct');
        $("#slide"+currentStep+" .check").html('Skip this →').removeClass('passed');;
        $(".pagination .next").addClass('disabled');
      }
	}

	var unlockSkills = {
		12: "head",
    17: "body",
    23: "h1-h6",
		20: "email-input",
		24: "submit-input"
	}

	if( unlockSkills[currentStep] !== undefined ) {
				$("#"+unlockSkills[currentStep]+"-skill").addClass('has-skill-true');
				$("#"+unlockSkills[currentStep]+"-skill").removeClass('has-skill-false');
		//console.log('Unlocked skill '+unlockSkills[currentStep]);
	//	console.log("#"+currentStep+"-skill");

	  $("#"+unlockSkills[currentStep]+"-skill").attr('data-original-title', $("#"+unlockSkills[currentStep]+"-skill").attr('data-title-text')).tooltip('show').tooltip('hide');
	 //$("#"+unlockSkills[currentStep]+"-skill").attr('data-original-title',"passwords did not match").tooltip('show').tooltip('hide');
	  //	  console.log("#"+unlockSkills[currentStep]+"-skill");
	  //console.log($("#"+unlockSkills[currentStep]+"-skill").attr('data-title-text'));
	}
}

//Function to validate challenges on P01-T004-D
function validateCheckPointP01_T004_D() {
	var check_points = {
    9:"(<header>|<header [^>]*>)((.|\n)*)\s*(<div>|<div [^>]*>)((.|\n)*)\s*(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>((.|\n)*)\s*<\/div>((.|\n)*)\s*<\/header>",
    12:"(<section>|<section [^>]*>)((.|\n)*)\s*(<div>|<div [^>]*>)((.|\n)*)\s*(<h3>|<h3 [^>]*>)((.|\n)*)\s*<\/h3>((.|\n)*)\s*<\/div>((.|\n)*)\s*<\/section>",
    16:"(<footer>|<footer [^>]*>)((.|\n)*)\s*(<div>|<div [^>]*>)((.|\n)*)\s*<\/div>((.|\n)*)\s*<\/footer>",
    24:"(<style>|<style [^>]*>)((.|\n)*)\s*div((.|\n)*)\s*{((.|\n)*)\s*}((.|\n)*)\s*<\/style>",
    26:"(<style>|<style [^>]*>)((.|\n)*)\s*div((.|\n)*)\s*{((.|\n)*)\s*text-align((.|\n)*)\s*center((.|\n)*)\s*}((.|\n)*)\s*<\/style>",
    29:"(<style>|<style [^>]*>)((.|\n)*)\s*div((.|\n)*)\s*{((.|\n)*)\s*padding((.|\n)*)\s*:((.|\n)*)\s*40px((.|\n)*)\s*}((.|\n)*)\s*<\/style>",
    33:"(<style>|<style [^>]*>)((.|\n)*)\s*h3((.|\n)*)\s*{((.|\n)*)\s*}((.|\n)*)\s*<\/style>",
    36:"(<style>|<style [^>]*>)((.|\n)*)\s*h3((.|\n)*)\s*{((.|\n)*)\s*border((.|\n)*)\s*:((.|\n)*)\s*((white((.|\n)*)\s*solid((.|\n)*)\s*2px)|(solid((.|\n)*)\s*2px((.|\n)*)\s*white)|(2px((.|\n)*)\s*solid((.|\n)*)\s*white))((.|\n)*)\s*}((.|\n)*)\s*<\/style>",
    37:"(<style>|<style [^>]*>)((.|\n)*)\s*h3((.|\n)*)\s*{((.|\n)*)\s*font-size((.|\n)*)\s*45px((.|\n)*)\s*padding((.|\n)*)\s*15px((.|\n)*)\s*}((.|\n)*)\s*<\/style>",
    39:"(<style>|<style [^>]*>)((.|\n)*)\s*h3((.|\n)*)\s*{((.|\n)*)\s*margin((.|\n)*)\s*auto((.|\n)*)\s*}((.|\n)*)\s*<\/style>",
    41:"(<style>|<style [^>]*>)((.|\n)*)\s*h3((.|\n)*)\s*{((.|\n)*)\s*max-width((.|\n)*)\s*400px((.|\n)*)\s*}((.|\n)*)\s*<\/style>",
    45:"(<style>|<style [^>]*>)((.|\n)*)\s*h1((.|\n)*)\s*{((.|\n)*)\s*color((.|\n)*)\s*white((.|\n)*)\s*}((.|\n)*)\s*<\/style>"
  }
  var hintsForCheckPonts = {
    9:"The &lt;div&gt; tag defines a division or a section in an HTML document.",
    16:"<span class='html-code'><strong>&lt;footer&gt;</strong></span> Insert div here <span class='html-code'><strong>&lt;\/footer&gt;</strong></span>",
    26:"Use the <strong>text-align</strong> property",
  }
  if(hintsForCheckPonts[currentStep] === undefined ){
      $(".hint-popup").hide();
  }

	if( check_points[currentStep] !== undefined ) {
    // do something
		// console.log('Run validation only on '+currentStep);
		var re = check_points[currentStep];
		var reResults = new RegExp(re, "i").test(editor.getValue());
	 		console.log('Run validation results '+reResults);
       if (reResults) {
        $("#slide"+currentStep+" .check-icon").html('/');
        $("#slide"+currentStep+" .check").html('I did it →').addClass('passed');
        $("#slide"+currentStep+" .actions li").addClass('correct');
             // $(".pagination .next").addClass('disabled');
        $(".pagination .next").removeClass('disabled');
            console.log('Run validation using '+check_points[currentStep]);
            saveUserData(true, findIndexOfKeys(check_points, currentStep), "P1Training4");

      } else {
        if(!$(".hint-popup-checked").hasClass("checked"+currentStep)){
          $(".hint-popup-checked").addClass("checked"+currentStep);
          if(!$("#slide"+currentStep+" .check").hasClass('passed')){
            if( hintsForCheckPonts[currentStep] !== undefined ){
              htmlTagToString = hintsForCheckPonts[currentStep];
              $(".hint-text").html(htmlTagToString);
             
              $(".hint-popup").show().delay(5000).fadeOut();
              //$(".hint-popup").show().delay(10000).fadeOut();
              $(".close-hint-popup").click(function(){
                  $(".hint-popup").clearQueue();
                  $(".hint-popup").fadeOut();
              });
            }
          }
        }

        $("#slide"+currentStep+" .check-icon").html('\\');
        $("#slide"+currentStep+" .actions li").removeClass('correct');
        $("#slide"+currentStep+" .check").html('Skip this →').removeClass('passed');;
        $(".pagination .next").addClass('disabled');
      }
	}

	var unlockSkills = {
		12: "h1-h6",
		15: "p",
		20: "email-input",
		24: "submit-input"
	}

	if( unlockSkills[currentStep] !== undefined ) {
				$("#"+unlockSkills[currentStep]+"-skill").addClass('has-skill-true');
				$("#"+unlockSkills[currentStep]+"-skill").removeClass('has-skill-false');
		//console.log('Unlocked skill '+unlockSkills[currentStep]);
	//	console.log("#"+currentStep+"-skill");

	  $("#"+unlockSkills[currentStep]+"-skill").attr('data-original-title', $("#"+unlockSkills[currentStep]+"-skill").attr('data-title-text')).tooltip('show').tooltip('hide');
	 //$("#"+unlockSkills[currentStep]+"-skill").attr('data-original-title',"passwords did not match").tooltip('show').tooltip('hide');
	  //	  console.log("#"+unlockSkills[currentStep]+"-skill");
	  //console.log($("#"+unlockSkills[currentStep]+"-skill").attr('data-title-text'));
	}
}




//Function to validate challenges on P02-T001-D
function validateCheckPointP02_T001_D() {
var check_points = {
    9:"(!Doctype html>)((.|\n)*)\s*(<head>|<head [^>]*>)((.|\n)*)\s*(<style>|<style [^>]*>)((.|\n)*)\s*<\/style>((.|\n)*)\s*<\/head>",
    10:"((.|\n)*)\s*<\/head>((.|\n)*)\s*(<body>|<body [^>]*>)((.|\n)*)\s*<\/body>",
    13:"((.|\n)*)\s*(<body>|<body [^>]*>)((.|\n)*)\s*(<header>|<header [^>]*>)((.|\n)*)\s*(<\/header>)((.|\n)*)\s*<\/body>",
    14:"((.|\n)*)\s*(<body>|<body [^>]*>)((.|\n)*)\s*(<header>|<header [^>]*>)((.|\n)*)\s*<h1>((.|\n)*)\s*<\/h1>((.|\n)*)\s*(<\/header>)((.|\n)*)\s*<\/body>",
    20:"((.|\n)*)\s*(<style>|<style [^>]*>)((.|\n)*)\s*header((.|\n)*)\s*{((.|\n)*)\s*}((.|\n)*)\s*<\/style>((.|\n)*)\s*",
    21:"(<style>|<style [^>]*>)((.|\n)*)header((.|\n)*)\s*{((.|\n)*)\s*(background|background-color)((.|\n)*)\s*:((.|\n)*)\s*lightblue;((.|\n)*)\s*<\/style>",
    22:"(<style>|<style [^>]*>)((.|\n)*)header((.|\n)*)\s*{((.|\n)*)\s*(border)((.|\n)*)\s*:((.|\n)*)\s*solid((.|\n)*)\s*blue;((.|\n)*)\s*<\/style>",
    25:"(<style>|<style [^>]*>)((.|\n)*)header((.|\n)*)\s*{((.|\n)*)\s*(border-width)((.|\n)*)\s*:((.|\n)*)\s*10px((.|\n)*)\s*0px((.|\n)*)\s*10px((.|\n)*)\s*0px;((.|\n)*)\s*<\/style>",
   
	}
  var hintsForCheckPonts = {
    10:"Type your code below the closing <span class='html-code'><strong>&lt;\/head&gt;</strong></span> tag.",
    13:"<span class='html-code'><strong>&lt;body&gt;</strong></span> Type Code Here <span class='html-code'><strong>&lt;\/body&gt;</strong></span>",
    20:"<span class='html-code'><strong>&lt;style&gt;</strong></span> Type Code Here <span class='html-code'><strong>&lt;\/style&gt;</strong></span>",
  }
  if(hintsForCheckPonts[currentStep] === undefined ){
      $(".hint-popup").hide();
  }
	if( check_points[currentStep] !== undefined ) {

    // do something
		// console.log('Run validation only on '+currentStep);
		var re = check_points[currentStep];
		var reResults = new RegExp(re, "i").test(editor.getValue());
	 		console.log('Run validation results '+reResults);
       if (reResults) {
        $("#slide"+currentStep+" .check-icon").html('/');
        $("#slide"+currentStep+" .check").html('I did it →').addClass('passed');
        $("#slide"+currentStep+" .actions li").addClass('correct');
             // $(".pagination .next").addClass('disabled');
        $(".pagination .next").removeClass('disabled');
       saveUserData(true, findIndexOfKeys(check_points, currentStep), "P1Training1");
      } else {
        if(!$(".hint-popup-checked").hasClass("checked"+currentStep)){
          $(".hint-popup-checked").addClass("checked"+currentStep);
          if(!$("#slide"+currentStep+" .check").hasClass('passed')){
            if( hintsForCheckPonts[currentStep] !== undefined ){
              htmlTagToString = hintsForCheckPonts[currentStep];
              $(".hint-text").html(htmlTagToString);
              $(".hint-popup").delay(5000).fadeIn().delay(3000).fadeOut(5000);
              //$(".hint-popup").show().delay(10000).fadeOut();
             // $(".hint-popup").show().delay(5000).fadeOut();
              

              // $(".hint-popup").mouseover(function(){
              //   $(this).stop();
              // });
              // $(".hint-popup").mouseout(function(){
              //   $(this).show().delay(10000).fadeOut();
              // });

              $(".close-hint-popup").click(function(){
                  $(".hint-popup").clearQueue();
                  $(".hint-popup").fadeOut();
                  
              });

              $(".hint-popup").click(function(){
                $(".hint-popup").clearQueue();
                $(".hint-popup").fadeOut();
            });

              
            }
          }
        }

        $("#slide"+currentStep+" .check-icon").html('\\');
        $("#slide"+currentStep+" .actions li").removeClass('correct');
        $("#slide"+currentStep+" .check").html('Skip this →').removeClass('passed');;
        $(".pagination .next").addClass('disabled');
      }
  }


	var unlockSkills = {
		12: "h1-h6",
		15: "p",
		20: "email-input",
		24: "submit-input"
	}

	if( unlockSkills[currentStep] !== undefined ) {
				$("#"+unlockSkills[currentStep]+"-skill").addClass('has-skill-true');
				$("#"+unlockSkills[currentStep]+"-skill").removeClass('has-skill-false');
		//console.log('Unlocked skill '+unlockSkills[currentStep]);
	//	console.log("#"+currentStep+"-skill");

	  $("#"+unlockSkills[currentStep]+"-skill").attr('data-original-title', $("#"+unlockSkills[currentStep]+"-skill").attr('data-title-text')).tooltip('show').tooltip('hide');
	 //$("#"+unlockSkills[currentStep]+"-skill").attr('data-original-title',"passwords did not match").tooltip('show').tooltip('hide');
	  //	  console.log("#"+unlockSkills[currentStep]+"-skill");
	  //console.log($("#"+unlockSkills[currentStep]+"-skill").attr('data-title-text'));
	}
}

//Function to validate challenges on P02-T002-D
function validateCheckPointP02_T002_D() {
var check_points = {
    11:"(<style>|<style [^>]*>)((.|\n)*)header((.|\n)*)\s*{((.|\n)*)\s*(margin)((.|\n)*)\s*:((.|\n)*)\s*0((.|\n)*)\s*auto((.|\n)*)\s*;((.|\n)*)\s*<\/style>",
    13:"(<style>|<style [^>]*>)((.|\n)*)header((.|\n)*)\s*{((.|\n)*)\s*(text-align)((.|\n)*)\s*:((.|\n)*)\s*center((.|\n)*)\s*;((.|\n)*)\s*<\/style>",
    19:"((.|\n)*)\s*<\/h1>((.|\n)*)\s*(<\/header>|<\/header [^>]*>)((.|\n)*)\s*",
    21:"((.|\n)*)\s*<i>((.|\n)*)\s*(<\/i>|<\/i [^>]*>)((.|\n)*)\s*",
    25:"(<style>|<style [^>]*>)((.|\n)*)h1((.|\n)*)\s*{((.|\n)*)\s*(font-size)((.|\n)*)\s*:((.|\n)*)\s*3em((.|\n)*)\s*;((.|\n)*)\s*<\/style>",
    29:"((.|\n)*)\s*(<\/i>)((.|\n)*)\s*(<br>)((.|\n)*)\s*(<br>)((.|\n)*)\s*<\/header>((.|\n)*)\s*(<br>)",
    33:"((.|\n)*)\s*<h3>((.|\n)*)\s*(DETAILS)((.|\n)*)\s*<\/h3>",
    34:"((.|\n)*)\s*<\/h3>((.|\n)*)\s*<p>((.|\n)*)\s*((.|\n)*)\s*<\/p>",
    36:"((.|\n)*)\s*<\/p>((.|\n)*)\s*<p>((.|\n)*)\s*((.|\n)*)\s*<\/p>",
    37:"((.|\n)*)\s*<p>((.|\n)*)\s*<\/p>((.|\n)*)\s*<p>((.|\n)*)\s*<\/p>((.|\n)*)\s*<p>((.|\n)*)\s*((.|\n)*)\s*<\/p>",
   
	}
  var hintsForCheckPonts = {
    11:"Type your code below the inside the <strong>header{ }</strong> selector in style.",

  
  }
  if(hintsForCheckPonts[currentStep] === undefined ){
      $(".hint-popup").hide();
  }
	if( check_points[currentStep] !== undefined ) {

    // do something
		// console.log('Run validation only on '+currentStep);
		var re = check_points[currentStep];
		var reResults = new RegExp(re, "i").test(editor.getValue());
	 		console.log('Run validation results '+reResults);
       if (reResults) {
        $("#slide"+currentStep+" .check-icon").html('/');
        $("#slide"+currentStep+" .check").html('I did it →').addClass('passed');
        $("#slide"+currentStep+" .actions li").addClass('correct');
             // $(".pagination .next").addClass('disabled');
        $(".pagination .next").removeClass('disabled');
       saveUserData(true, findIndexOfKeys(check_points, currentStep), "P1Training1");
      } else {
        if(!$(".hint-popup-checked").hasClass("checked"+currentStep)){
          $(".hint-popup-checked").addClass("checked"+currentStep);
          if(!$("#slide"+currentStep+" .check").hasClass('passed')){
            if( hintsForCheckPonts[currentStep] !== undefined ){
              htmlTagToString = hintsForCheckPonts[currentStep];
              $(".hint-text").html(htmlTagToString);
              $(".hint-popup").delay(5000).fadeIn().delay(3000).fadeOut(5000);
              //$(".hint-popup").show().delay(10000).fadeOut();
             // $(".hint-popup").show().delay(5000).fadeOut();
              

              // $(".hint-popup").mouseover(function(){
              //   $(this).stop();
              // });
              // $(".hint-popup").mouseout(function(){
              //   $(this).show().delay(10000).fadeOut();
              // });

              $(".close-hint-popup").click(function(){
                  $(".hint-popup").clearQueue();
                  $(".hint-popup").fadeOut();
                  
              });

              $(".hint-popup").click(function(){
                $(".hint-popup").clearQueue();
                $(".hint-popup").fadeOut();
            });

              
            }
          }
        }

        $("#slide"+currentStep+" .check-icon").html('\\');
        $("#slide"+currentStep+" .actions li").removeClass('correct');
        $("#slide"+currentStep+" .check").html('Skip this →').removeClass('passed');;
        $(".pagination .next").addClass('disabled');
      }
  }


	var unlockSkills = {
		12: "h1-h6",
		15: "p",
		20: "email-input",
		24: "submit-input"
	}

	if( unlockSkills[currentStep] !== undefined ) {
				$("#"+unlockSkills[currentStep]+"-skill").addClass('has-skill-true');
				$("#"+unlockSkills[currentStep]+"-skill").removeClass('has-skill-false');
		//console.log('Unlocked skill '+unlockSkills[currentStep]);
	//	console.log("#"+currentStep+"-skill");

	  $("#"+unlockSkills[currentStep]+"-skill").attr('data-original-title', $("#"+unlockSkills[currentStep]+"-skill").attr('data-title-text')).tooltip('show').tooltip('hide');
	 //$("#"+unlockSkills[currentStep]+"-skill").attr('data-original-title',"passwords did not match").tooltip('show').tooltip('hide');
	  //	  console.log("#"+unlockSkills[currentStep]+"-skill");
	  //console.log($("#"+unlockSkills[currentStep]+"-skill").attr('data-title-text'));
	}
}

//Function to validate challenges on P02-T003-D
function validateCheckPointP02_T003_D() {
    var check_points = {
    11:"((.|\n)\s*)(.box)((.|\n)*)\s*{((.|\n)*)\s*(height)((.|\n)*)\s*:((.|\n)*)\s*250px;((.|\n)*)\s*width((.|\n)*)\s*:((.|\n)*)\s*250px;((.|\n)*)\s*",
    12:"((.|\n)*)\s*(<div((.)*)\s* class=\"box\"((.|\n)*)\s*>)((.|\n)*)\s*(<\/div>)((.|\n)*)\s*(<h1>)",
    14:"((.|\n)\s*)(.box)((.|\n)*)\s*{((.|\n)*)\s*(height)((.|\n)*)\s*:((.|\n)*)\s*250px;((.|\n)*)\s*width((.|\n)*)\s*:((.|\n)*)\s*250px;((.|\n)*)\s*background((.|\n)*)\s*:((.|\n)*)\s*blue;((.|\n)*)\s*",
    24:"((.|\n)*)\s*width((.|\n)*)\s*:((.|\n)*)\s*250px;((.|\n)*)\s*background((.|\n)*)\s*:((.|\n)*)\s*blue;((.|\n)*)\s*border-radius((.|\n)*)\s*:((.|\n)*)\s*50%;((.|\n)*)\s*",
    25:"((.|\n)*)\s*(<div((.)*)\s* class=\"circle\"((.|\n)*)\s*>)((.|\n)*)\s*(<\/div>)((.|\n)*)\s*(<h1>)",
    28:"((.|\n)*)\s*width((.|\n)*)\s*:((.|\n)*)\s*250px;((.|\n)*)\s*background((.|\n)*)\s*:((.|\n)*)\s*blue;((.|\n)*)\s*border-radius((.|\n)*)\s*:((.|\n)*)\s*50%;((.|\n)*)\s*border((.|\n)*)\s*:((.|\n)*)\s*10px((.|\n)*)\s*solid((.|\n)*)\s*white;((.|\n)*)\s*",
    30:"((.|\n)*)\s*width((.|\n)*)\s*:((.|\n)*)\s*250px;((.|\n)*)\s*background((.|\n)*)\s*:((.|\n)*)\s*blue;((.|\n)*)\s*border-radius((.|\n)*)\s*:((.|\n)*)\s*50%;((.|\n)*)\s*border((.|\n)*)\s*:((.|\n)*)\s*10px((.|\n)*)\s*solid((.|\n)*)\s*white;((.|\n)*)\s*display((.|\n)*)\s*:((.|\n)*)\s*inline-block;((.|\n)*)\s*",
   
	}
  var hintsForCheckPonts = {
    11:"Type your code below the inside the <strong>header{ }</strong> selector in style.",
  }
  if(hintsForCheckPonts[currentStep] === undefined ){
      $(".hint-popup").hide();
  }
	if( check_points[currentStep] !== undefined ) {

    // do something
		// console.log('Run validation only on '+currentStep);
		var re = check_points[currentStep];
		var reResults = new RegExp(re, "i").test(editor.getValue());
	 		console.log('Run validation results '+reResults);
       if (reResults) {
        $("#slide"+currentStep+" .check-icon").html('/');
        $("#slide"+currentStep+" .check").html('I did it →').addClass('passed');
        $("#slide"+currentStep+" .actions li").addClass('correct');
             // $(".pagination .next").addClass('disabled');
        $(".pagination .next").removeClass('disabled');
       saveUserData(true, findIndexOfKeys(check_points, currentStep), "P1Training1");
      } else {
        if(!$(".hint-popup-checked").hasClass("checked"+currentStep)){
          $(".hint-popup-checked").addClass("checked"+currentStep);
          if(!$("#slide"+currentStep+" .check").hasClass('passed')){
            if( hintsForCheckPonts[currentStep] !== undefined ){
              htmlTagToString = hintsForCheckPonts[currentStep];
              $(".hint-text").html(htmlTagToString);
              $(".hint-popup").delay(5000).fadeIn().delay(3000).fadeOut(5000);
              //$(".hint-popup").show().delay(10000).fadeOut();
             // $(".hint-popup").show().delay(5000).fadeOut();
              

              // $(".hint-popup").mouseover(function(){
              //   $(this).stop();
              // });
              // $(".hint-popup").mouseout(function(){
              //   $(this).show().delay(10000).fadeOut();
              // });

              $(".close-hint-popup").click(function(){
                  $(".hint-popup").clearQueue();
                  $(".hint-popup").fadeOut();
                  
              });

              $(".hint-popup").click(function(){
                $(".hint-popup").clearQueue();
                $(".hint-popup").fadeOut();
            });

              
            }
          }
        }

        $("#slide"+currentStep+" .check-icon").html('\\');
        $("#slide"+currentStep+" .actions li").removeClass('correct');
        $("#slide"+currentStep+" .check").html('Skip this →').removeClass('passed');;
        $(".pagination .next").addClass('disabled');
      }
  }


	var unlockSkills = {
		12: "h1-h6",
		15: "p",
		20: "email-input",
		24: "submit-input"
	}

	if( unlockSkills[currentStep] !== undefined ) {
				$("#"+unlockSkills[currentStep]+"-skill").addClass('has-skill-true');
				$("#"+unlockSkills[currentStep]+"-skill").removeClass('has-skill-false');
		//console.log('Unlocked skill '+unlockSkills[currentStep]);
	//	console.log("#"+currentStep+"-skill");

	  $("#"+unlockSkills[currentStep]+"-skill").attr('data-original-title', $("#"+unlockSkills[currentStep]+"-skill").attr('data-title-text')).tooltip('show').tooltip('hide');
	 //$("#"+unlockSkills[currentStep]+"-skill").attr('data-original-title',"passwords did not match").tooltip('show').tooltip('hide');
	  //	  console.log("#"+unlockSkills[currentStep]+"-skill");
	  //console.log($("#"+unlockSkills[currentStep]+"-skill").attr('data-title-text'));
	}
    
}

//Function to validate challenges on P02-T004-D
function validateCheckPointP02_T004_D() {
 var check_points = {
     14:"((.|\n)\s*)(h3)((.|\n)*)\s*{((.|\n)*)\s*}((.|\n)*)\s*",
     15:"((.|\n)\s*)(h3)((.|\n)*)\s*{((.|\n)*)\s*(background-color)((.|\n)*)\s*:((.|\n)*)\s*(#ce54c7;)((.|\n)*)\s*}((.|\n)*)\s*",
     17:"((.|\n)\s*)(h3)((.|\n)*)\s*{((.|\n)*)\s*(background-color)((.|\n)*)\s*:((.|\n)*)\s*(#ce54c7;)((.|\n)*)\s*(padding)((.|\n)*)\s*:((.|\n)*)\s*(15px;)((.|\n)*)\s*}((.|\n)*)\s*",
     18:"((.|\n)\s*)(h3)((.|\n)*)\s*{((.|\n)*)\s*(background-color)((.|\n)*)\s*:((.|\n)*)\s*(#ce54c7;)((.|\n)*)\s*(padding)((.|\n)*)\s*:((.|\n)*)\s*(15px;)((.|\n)*)\s*(font-size)((.|\n)*)\s*:((.|\n)*)\s*(1.5em;)((.|\n)*)\s*}((.|\n)*)\s*",
     19:"((.|\n)\s*)(h3)((.|\n)*)\s*{((.|\n)*)\s*(background-color)((.|\n)*)\s*:((.|\n)*)\s*(#ce54c7;)((.|\n)*)\s*(padding)((.|\n)*)\s*:((.|\n)*)\s*(15px;)((.|\n)*)\s*(font-size)((.|\n)*)\s*:((.|\n)*)\s*(1.5em;)((.|\n)*)\s*(border-left)((.|\n)*)\s*:((.|\n)*)\s*(10px)((.|\n)*)\s*(solid)((.|\n)*)\s*(#d6d5cd);((.|\n)*)\s*}((.|\n)*)\s*",
     24:"((.|\n)\s*)(header)((.|\n)*)\s*(\,)((.|\n)*)\s*(h3)((.|\n)*)\s*{((.|\n)*)\s*(color)((.|\n)*)\s*(:)((.|\n)*)\s*(white)((.|\n)*)\s*(;)((.|\n)*)\s*}((.|\n)*)\s*",
     34:"((.|\n)\s*)(body)((.|\n)*)\s*({)((.|\n)*)\s*(font-family)((.|\n)*)\s*(:)((.|\n)*)\s*(helvetica)((.|\n)*)\s*(,)((.|\n)*)\s*(arial)((.|\n)*)\s*(,)((.|\n)*)\s*(san-serif)((.|\n)*)\s*(;)((.|\n)*)\s*}((.|\n)*)\s*",
     35:"((.|\n)\s*)(header)((.|\n)*)\s*(\,)((.|\n)*)\s*(h3)((.|\n)*)\s*{((.|\n)*)\s*(color)((.|\n)*)\s*(:)((.|\n)*)\s*(white)((.|\n)*)\s*(;)((.|\n)*)\s*(font-family)((.|\n)*)\s*(:)((.|\n)*)\s*(\"Arial Black\")((.|\n)*)\s*(,)((.|\n)*)\s*(Gadget)((.|\n)*)\s*(,)((.|\n)*)\s*(san-serif)((.|\n)*)\s*(;)((.|\n)*)\s*}((.|\n)*)\s*",


   
	}
  var hintsForCheckPonts = {
    11:"Type your code below the inside the <strong>header{ }</strong> selector in style.",
  }
  if(hintsForCheckPonts[currentStep] === undefined ){
      $(".hint-popup").hide();
  }
	if( check_points[currentStep] !== undefined ) {

    // do something
		// console.log('Run validation only on '+currentStep);
		var re = check_points[currentStep];
		var reResults = new RegExp(re, "i").test(editor.getValue());
	 		console.log('Run validation results '+reResults);
       if (reResults) {
        $("#slide"+currentStep+" .check-icon").html('/');
        $("#slide"+currentStep+" .check").html('I did it →').addClass('passed');
        $("#slide"+currentStep+" .actions li").addClass('correct');
             // $(".pagination .next").addClass('disabled');
        $(".pagination .next").removeClass('disabled');
       saveUserData(true, findIndexOfKeys(check_points, currentStep), "P1Training1");
      } else {
        if(!$(".hint-popup-checked").hasClass("checked"+currentStep)){
          $(".hint-popup-checked").addClass("checked"+currentStep);
          if(!$("#slide"+currentStep+" .check").hasClass('passed')){
            if( hintsForCheckPonts[currentStep] !== undefined ){
              htmlTagToString = hintsForCheckPonts[currentStep];
              $(".hint-text").html(htmlTagToString);
              $(".hint-popup").delay(5000).fadeIn().delay(3000).fadeOut(5000);
              //$(".hint-popup").show().delay(10000).fadeOut();
             // $(".hint-popup").show().delay(5000).fadeOut();
              

              // $(".hint-popup").mouseover(function(){
              //   $(this).stop();
              // });
              // $(".hint-popup").mouseout(function(){
              //   $(this).show().delay(10000).fadeOut();
              // });

              $(".close-hint-popup").click(function(){
                  $(".hint-popup").clearQueue();
                  $(".hint-popup").fadeOut();
                  
              });

              $(".hint-popup").click(function(){
                $(".hint-popup").clearQueue();
                $(".hint-popup").fadeOut();
            });

              
            }
          }
        }

        $("#slide"+currentStep+" .check-icon").html('\\');
        $("#slide"+currentStep+" .actions li").removeClass('correct');
        $("#slide"+currentStep+" .check").html('Skip this →').removeClass('passed');;
        $(".pagination .next").addClass('disabled');
      }
  }


	var unlockSkills = {
		12: "h1-h6",
		15: "p",
		20: "email-input",
		24: "submit-input"
	}

	if( unlockSkills[currentStep] !== undefined ) {
				$("#"+unlockSkills[currentStep]+"-skill").addClass('has-skill-true');
				$("#"+unlockSkills[currentStep]+"-skill").removeClass('has-skill-false');
		//console.log('Unlocked skill '+unlockSkills[currentStep]);
	//	console.log("#"+currentStep+"-skill");

	  $("#"+unlockSkills[currentStep]+"-skill").attr('data-original-title', $("#"+unlockSkills[currentStep]+"-skill").attr('data-title-text')).tooltip('show').tooltip('hide');
	 //$("#"+unlockSkills[currentStep]+"-skill").attr('data-original-title',"passwords did not match").tooltip('show').tooltip('hide');
	  //	  console.log("#"+unlockSkills[currentStep]+"-skill");
	  //console.log($("#"+unlockSkills[currentStep]+"-skill").attr('data-title-text'));
	}
    
}

//Function to validate challenges on P02-T005-D
function validateCheckPointP02_T005_D() {
	var check_points = {
     10:"((.|\n)\s*)<h3>((.|\n)*)\s*(ABOUT ME)((.|\n)*)\s*<\/h3>((.|\n)*)\s*",
     12:"((.|\n)\s*)<h3>((.|\n)*)\s*(ABOUT ME)((.|\n)*)\s*<\/h3>((.|\n)*)\s*<p>((.|\n)*)\s*<\/p>((.|\n)*)\s*",
     16:"((.|\n)\s*)<h3>((.|\n)*)\s*(EDUCATION)((.|\n)*)\s*<\/h3>((.|\n)*)\s*<p>((.|\n)*)\s*<\/p>((.|\n)*)\s*",
     18:"((.|\n)\s*)<h3>((.|\n)*)\s*(EXPERIENCES)((.|\n)*)\s*<\/h3>((.|\n)*)\s*<p>((.|\n)*)\s*<\/p>((.|\n)*)\s*",
     30:"((.|\n)\s*)<h3>((.|\n)*)\s*(SKILLS)((.|\n)*)\s*<\/h3>((.|\n)*)\s*<ul>((.|\n)*)\s*<li>((.|\n)*)\s*<\/li>((.|\n)*)\s*<\/ul>((.|\n)*)\s*",
     33:"((.|\n)\s*)(p)((.|\n)*)\s*(,)((.|\n)*)\s*(ul)((.|\n)*)\s*({)((.|\n)*)\s*(})((.|\n)*)\s*",
     34:"((.|\n)\s*)(p)((.|\n)*)\s*(,)((.|\n)*)\s*(ul)((.|\n)*)\s*({)((.|\n)*)\s*padding-left:((.|\n)*)\s*50px((.|\n)*)\s*;((.|\n)*)\s*(})((.|\n)*)\s*",
     35:"((.|\n)\s*)(p)((.|\n)*)\s*(,)((.|\n)*)\s*(ul)((.|\n)*)\s*({)((.|\n)*)\s*padding-left:((.|\n)*)\s*50px((.|\n)*)\s*;((.|\n)*)\s*font-size:((.|\n)*)\s*1.3em((.|\n)*)\s*;((.|\n)*)\s*(})((.|\n)*)\s*",
     40:"((.|\n)\s*)<h3>((.|\n)*)\s*(CONTACT)((.|\n)*)\s*<\/h3>((.|\n)*)\s*<p>((.|\n)*)\s*<\/p>((.|\n)*)\s*",
   
	}
  var hintsForCheckPonts = {
    11:"Type your code below the inside the <strong>header{ }</strong> selector in style.",
  }
  if(hintsForCheckPonts[currentStep] === undefined ){
      $(".hint-popup").hide();
  }
	if( check_points[currentStep] !== undefined ) {

    // do something
		// console.log('Run validation only on '+currentStep);
		var re = check_points[currentStep];
		var reResults = new RegExp(re, "i").test(editor.getValue());
	 		console.log('Run validation results '+reResults);
       if (reResults) {
        $("#slide"+currentStep+" .check-icon").html('/');
        $("#slide"+currentStep+" .check").html('I did it →').addClass('passed');
        $("#slide"+currentStep+" .actions li").addClass('correct');
             // $(".pagination .next").addClass('disabled');
        $(".pagination .next").removeClass('disabled');
       saveUserData(true, findIndexOfKeys(check_points, currentStep), "P1Training1");
      } else {
        if(!$(".hint-popup-checked").hasClass("checked"+currentStep)){
          $(".hint-popup-checked").addClass("checked"+currentStep);
          if(!$("#slide"+currentStep+" .check").hasClass('passed')){
            if( hintsForCheckPonts[currentStep] !== undefined ){
              htmlTagToString = hintsForCheckPonts[currentStep];
              $(".hint-text").html(htmlTagToString);
              $(".hint-popup").delay(5000).fadeIn().delay(3000).fadeOut(5000);
              //$(".hint-popup").show().delay(10000).fadeOut();
             // $(".hint-popup").show().delay(5000).fadeOut();
              

              // $(".hint-popup").mouseover(function(){
              //   $(this).stop();
              // });
              // $(".hint-popup").mouseout(function(){
              //   $(this).show().delay(10000).fadeOut();
              // });

              $(".close-hint-popup").click(function(){
                  $(".hint-popup").clearQueue();
                  $(".hint-popup").fadeOut();
                  
              });

              $(".hint-popup").click(function(){
                $(".hint-popup").clearQueue();
                $(".hint-popup").fadeOut();
            });

              
            }
          }
        }

        $("#slide"+currentStep+" .check-icon").html('\\');
        $("#slide"+currentStep+" .actions li").removeClass('correct');
        $("#slide"+currentStep+" .check").html('Skip this →').removeClass('passed');;
        $(".pagination .next").addClass('disabled');
      }
  }


	var unlockSkills = {
		12: "h1-h6",
		15: "p",
		20: "email-input",
		24: "submit-input"
	}

	if( unlockSkills[currentStep] !== undefined ) {
				$("#"+unlockSkills[currentStep]+"-skill").addClass('has-skill-true');
				$("#"+unlockSkills[currentStep]+"-skill").removeClass('has-skill-false');
		//console.log('Unlocked skill '+unlockSkills[currentStep]);
	//	console.log("#"+currentStep+"-skill");

	  $("#"+unlockSkills[currentStep]+"-skill").attr('data-original-title', $("#"+unlockSkills[currentStep]+"-skill").attr('data-title-text')).tooltip('show').tooltip('hide');
	 //$("#"+unlockSkills[currentStep]+"-skill").attr('data-original-title',"passwords did not match").tooltip('show').tooltip('hide');
	  //	  console.log("#"+unlockSkills[currentStep]+"-skill");
	  //console.log($("#"+unlockSkills[currentStep]+"-skill").attr('data-title-text'));
	}
    
}

//Function to validate challenges on P02-T006-D
function validateCheckPointP02_T006_D() {
	var check_points = {
     12:"((.|\n)*)\s*<div((.|\n)*)\s*class=\"((.|\n)*)\s*circle((.|\n)*)\s*\"((.|\n)*)\s*>((.|\n)*)\s*<div((.|\n)*)\s*>((.|\n)*)\s*((.|\n)*)\s*<\/div((.|\n)*)\s*>((.|\n)*)\s*<\/div((.|\n)*)\s*>((.|\n)*)\s*",
     13:"((.|\n)*)\s*<div((.|\n)*)\s*class=\"((.|\n)*)\s*circle((.|\n)*)\s*\"((.|\n)*)\s*>((.|\n)*)\s*<div((.|\n)*)\s*>((.|\n)*)\s*&#x1F60E((.|\n)*)\s*<\/div((.|\n)*)\s*>((.|\n)*)\s*<\/div((.|\n)*)\s*>((.|\n)*)\s*",
     17:"((.|\n)\s*)<style>((.|\n)*)\s*(.emoji)((.|\n)*)\s*{((.|\n)*)\s*(font-size)((.|\n)*)\s*:((.|\n)*)\s*(8em)((.|\n)*)\s*}((.|\n)*)\s*<\/style>((.|\n)*)\s*",
     18:"((.|\n)*)\s*<div((.|\n)*)\s*class=\"((.|\n)*)\s*circle((.|\n)*)\s*\"((.|\n)*)\s*>((.|\n)*)\s*<div((.|\n)*)\s*class=\"((.|\n)*)\s*emoji((.|\n)*)\s*\"((.|\n)*)\s*>((.|\n)*)\s*&#x1F60E((.|\n)*)\s*<\/div((.|\n)*)\s*>((.|\n)*)\s*<\/div((.|\n)*)\s*>((.|\n)*)\s*",
     23:"((.|\n)\s*)<h3>((.|\n)*)\s*(&#x1F60E)((.|\n)*)\s*(DETAILS)((.|\n)*)\s*<\/h3>((.|\n)*)\s*<p>((.|\n)*)\s*<\/p>((.|\n)*)\s*",
     30:"((.|\n)\s*)<style>((.|\n)*)\s*(p)((.|\n)*)\s*(,)((.|\n)*)\s*(ul)((.|\n)*)\s*{((.|\n)*)\s*(list-style)((.|\n)*)\s*:((.|\n)*)\s*(none)((.|\n)*)\s*;((.|\n)*)\s*(padding-left)((.|\n)*)\s*:((.|\n)*)\s*(50px)((.|\n)*)\s*;((.|\n)*)\s*(font-size)((.|\n)*)\s*:((.|\n)*)\s*(1.3em)((.|\n)*)\s*;((.|\n)*)\s*}((.|\n)*)\s*<\/style>((.|\n)*)\s*",
     37:"((.|\n)*)\s*<br>((.|\n)*)\s*(&copy)((.|\n)*)\s*<\/body>((.|\n)*)\s*",
   
	}
  var hintsForCheckPonts = {
    11:"Type your code below the inside the <strong>header{ }</strong> selector in style.",
  }
  if(hintsForCheckPonts[currentStep] === undefined ){
      $(".hint-popup").hide();
  }
	if( check_points[currentStep] !== undefined ) {

    // do something
		// console.log('Run validation only on '+currentStep);
		var re = check_points[currentStep];
		var reResults = new RegExp(re, "i").test(editor.getValue());
	 		console.log('Run validation results '+reResults);
       if (reResults) {
        $("#slide"+currentStep+" .check-icon").html('/');
        $("#slide"+currentStep+" .check").html('I did it →').addClass('passed');
        $("#slide"+currentStep+" .actions li").addClass('correct');
             // $(".pagination .next").addClass('disabled');
        $(".pagination .next").removeClass('disabled');
       saveUserData(true, findIndexOfKeys(check_points, currentStep), "P1Training1");
      } else {
        if(!$(".hint-popup-checked").hasClass("checked"+currentStep)){
          $(".hint-popup-checked").addClass("checked"+currentStep);
          if(!$("#slide"+currentStep+" .check").hasClass('passed')){
            if( hintsForCheckPonts[currentStep] !== undefined ){
              htmlTagToString = hintsForCheckPonts[currentStep];
              $(".hint-text").html(htmlTagToString);
              $(".hint-popup").delay(5000).fadeIn().delay(3000).fadeOut(5000);
              //$(".hint-popup").show().delay(10000).fadeOut();
             // $(".hint-popup").show().delay(5000).fadeOut();
              

              // $(".hint-popup").mouseover(function(){
              //   $(this).stop();
              // });
              // $(".hint-popup").mouseout(function(){
              //   $(this).show().delay(10000).fadeOut();
              // });

              $(".close-hint-popup").click(function(){
                  $(".hint-popup").clearQueue();
                  $(".hint-popup").fadeOut();
                  
              });

              $(".hint-popup").click(function(){
                $(".hint-popup").clearQueue();
                $(".hint-popup").fadeOut();
            });

              
            }
          }
        }

        $("#slide"+currentStep+" .check-icon").html('\\');
        $("#slide"+currentStep+" .actions li").removeClass('correct');
        $("#slide"+currentStep+" .check").html('Skip this →').removeClass('passed');;
        $(".pagination .next").addClass('disabled');
      }
  }


	var unlockSkills = {
		12: "h1-h6",
		15: "p",
		20: "email-input",
		24: "submit-input"
	}

	if( unlockSkills[currentStep] !== undefined ) {
				$("#"+unlockSkills[currentStep]+"-skill").addClass('has-skill-true');
				$("#"+unlockSkills[currentStep]+"-skill").removeClass('has-skill-false');
		//console.log('Unlocked skill '+unlockSkills[currentStep]);
	//	console.log("#"+currentStep+"-skill");

	  $("#"+unlockSkills[currentStep]+"-skill").attr('data-original-title', $("#"+unlockSkills[currentStep]+"-skill").attr('data-title-text')).tooltip('show').tooltip('hide');
	 //$("#"+unlockSkills[currentStep]+"-skill").attr('data-original-title',"passwords did not match").tooltip('show').tooltip('hide');
	  //	  console.log("#"+unlockSkills[currentStep]+"-skill");
	  //console.log($("#"+unlockSkills[currentStep]+"-skill").attr('data-title-text'));
	}
    
}

//Function to validate challenges on P03-T001-D
function validateCheckPointP03_T001_D() {
	var check_points = {
    100:"(<head>|<head [^>]*>)((.|\n)*)\s*<\/head>",

	}

	if( check_points[currentStep] !== undefined ) {
    // do something
		// console.log('Run validation only on '+currentStep);
		var re = check_points[currentStep];
		var reResults = new RegExp(re, "i").test(editor.getValue());
	 		console.log('Run validation results '+reResults);
       if (reResults) {
        $("#slide"+currentStep+" .check-icon").html('/');
        $("#slide"+currentStep+" .check").html('I did it →').addClass('passed');
        $("#slide"+currentStep+" .actions li").addClass('correct');
             // $(".pagination .next").addClass('disabled');
        $(".pagination .next").removeClass('disabled');
            console.log('Run validation using '+check_points[currentStep]);
            saveUserData(true, findIndexOfKeys(check_points, currentStep), "P3Training1");
      } else {
        $("#slide"+currentStep+" .check-icon").html('\\');
        $("#slide"+currentStep+" .actions li").removeClass('correct');
        $("#slide"+currentStep+" .check").html('Skip this →').removeClass('passed');;
        $(".pagination .next").addClass('disabled');
      }
	}

	var unlockSkills = {
		12: "h1-h6",
		15: "p",
		20: "email-input",
		24: "submit-input"
	}

	if( unlockSkills[currentStep] !== undefined ) {
				$("#"+unlockSkills[currentStep]+"-skill").addClass('has-skill-true');
				$("#"+unlockSkills[currentStep]+"-skill").removeClass('has-skill-false');
		//console.log('Unlocked skill '+unlockSkills[currentStep]);
	//	console.log("#"+currentStep+"-skill");

	  $("#"+unlockSkills[currentStep]+"-skill").attr('data-original-title', $("#"+unlockSkills[currentStep]+"-skill").attr('data-title-text')).tooltip('show').tooltip('hide');
	 //$("#"+unlockSkills[currentStep]+"-skill").attr('data-original-title',"passwords did not match").tooltip('show').tooltip('hide');
	  //	  console.log("#"+unlockSkills[currentStep]+"-skill");
	  //console.log($("#"+unlockSkills[currentStep]+"-skill").attr('data-title-text'));
	}
}

//Function to validate challenges on P03-T002-D
function validateCheckPointP03_T002_D() {
	var check_points = {
    100:"(<head>|<head [^>]*>)((.|\n)*)\s*<\/head>",
	}

	if( check_points[currentStep] !== undefined ) {
    // do something
		// console.log('Run validation only on '+currentStep);
		var re = check_points[currentStep];
		var reResults = new RegExp(re, "i").test(editor.getValue());
	 		console.log('Run validation results '+reResults);
       if (reResults) {
        $("#slide"+currentStep+" .check-icon").html('/');
        $("#slide"+currentStep+" .check").html('I did it →').addClass('passed');
        $("#slide"+currentStep+" .actions li").addClass('correct');
             // $(".pagination .next").addClass('disabled');
        $(".pagination .next").removeClass('disabled');
            console.log('Run validation using '+check_points[currentStep]);
            saveUserData(true, findIndexOfKeys(check_points, currentStep), "P3Training2");
      } else {
        $("#slide"+currentStep+" .check-icon").html('\\');
        $("#slide"+currentStep+" .actions li").removeClass('correct');
        $("#slide"+currentStep+" .check").html('Skip this →').removeClass('passed');;
        $(".pagination .next").addClass('disabled');
      }
	}

	var unlockSkills = {
		12: "h1-h6",
		15: "p",
		20: "email-input",
		24: "submit-input"
	}

	if( unlockSkills[currentStep] !== undefined ) {
				$("#"+unlockSkills[currentStep]+"-skill").addClass('has-skill-true');
				$("#"+unlockSkills[currentStep]+"-skill").removeClass('has-skill-false');
		//console.log('Unlocked skill '+unlockSkills[currentStep]);
	//	console.log("#"+currentStep+"-skill");

	  $("#"+unlockSkills[currentStep]+"-skill").attr('data-original-title', $("#"+unlockSkills[currentStep]+"-skill").attr('data-title-text')).tooltip('show').tooltip('hide');
	 //$("#"+unlockSkills[currentStep]+"-skill").attr('data-original-title',"passwords did not match").tooltip('show').tooltip('hide');
	  //	  console.log("#"+unlockSkills[currentStep]+"-skill");
	  //console.log($("#"+unlockSkills[currentStep]+"-skill").attr('data-title-text'));
	}
}

//Function to validate challenges on P03-T003-D
function validateCheckPointP03_T003_D() {
	var check_points = {
    100:"(<head>|<head [^>]*>)((.|\n)*)\s*<\/head>",
	}

	if( check_points[currentStep] !== undefined ) {
    // do something
		// console.log('Run validation only on '+currentStep);
		var re = check_points[currentStep];
		var reResults = new RegExp(re, "i").test(editor.getValue());
	 		console.log('Run validation results '+reResults);
       if (reResults) {
        $("#slide"+currentStep+" .check-icon").html('/');
        $("#slide"+currentStep+" .check").html('I did it →').addClass('passed');
        $("#slide"+currentStep+" .actions li").addClass('correct');
             // $(".pagination .next").addClass('disabled');
        $(".pagination .next").removeClass('disabled');
            console.log('Run validation using '+check_points[currentStep]);
            saveUserData(true, findIndexOfKeys(check_points, currentStep), "P3Training3");
      } else {
        $("#slide"+currentStep+" .check-icon").html('\\');
        $("#slide"+currentStep+" .actions li").removeClass('correct');
        $("#slide"+currentStep+" .check").html('Skip this →').removeClass('passed');;
        $(".pagination .next").addClass('disabled');
      }
	}

	var unlockSkills = {
		12: "h1-h6",
		15: "p",
		20: "email-input",
		24: "submit-input"
	}

	if( unlockSkills[currentStep] !== undefined ) {
				$("#"+unlockSkills[currentStep]+"-skill").addClass('has-skill-true');
				$("#"+unlockSkills[currentStep]+"-skill").removeClass('has-skill-false');
		//console.log('Unlocked skill '+unlockSkills[currentStep]);
	//	console.log("#"+currentStep+"-skill");

	  $("#"+unlockSkills[currentStep]+"-skill").attr('data-original-title', $("#"+unlockSkills[currentStep]+"-skill").attr('data-title-text')).tooltip('show').tooltip('hide');
	 //$("#"+unlockSkills[currentStep]+"-skill").attr('data-original-title',"passwords did not match").tooltip('show').tooltip('hide');
	  //	  console.log("#"+unlockSkills[currentStep]+"-skill");
	  //console.log($("#"+unlockSkills[currentStep]+"-skill").attr('data-title-text'));
	}
}

//Function to validate challenges on P03-T004-D
function validateCheckPointP03_T004_D() {
	var check_points = {
    100:"(<head>|<head [^>]*>)((.|\n)*)\s*<\/head>",
	}

	if( check_points[currentStep] !== undefined ) {
    // do something
		// console.log('Run validation only on '+currentStep);
		var re = check_points[currentStep];
		var reResults = new RegExp(re, "i").test(editor.getValue());
	 		console.log('Run validation results '+reResults);
       if (reResults) {
        $("#slide"+currentStep+" .check-icon").html('/');
        $("#slide"+currentStep+" .check").html('I did it →').addClass('passed');
        $("#slide"+currentStep+" .actions li").addClass('correct');
             // $(".pagination .next").addClass('disabled');
        $(".pagination .next").removeClass('disabled');
            console.log('Run validation using '+check_points[currentStep]);
            saveUserData(true, findIndexOfKeys(check_points, currentStep), "P3Training4");
      } else {
        $("#slide"+currentStep+" .check-icon").html('\\');
        $("#slide"+currentStep+" .actions li").removeClass('correct');
        $("#slide"+currentStep+" .check").html('Skip this →').removeClass('passed');;
        $(".pagination .next").addClass('disabled');
      }
	}

	var unlockSkills = {
		12: "h1-h6",
		15: "p",
		20: "email-input",
		24: "submit-input"
	}

	if( unlockSkills[currentStep] !== undefined ) {
				$("#"+unlockSkills[currentStep]+"-skill").addClass('has-skill-true');
				$("#"+unlockSkills[currentStep]+"-skill").removeClass('has-skill-false');
		//console.log('Unlocked skill '+unlockSkills[currentStep]);
	//	console.log("#"+currentStep+"-skill");

	  $("#"+unlockSkills[currentStep]+"-skill").attr('data-original-title', $("#"+unlockSkills[currentStep]+"-skill").attr('data-title-text')).tooltip('show').tooltip('hide');
	 //$("#"+unlockSkills[currentStep]+"-skill").attr('data-original-title',"passwords did not match").tooltip('show').tooltip('hide');
	  //	  console.log("#"+unlockSkills[currentStep]+"-skill");
	  //console.log($("#"+unlockSkills[currentStep]+"-skill").attr('data-title-text'));
	}
}

//Function to validate challenges on P03-T005-D
function validateCheckPointP03_T005_D() {
	var check_points = {
    100:"(<head>|<head [^>]*>)((.|\n)*)\s*<\/head>",
	}

	if( check_points[currentStep] !== undefined ) {
    // do something
		// console.log('Run validation only on '+currentStep);
		var re = check_points[currentStep];
		var reResults = new RegExp(re, "i").test(editor.getValue());
	 		console.log('Run validation results '+reResults);
       if (reResults) {
        $("#slide"+currentStep+" .check-icon").html('/');
        $("#slide"+currentStep+" .check").html('I did it →').addClass('passed');
        $("#slide"+currentStep+" .actions li").addClass('correct');
             // $(".pagination .next").addClass('disabled');
        $(".pagination .next").removeClass('disabled');
            console.log('Run validation using '+check_points[currentStep]);
            saveUserData(true, findIndexOfKeys(check_points, currentStep), "P3Training5");
      } else {
        $("#slide"+currentStep+" .check-icon").html('\\');
        $("#slide"+currentStep+" .actions li").removeClass('correct');
        $("#slide"+currentStep+" .check").html('Skip this →').removeClass('passed');;
        $(".pagination .next").addClass('disabled');
      }
	}

	var unlockSkills = {
		12: "h1-h6",
		15: "p",
		20: "email-input",
		24: "submit-input"
	}

	if( unlockSkills[currentStep] !== undefined ) {
				$("#"+unlockSkills[currentStep]+"-skill").addClass('has-skill-true');
				$("#"+unlockSkills[currentStep]+"-skill").removeClass('has-skill-false');
		//console.log('Unlocked skill '+unlockSkills[currentStep]);
	//	console.log("#"+currentStep+"-skill");

	  $("#"+unlockSkills[currentStep]+"-skill").attr('data-original-title', $("#"+unlockSkills[currentStep]+"-skill").attr('data-title-text')).tooltip('show').tooltip('hide');
	 //$("#"+unlockSkills[currentStep]+"-skill").attr('data-original-title',"passwords did not match").tooltip('show').tooltip('hide');
	  //	  console.log("#"+unlockSkills[currentStep]+"-skill");
	  //console.log($("#"+unlockSkills[currentStep]+"-skill").attr('data-title-text'));
	}
}

//Function to validate challenges on P02-T006-D
function validateCheckPointP03_T006_D() {
	var check_points = {
    100:"(<head>|<head [^>]*>)((.|\n)*)\s*<\/head>",
	}

	if( check_points[currentStep] !== undefined ) {
    // do something
		// console.log('Run validation only on '+currentStep);
		var re = check_points[currentStep];
		var reResults = new RegExp(re, "i").test(editor.getValue());
	 		console.log('Run validation results '+reResults);
       if (reResults) {
        $("#slide"+currentStep+" .check-icon").html('/');
        $("#slide"+currentStep+" .check").html('I did it →').addClass('passed');
        $("#slide"+currentStep+" .actions li").addClass('correct');
             // $(".pagination .next").addClass('disabled');
        $(".pagination .next").removeClass('disabled');
            console.log('Run validation using '+check_points[currentStep]);
            saveUserData(true, findIndexOfKeys(check_points, currentStep), "P3Training6");
      } else {
        $("#slide"+currentStep+" .check-icon").html('\\');
        $("#slide"+currentStep+" .actions li").removeClass('correct');
        $("#slide"+currentStep+" .check").html('Skip this →').removeClass('passed');;
        $(".pagination .next").addClass('disabled');
      }
	}

	var unlockSkills = {
		12: "h1-h6",
		15: "p",
		20: "email-input",
		24: "submit-input"
	}

	if( unlockSkills[currentStep] !== undefined ) {
				$("#"+unlockSkills[currentStep]+"-skill").addClass('has-skill-true');
				$("#"+unlockSkills[currentStep]+"-skill").removeClass('has-skill-false');
		//console.log('Unlocked skill '+unlockSkills[currentStep]);
	//	console.log("#"+currentStep+"-skill");

	  $("#"+unlockSkills[currentStep]+"-skill").attr('data-original-title', $("#"+unlockSkills[currentStep]+"-skill").attr('data-title-text')).tooltip('show').tooltip('hide');
	 //$("#"+unlockSkills[currentStep]+"-skill").attr('data-original-title',"passwords did not match").tooltip('show').tooltip('hide');
	  //	  console.log("#"+unlockSkills[currentStep]+"-skill");
	  //console.log($("#"+unlockSkills[currentStep]+"-skill").attr('data-title-text'));
	}
}

/*========= End of validators ======*/




//Function to validate challenges on project1
function validateCheckPointProject1() {
	var check_points = {
    46:"(<head>|<head [^>]*>)((.|\n)*)\s*<\/head>",
	////	48: "(<body>|<body [^>]*>)(\n|(\n\n)|(\n\s)|\n\s\n|\n\s)\s*(.*?)\s*<\/body>"
    47: "(<head>|<head [^>]*>)((.|\n)*)\s*<\/head>((.|\n)*)\s*(<body>|<body [^>]*>)((.|\n)*)\s*<\/body>",
    53: "(<head>|<head [^>]*>)((.|\n)*)\s*<\/head>((.|\n)*)\s*(<body>|<body [^>]*>)((.|\n)*)\s*(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>((.|\n)*)\s*<\/body>",
		54: "(<head>|<head [^>]*>)((.|\n)*)\s*<\/head>((.|\n)*)\s*(<body>|<body [^>]*>)((.|\n)*)\s*(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>((.|\n)*)\s*<\/body>",
		62: "(<head>|<head [^>]*>)((.|\n)*)\s*<\/head>((.|\n)*)\s*(<body>|<body [^>]*>)((.|\n)*)\s*(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>((.|\n)*)\s*(<h3>|<h3 [^>]*>)((.|\n)*)\s*<\/h3>((.|\n)*)\s*<\/body>",
		63: "(<head>|<head [^>]*>)((.|\n)*)\s*<\/head>((.|\n)*)\s*(<body>|<body [^>]*>)((.|\n)*)\s*(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>((.|\n)*)\s*(<h3>|<h3 [^>]*>)((.|\n)*)\s*<\/h3>((.|\n)*)\s*<\/body>",
    66:"(<p>|<p [^>]*>)((.|\n)*)\s*<\/p>",
    85:"(<style>|<style [^>]*>)((.|\n)*)\s*<\/style>",
    95:"(<head>|<head [^>]*>)((.|\n)*)\s*(<style>|<style [^>]*>)((.|\n)*)\s*h1((.|\n)*)\s*<\/style>((.|\n)*)\s*<\/head>",
    96:"(<head>|<head [^>]*>)((.|\n)*)\s*(<style>|<style [^>]*>)((.|\n)*)\s*h1((.|\n)*)\s*{((.|\n)*)\s*font-size((.|\n)*)\s*75px;((.|\n)*)\s*}((.|\n)*)\s*<\/style>((.|\n)*)\s*<\/head>",

    100:"(<head>|<head [^>]*>)((.|\n)*)\s*(<style>|<style [^>]*>)((.|\n)*)\s*h1((.|\n)*)\s*{((.|\n)*)\s*font-size((.|\n)*)\s*75px;((.|\n)*)\s*}((.|\n)*)\s*<\/style>((.|\n)*)\s*<\/head>((.|\n)*)\s*"+
    "(<body>|<body [^>]*>)((.|\n)*)\s*(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>((.|\n)*)\s*(Motivation|motivation|MOTIVATION):((.|\n)*)\s*<\/body>",
    101:"(<head>|<head [^>]*>)((.|\n)*)\s*(<style>|<style [^>]*>)((.|\n)*)\s*h1((.|\n)*)\s*{((.|\n)*)\s*font-size((.|\n)*)\s*75px;((.|\n)*)\s*}((.|\n)*)\s*<\/style>((.|\n)*)\s*<\/head>((.|\n)*)\s*"+
      "(<body>|<body [^>]*>)((.|\n)*)\s*(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>((.|\n)*)\s*(<h3>|<h3 [^>]*>)((.|\n)*)\s*<\/h3>((.|\n)*)\s*<\/body>",
    104:"(<head>|<head [^>]*>)((.|\n)*)\s*(<style>|<style [^>]*>)((.|\n)*)\s*h1((.|\n)*)\s*{((.|\n)*)\s*font-size((.|\n)*)\s*75px;((.|\n)*)\s*}((.|\n)*)\s*<\/style>((.|\n)*)\s*<\/head>((.|\n)*)\s*"+
      "(<body>|<body [^>]*>)((.|\n)*)\s*(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>((.|\n)*)\s*(<h3>|<h3 [^>]*>)((.|\n)*)\s*(Motivation|motivation|MOTIVATION):((.|\n)*)\s*<\/h3>((.|\n)*)\s*<\/body>",
    110:"(<head>|<head [^>]*>)((.|\n)*)\s*(<style>|<style [^>]*>)((.|\n)*)\s*h1((.|\n)*)\s*{((.|\n)*)\s*font-size((.|\n)*)\s*75px;((.|\n)*)\s*}((.|\n)*)\s*<\/style>((.|\n)*)\s*<\/head>((.|\n)*)\s*"+
      "(<body>|<body [^>]*>)((.|\n)*)\s*(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>((.|\n)*)\s*(<h3>|<h3 [^>]*>)((.|\n)*)\s*(Motivation|motivation|MOTIVATION):((.|\n)*)\s*(<i>|<i [^>]*>)((.|\n)*)\s*<\/i>((.|\n)*)\s*<\/h3>((.|\n)*)\s*<\/body>",
    112:"(<head>|<head [^>]*>)((.|\n)*)\s*(<style>|<style [^>]*>)((.|\n)*)\s*h1((.|\n)*)\s*{((.|\n)*)\s*font-size((.|\n)*)\s*75px;((.|\n)*)\s*}((.|\n)*)\s*i((.|\n)*)\s*{((.|\n)*)\s*font-size((.|\n)*)\s*25px;((.|\n)*)\s*}((.|\n)*)\s*<\/style>((.|\n)*)\s*<\/head>((.|\n)*)\s*"+
      "(<body>|<body [^>]*>)((.|\n)*)\s*(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>((.|\n)*)\s*(<h3>|<h3 [^>]*>)((.|\n)*)\s*(Motivation|motivation|MOTIVATION):((.|\n)*)\s*(<i>|<i [^>]*>)((.|\n)*)\s*<\/i>((.|\n)*)\s*<\/h3>((.|\n)*)\s*<\/body>",
    117:"(<head>|<head [^>]*>)((.|\n)*)\s*(<style>|<style [^>]*>)((.|\n)*)\s*h1((.|\n)*)\s*{((.|\n)*)\s*font-size((.|\n)*)\s*75px;((.|\n)*)\s*}((.|\n)*)\s*i((.|\n)*)\s*{((.|\n)*)\s*font-size((.|\n)*)\s*25px;((.|\n)*)\s*}((.|\n)*)\s*<\/style>((.|\n)*)\s*<\/head>((.|\n)*)\s*"+
      "(<body>|<body [^>]*>)((.|\n)*)\s*(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>((.|\n)*)\s*(<h3>|<h3 [^>]*>)((.|\n)*)\s*(Motivation|motivation|MOTIVATION):((.|\n)*)\s*(<br>|<br [^>]*>)((.|\n)*)\s*(<br>|<br [^>]*>)((.|\n)*)\s*(<i>|<i [^>]*>)((.|\n)*)\s*<\/i>((.|\n)*)\s*<\/h3>((.|\n)*)\s*<\/body>",
    146:"(<head>|<head [^>]*>)((.|\n)*)\s*(<style>|<style [^>]*>)((.|\n)*)\s*h1((.|\n)*)\s*{((.|\n)*)\s*font-size((.|\n)*)\s*75px;((.|\n)*)\s*}((.|\n)*)\s*i((.|\n)*)\s*{((.|\n)*)\s*font-size((.|\n)*)\s*25px;((.|\n)*)\s*}((.|\n)*)\s*<\/style>((.|\n)*)\s*<\/head>((.|\n)*)\s*"+
      "(<body>|<body [^>]*>)((.|\n)*)\s*(<header>|<header [^>]*>)((.|\n)*)\s*<\/header>((.|\n)*)\s*(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>((.|\n)*)\s*(<h3>|<h3 [^>]*>)((.|\n)*)\s*(Motivation|motivation|MOTIVATION):((.|\n)*)\s*(<br>|<br [^>]*>)((.|\n)*)\s*(<br>|<br [^>]*>)((.|\n)*)\s*(<i>|<i [^>]*>)((.|\n)*)\s*<\/i>((.|\n)*)\s*<\/h3>((.|\n)*)\s*<\/body>",
    152:"(<head>|<head [^>]*>)((.|\n)*)\s*(<style>|<style [^>]*>)((.|\n)*)\s*h1((.|\n)*)\s*{((.|\n)*)\s*font-size((.|\n)*)\s*75px;((.|\n)*)\s*}((.|\n)*)\s*i((.|\n)*)\s*{((.|\n)*)\s*font-size((.|\n)*)\s*25px;((.|\n)*)\s*}((.|\n)*)\s*header((.|\n)*)\s*{((.|\n)*)\s*}((.|\n)*)\s*<\/style>((.|\n)*)\s*<\/head>((.|\n)*)\s*"+
      "(<body>|<body [^>]*>)((.|\n)*)\s*(<header>|<header [^>]*>)((.|\n)*)\s*<\/header>((.|\n)*)\s*(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>((.|\n)*)\s*(<h3>|<h3 [^>]*>)((.|\n)*)\s*(Motivation|motivation|MOTIVATION):((.|\n)*)\s*(<br>|<br [^>]*>)((.|\n)*)\s*(<br>|<br [^>]*>)((.|\n)*)\s*(<i>|<i [^>]*>)((.|\n)*)\s*<\/i>((.|\n)*)\s*<\/h3>((.|\n)*)\s*<\/body>",
    153:"(<head>|<head [^>]*>)((.|\n)*)\s*(<style>|<style [^>]*>)((.|\n)*)\s*h1((.|\n)*)\s*{((.|\n)*)\s*font-size((.|\n)*)\s*75px;((.|\n)*)\s*}((.|\n)*)\s*i((.|\n)*)\s*{((.|\n)*)\s*font-size((.|\n)*)\s*25px;((.|\n)*)\s*}((.|\n)*)\s*header((.|\n)*)\s*{((.|\n)*)\s*background:((.|\n)*)\s*linear-gradient((.|\n)*)\s*}((.|\n)*)\s*<\/style>((.|\n)*)\s*<\/head>((.|\n)*)\s*"+
      "(<body>|<body [^>]*>)((.|\n)*)\s*(<header>|<header [^>]*>)((.|\n)*)\s*<\/header>((.|\n)*)\s*(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>((.|\n)*)\s*(<h3>|<h3 [^>]*>)((.|\n)*)\s*(Motivation|motivation|MOTIVATION):((.|\n)*)\s*(<br>|<br [^>]*>)((.|\n)*)\s*(<br>|<br [^>]*>)((.|\n)*)\s*(<i>|<i [^>]*>)((.|\n)*)\s*<\/i>((.|\n)*)\s*<\/h3>((.|\n)*)\s*<\/body>",
      // 14: "(<p>|<p [^>]*>)\s*(.*?)\s*<\/p>",
		// 19: "(<input( \s*|[^]* )\s*type\s*=\s*\"\s*email\s*\"\s*[^>]*>)",
		// 23: "(<input( \s*|[^]* )\s*type\s*=\s*\"\s*submit\s*\"\s*[^>]*>)",
		// 27: "(?=(<input( \s*|[^]* )\s*type\s*=\s*\"\s*email\s*\"\s*[^>]*>))(?=(<input( \s*|[^]* )\s*placeholder\s*=\s*\"\s*Your\s* \s*email\s*\"\s*[^>]*>))",
		// 219: "<h2 [^>]*>\s*Hi\s*Tembi\s*<\\/h2>",
		// 318: "^\s*Date\s*\(\s*\)\s*$"
	}

	if( check_points[currentStep] !== undefined ) {
    // do something
		// console.log('Run validation only on '+currentStep);
		var re = check_points[currentStep];
		var reResults = new RegExp(re, "i").test(editor.getValue());
	 		console.log('Run validation results '+reResults);
       if (reResults) {
        $("#slide"+currentStep+" .check-icon").html('/');
        $("#slide"+currentStep+" .check").html('I did it →').addClass('passed');
        $("#slide"+currentStep+" .actions li").addClass('correct');
             // $(".pagination .next").addClass('disabled');
        $(".pagination .next").removeClass('disabled');
            console.log('Run validation using '+check_points[currentStep]);
       saveUserData(true);
      } else {
        $("#slide"+currentStep+" .check-icon").html('\\');
        $("#slide"+currentStep+" .actions li").removeClass('correct');
        $("#slide"+currentStep+" .check").html('Skip this →').removeClass('passed');;
        $(".pagination .next").addClass('disabled');
      }
	}

	var unlockSkills = {
		12: "h1-h6",
		15: "p",
		20: "email-input",
		24: "submit-input"
	}

	if( unlockSkills[currentStep] !== undefined ) {
				$("#"+unlockSkills[currentStep]+"-skill").addClass('has-skill-true');
				$("#"+unlockSkills[currentStep]+"-skill").removeClass('has-skill-false');
		//console.log('Unlocked skill '+unlockSkills[currentStep]);
	//	console.log("#"+currentStep+"-skill");

	  $("#"+unlockSkills[currentStep]+"-skill").attr('data-original-title', $("#"+unlockSkills[currentStep]+"-skill").attr('data-title-text')).tooltip('show').tooltip('hide');
	 //$("#"+unlockSkills[currentStep]+"-skill").attr('data-original-title',"passwords did not match").tooltip('show').tooltip('hide');
	  //	  console.log("#"+unlockSkills[currentStep]+"-skill");
	  //console.log($("#"+unlockSkills[currentStep]+"-skill").attr('data-title-text'));
	}
}

//Function to validate challenges on lesson2
function validateCheckPointLesson2() {

	var check_points = {
		12: "(<h5>|<h5 [^>]*>)\s*(.*?)\s*<\/h5>",
		14: "(<p>|<p [^>]*>)\s*(.*?)\s*<\/p>",
		19: "(<input( \s*|[^]* )\s*type\s*=\s*\"\s*email\s*\"\s*[^>]*>)",
		23: "(<input( \s*|[^]* )\s*type\s*=\s*\"\s*submit\s*\"\s*[^>]*>)",
		27: "(?=(<input( \s*|[^]* )\s*type\s*=\s*\"\s*email\s*\"\s*[^>]*>))(?=(<input( \s*|[^]* )\s*placeholder\s*=\s*\"\s*Your\s* \s*email\s*\"\s*[^>]*>))",
		219: "<h2 [^>]*>\s*Hi\s*Tembi\s*<\\/h2>",
		318: "^\s*Date\s*\(\s*\)\s*$"
	}

	if( check_points[currentStep] !== undefined ) {
    // do something
		// console.log('Run validation only on '+currentStep);
		var re = check_points[currentStep];
		var reResults = new RegExp(re, "i").test(editor.getValue());
	 		// console.log('Run validation results '+reResults);
		if (reResults) {
			$("#slide"+currentStep+" .check-icon").html('/');
			$("#slide"+currentStep+" .check").html('I did it →').addClass('passed');
			$("#slide"+currentStep+" .actions li").addClass('correct');
				//		$(".pagination .next").addClass('disabled');
			$(".pagination .next").removeClass('disabled');
					// console.log('Run validation using '+check_points[currentStep]);
     saveUserData(true);
		} else {
			$("#slide"+currentStep+" .check-icon").html('\\');
			$("#slide"+currentStep+" .actions li").removeClass('correct');
			$(".pagination .next").addClass('disabled');
		}
	}

	var unlockSkills = {
		12: "h1-h6",
		15: "p",
		20: "email-input",
		24: "submit-input"
	}

	if( unlockSkills[currentStep] !== undefined ) {
				$("#"+unlockSkills[currentStep]+"-skill").addClass('has-skill-true');
				$("#"+unlockSkills[currentStep]+"-skill").removeClass('has-skill-false');
		//console.log('Unlocked skill '+unlockSkills[currentStep]);
	//	console.log("#"+currentStep+"-skill");

	  $("#"+unlockSkills[currentStep]+"-skill").attr('data-original-title', $("#"+unlockSkills[currentStep]+"-skill").attr('data-title-text')).tooltip('show').tooltip('hide');
	 //$("#"+unlockSkills[currentStep]+"-skill").attr('data-original-title',"passwords did not match").tooltip('show').tooltip('hide');
	  //	  console.log("#"+unlockSkills[currentStep]+"-skill");
	  //console.log($("#"+unlockSkills[currentStep]+"-skill").attr('data-title-text'));
	}
}


function validateCheckPoint() {

	var check_points = {
		12: "(<h1>|<h1 [^>]*>)\s*(.*?)\s*<\/h1>",
		14: "(<p>|<p [^>]*>)\s*(.*?)\s*<\/p>",
		19: "(<input( \s*|[^]* )\s*type\s*=\s*\"\s*email\s*\"\s*[^>]*>)",
		23: "(<input( \s*|[^]* )\s*type\s*=\s*\"\s*submit\s*\"\s*[^>]*>)",
		27: "(?=(<input( \s*|[^]* )\s*type\s*=\s*\"\s*email\s*\"\s*[^>]*>))(?=(<input( \s*|[^]* )\s*placeholder\s*=\s*\"\s*Your\s* \s*email\s*\"\s*[^>]*>))",
		219: "<h2 [^>]*>\s*Hi\s*Tembi\s*<\\/h2>",
		318: "^\s*Date\s*\(\s*\)\s*$"
	}

	if( check_points[currentStep] !== undefined ) {
    // do something
		//console.log('Run validation only on '+currentStep);
		var re = check_points[currentStep];
		var reResults = new RegExp(re, "i").test(editor.getValue());
	 		//console.log('Run validation results '+reResults);
		if (reResults) {
			$("#slide"+currentStep+" .check-icon").html('/');
			$("#slide"+currentStep+" .check").html('I did it →').addClass('passed');
			$("#slide"+currentStep+" .actions li").addClass('correct');
			//			$(".pagination .next").addClass('disabled');
			$(".pagination .next").removeClass('disabled');
		//			console.log('Run validation using '+check_points[currentStep]);
     saveUserData(true);
		} else {
			$("#slide"+currentStep+" .check-icon").html('\\');
			$("#slide"+currentStep+" .actions li").removeClass('correct');
			$(".pagination .next").addClass('disabled');
		}
	}

	var unlockSkills = {
		12: "h1-h6",
		15: "p",
		20: "email-input",
		24: "submit-input"
	}

	if( unlockSkills[currentStep] !== undefined ) {
				$("#"+unlockSkills[currentStep]+"-skill").addClass('has-skill-true');
				$("#"+unlockSkills[currentStep]+"-skill").removeClass('has-skill-false');
		//console.log('Unlocked skill '+unlockSkills[currentStep]);
	//	console.log("#"+currentStep+"-skill");

	  $("#"+unlockSkills[currentStep]+"-skill").attr('data-original-title', $("#"+unlockSkills[currentStep]+"-skill").attr('data-title-text')).tooltip('show').tooltip('hide');
	 //$("#"+unlockSkills[currentStep]+"-skill").attr('data-original-title',"passwords did not match").tooltip('show').tooltip('hide');
	  //	  console.log("#"+unlockSkills[currentStep]+"-skill");
	  //console.log($("#"+unlockSkills[currentStep]+"-skill").attr('data-title-text'));
	}
}

//var activeTab = $(".tab-content .tab-content").find(".active");
//var id = activeTab.attr('id');
//console.log('Run validation only on '+currentStep);


setTimeout(updatePreview, 300);

totalSlides = $(".tab-pane-slide").length;
$("#lessonProgressBar").attr("data-valuemax",totalSlides);

var lessonProgressBar = document.getElementById('lessonProgressBar');
var maxNum = lessonProgressBar.dataset.valuemax;
var currentStep = 1;

// Javascript to enable link to tab
var url = document.location.toString();
if (url.match('#')) {
	if (currentStep = parseInt(url.split('#slide')[1])) {
		$('#tab-slides.nav-tabs a[href="#slide'+currentStep+'"]').tab('show') ;
		//console.log('#tab-slides.nav-tabs a[href="#slide'+currentStep+'"]');
	} else {
		$('#submenu.nav-tabs a[href="#'+url.split('#')[1]+'"]').tab('show') ;
	}

}

// With HTML5 history API, we can easily prevent scrolling!
$('.nav-tabs a').on('shown.bs.tab', function (e) {
    if(history.pushState) {
        history.pushState(null, null, e.target.hash);
    } else {
        window.location.hash = e.target.hash; //Polyfill for old browsers
    }
})



function updateProgressBar(e, step) {
	if (!step) {
    currentStep = parseInt($(e.target).data('step'));
	} else {
		currentStep = step;
	}

	   //console.log("e: "+currentStep);
		if (!parseInt(currentStep)) {
		   currentStep = 1;
	   }
	  // console.log("e2: "+currentStep);


  var percent = (parseInt(currentStep) / maxNum) * 100;


  $('.progress-bar').css({width: percent + '%'});
  $('.lessonProgressText').text("Slide " + currentStep + " of " + maxNum);

  if (currentStep === 1) {
	  $(".pagination .text-right .next").html('Start Slideshow').removeClass('disabled').attr("href", "#slide"+(currentStep+1));
	  $(".pagination .prev").addClass('disabled');
	 // $(".pagination .prev").click(function(e) {
	//	e.preventDefault();
	 // });
  }
  else if (currentStep === parseInt(maxNum)) {
    //$("#slide"+(currentStep)).append("<a class='btn btn-primary' href='GitHub/codejika/learn/nomzamos-website-01.php?lesson=P01-T001-D/nomzamos-website-01.php?lesson=P01-T001-D' style='top:65%;'>Start next training →</a></div>");

	  $(".pagination .next").addClass('disabled');
	  $(".pagination .prev").removeClass('disabled').attr("href", "#slide"+(currentStep-1));
	 // $(".pagination .next").click(function(e) {
	//	e.preventDefault();
	 // });
  }
  else {
    $(".pagination .next").removeClass('disabled').attr("href", "#slide"+(currentStep+1));
    $(".pagination .text-right .next").html('Next >').removeClass('disabled').attr("href", "#slide"+(currentStep+1));
	  $(".pagination .prev").removeClass('disabled').attr("href", "#slide"+(currentStep-1));
  }
  //e.relatedTarget // previous tab

  //validateCheckPoint();
  validatorLessonChallenges();
}

$('a[data-toggle="tab"][role="tab-slides"]').on('shown.bs.tab', updateProgressBar );

function nextTab(elem) {
    var elemFind = $(elem).parent().next().find('a[data-toggle="tab"]')
	if (elemFind) {
		elemFind.click();
	}
}
function prevTab(elem) {
    var $elemFind = $(elem).parent().prev().find('a[data-toggle="tab"]')
	if ($elemFind) {
		$elemFind.click();
	}
}

$('.first').click(function(){

  $('#myWizard a:first').tab('show')

})


function timeNow() {
  return new Date().toISOString();
}

function saveUserData(save_code, checkpoint_id, lesson_id) {
  var now = new Date().toISOString();
  if (save_pending || save_code) {
    //console.log("lesson_progress.progress_completed: " + lesson_progress.progress_completed);
    firebase.database().ref('user_profile/' + auth_id + '/lesson_progress/' + lesson_id + "/user_checkpoint/" + checkpoint_id).update({
      user_code: editor.getValue(),
      //last_checkpoint: checkpoint_id,
      //progress_completed: lesson_progress.progress_completed,
      last_updated: timeNow()
    });
    // console.log("Saved code and checkpoint #" + currentStep + " " + timeNow());
    save_pending = false;
  } else {
    //console.log("Nothing to save: "+timeNow());
  }
}


function initFirebase() {
  // Initialize Firebase
  // var config = {
  //   apiKey: "AIzaSyA2KjWwZOoBVEvuv2n4mn1ey6wSzYphJME",
  //   authDomain: "codejika-2cf17.firebaseapp.com",
  //   databaseURL: "https://codejika-2cf17.firebaseio.com",
  //   projectId: "codejika-2cf17",
  //   storageBucket: "codejika-2cf17.appspot.com",
  //   messagingSenderId: "405485160215"

  // gallery Firebase
  var config = {
    apiKey: 'AIzaSyAw269vPfE3QreRGZDuEisv3wSnfFmFFoY',
    authDomain: 'codejika-staging.firebaseapp.com',
    databaseURL: 'https://cj-staging.firebaseio.com/',
    projectId: 'codejika-staging',
    storageBucket: 'codejika-staging.appspot.com',
    messagingSenderId: '405485160215'
  }

  firebase.initializeApp(config);
}

function progressUpdate() {
  var progressPercentage = checkpoint_count / checkpoint_id;
  firebase.database().ref('user_profile/' + auth_id + '/lesson_progress/' + save_lesson_id).set({
    progressCompleted: progressPercentage
  });
}


function getLessonProgressPromise(default_code) {

  return firebase.database().ref('/user_profile/' + auth_id + '/lesson_progress/' + save_lesson_id).once('value').then(function (snapshot) {
    return snapshot.val();
  }, function (error) {
    console.log(error);
  });
  console.log("getLessonProgressPromise");
}

function onStart() {

   editor.setValue(lesson_progress.user_code);
}

$(document).ready(function () {

  $("#menu_page").hide();
  $("#gallery_section").hide();
  hideButton()
  $('#close_button').click(function () {
    $('.blocker .jquery-modal').css('background', 'none')
  })

  initFirebase();

  var getGallery = getGalleryData('', '')
  var getLessonProgress = getLessonProgressPromise();
  Promise.all([getLessonProgress,getGallery]).then(function (results) {
    lesson_progress = results[0];

    //if no lesson data save default values to DB
    if (lesson_progress === null) {
      console.log("no lesson progress found");
      lesson_progress = {
          "last_checkpoint": "0",
          "user_code": "",
          "progress_completed": "0"
        }
        //saveUserData(true);
      console.log("default lesson_progress inserted");
    }

    //str = JSON.stringify(lesson_progress, null, 4);
    //console.log("default lesson_progress:\n"+str);

    console.log("promise all is true, now running onStart");
    onStart();
  });

	// console.log('echo1');
		updateProgressBar("lessonProgressBar", currentStep);
 //  console.log('echo2');


	//Initialize tooltips
    	$('.nav-tabs > li a[title]').tooltip();

    //Wizard
    $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
        var $target = $(e.target);

        if ($target.parent().hasClass('disabled')) {
            return false;
        }
    });

    $("#lesson_tab .next").click(function (e) {

        var $active = $('#lesson_tab .nav-tabs li>a.active');
        $active.parent().next().removeClass('disabled');
        nextTab($active);

    });
    $("#lesson_tab .prev").click(function (e) {

        var $active = $('#lesson_tab .nav-tabs li>a.active');
        prevTab($active);

    });
    getCurrentSlideNumber();
	    //  Activate the Tooltips
    // $('[data-toggle="tooltip"], [rel="tooltip"]').tooltip({
		// html: true,
		// trigger: 'manual'
    // }).click(showPopover).hover(showPopover, hidePopover).on('shown.bs.tooltip', function(){
    //     $('#'+$(this).attr('id')+'.has-skill-true .skills-example').clone().appendTo('.skill-details-content');
    //     });
});

// ============= unRegistered Model =========
function UnRegisteredModal () {
  // console.log("reg");
  $('.modal ').remove()
  $.createDialog({
    modalName: 'unRegistered-modal',
    popupStyle: 'speech-bubble top-align ' + current_avatar + ' sad',
    actionButton: loginModal,
    htmlContent:
      '<div id="confirm_dialog" style="display: block;">' +
      '\t<h2 id="confirm_title">OOOOPS!</h2>' +
      '<label>This feature is only available for logged in users.</label>\n\n\n' +
      '<h4>Login Now</h4>\n' +
      '\t<div id="confirm_actions">' +
      '\t\t<button id="close_button" class="btn btn-primary light cancel" data-dismiss="modal">Skip</button>' +
      '\t\t<input id="action_button" class="btn btn-primary light action " type="submit" value="Login">' +
      '\t</div>' +
      '<p class="sign-up register-button">Not Registered Yet? <span class="a-link register-button">Sign Up</span></p>' +
      '</div>'
  })
  $('.register-button').click(function () {
    registerModal()
  })
}
// ============== unRegistered Model end =========


function homePage(){
  // console.log('home')
  $("#main_page").show();
  $("#gallery_section").hide();
  $("#menu_page").hide();
  }

  function galleryPage(){
    // console.log('galery')
    $("#main_page").hide();
    $("#menu_page").hide();
    $("#gallery_section").show();
    $("#menu_page").hide();
    // addToGallery();
    }

  function menuPage(){
    $("#gallery_section").hide();
    $("#main_page").hide();
    $("#menu_page").show();
    }
    var loading = document.getElementById('loadingIcon')
    loading.style.display = 'none'

    var current_page = 0
    var page_id_array = ['']
    document.getElementById('pageNumber').innerHTML = current_page + 1
    // document.getElementById('pageNumber2').innerHTML = current_page + 1

    function addToGallery () {
      var userCode, keyString
      var check_if_blank = editor.getValue().replace(/(\r\n\t|\n|\r\t)/gm, '').trim()
      if (check_if_blank == '') {
        alert('write some code')
        return false
      }
      var validateAuthId = firebase.database()

      validateAuthId
        .ref('/users/')
        .once('value')
        .then(function (snapshot) {
          // loading.style.display = "block";
          data = snapshot.val()
          var keys = Object.keys(data)
          var validateName = keys['first_name']
          var findKey = keys.filter(key => key == auth_id)
          keyString = findKey.toString()
          // console.log(keyString, 'userDetails')
          console.log(auth_id, 'auid')
          if (findKey == auth_id) {
            titleModal()
          } else if (findKey !== auth_id || keys == null) {
            UnRegisteredModal()
            // alert("Your code is added to the gallery");
          }
        }) // end of firebase response validAuth2
    }

    var user_HTML
    var current_key1
    var user_code1
    var userCodeHtml2 = ''
    var pagesize = 6
    var current_page_size = 0
    function getGalleryData (start_with_id, filter) {
      firebase_object = firebase
        .database()
        .ref('/gallery/5-min-website')
        .orderByKey()
      if (start_with_id && start_with_id != '') {
        // console.log(start_with_id, 'Here is last fetched key')
        firebase_object.startAt(start_with_id)
      }
      var lastVisible = ''

      firebase_object
        .limitToFirst(pagesize * (current_page + 1))
        .once('value')
        .then(
          function (snapshot) {
            // console.log(pagesize * (current_page + 1), 'hello')
            var data = snapshot.val()
            var keys = Object.keys(data)
            if (keys.length < pagesize * (current_page + 1)) {
              document.getElementById('next2').style.visibility = 'hidden'
            } else {
              document.getElementById('next2').style.visibility = 'visible'
            }

            // console.log(keys, 'Here are the keys ')
            var sorted_array = []
            for (i = 0; i < keys.length; i++) {
              var k = keys[i]
              // dataLikes = data[k].date
              data[k]['key'] = k
              sorted_array.push(data[k])
              // console.log(likeArray,'data&k')
            }
            // console.log(sorted_array, 'before SortArray')
            if (filter && filter != '') {
              sorted_array.sort(function (a, b) {
                if (filter == 'MostLiked') {
                  return b.likes - a.likes
                } else if (filter == 'Recent') {
                  return new Date(b.date) - new Date(a.date)
                }
                return 0
              })
            }
            // console.log(sorted_array, 'After SortArray')
            current_page_size = sorted_array.length
            for (i = 0; i < sorted_array.length; i++) {
              loading.style.display = 'none'
              var key = sorted_array[i]['key']
              $('#bottom-pagination').css('visibility', 'visible')
              if (i < 6 * current_page) {
                continue
              }

              var current_key1 = key
              // var userCodeURL =
              //   "https://www.codejika.com/preview/5-minute-website?" + current_key;
              var name =
                data[current_key1].first_name + ' ' + data[current_key1].last_name
              var likes = data[current_key1].likes
              user_HTML = escape(data[current_key1].user_code)
              var user_code1 = escape(data[current_key1].user_code)
              var title = data[current_key1].title
              // console.log(user_HTML,'user')
              userCodeHtml2 += `
              <div id = "userKey" >
                  <div class="card-blog" id="cardHTML">
                  <div  class ="priview_blog"><a onclick="openthumbnail('${current_key1}', '${user_HTML}', '${user_code1}')">
                    <h3>${title}</h3>
                    <a onclick="openthumbnail('${current_key1}', '${user_HTML}', '${user_code1}')" id="previewIcon">
                     View
                     <span><i class="fas fa-external-link-square-alt"></i></span>
                    </a>
                  </div>
                  <a  class="HTMLscreen" >
                  <iframe src="data:text/html;charset=utf-8, ${user_code1}"  scrolling="no" id="my_iframe_${current_key1}">
                  </iframe style="background:#fff">
                </a>

                  <div class="detail-blog">
                  <h4>${name}</h4>
                  <div class="d-flex view-section">
                    <div class="likes">
                        <button onclick="likes('${current_key1}')"><span><i class="far fa-thumbs-up"></i></button><small id='my_like_${current_key1}'>${likes}</small></span>
                    </div>
                    <div class="views">
                    </div>
                  </div>
                  </div>
                </div>

              </div>`
              // console.log(userCodeHtml2, "Here is the code");
              lastVisible = current_key1
            }
            document.getElementById('galleryContainer').innerHTML = userCodeHtml2
            // document.getElementById('view_html_code').innerText = user_HTML;

            page_id_array.push(lastVisible)
            return snapshot.val()
          },
          function (error) {
            console.log(error, 'galleryError')
          }
        )

      return firebase_object
    }

    // likes function
    function likes (user_id) {
      // console.log("I am inside likes");

      like_count_ui = document.getElementById('my_like_' + user_id)
      var new_likes = parseInt(like_count_ui.innerHTML) + 1
      var ref = firebase.database().ref('/gallery/5-min-website/' + user_id)

      ref.once('value').then(function (snapshot) {
        // console.log(snapshot.val(), "Here os the snapsho");

        if (snapshot.val() && snapshot.val().likes) {
          new_likes = snapshot.val().likes + 1
        }
        like_count_ui.innerHTML = new_likes
        ref.update({
          likes: new_likes
        })
      })
    }

    // filter data
    function FilterData () {
      $('.modal ').remove()

      var selectedOption = document.getElementById('selectOptions').value
      console.log(selectedOption, 'option')
      userCodeHtml2 = ''
      $('#loadingIcon').css('display', 'block')
      $('.pagination-section').css('visibility', 'visible')
      // console.log('iam in projects')
      var previousPage = document.getElementById('galleryLikes')
      previousPage.style.display = 'none'
      var previousPage2 = document.getElementById('galleryContainer')
      previousPage2.innerHTML = getGalleryData('', selectedOption)
    }


    // **************** popup modal **********************

    // ============= thumbnail Model =========
function viewThumbnail (clicked_key, user_html_code, user_code_output) {
  // $('.modal ').remove()
  $.galleryDialog({
    modalName: 'viewThumbnail-modal',
    htmlContent: `<div >
        <div class="popup-header">
          <a id="viewHTML" class="view-code">VIEW</a>
        </div>
        <iframe src="data:text/html;charset=utf-8, ${user_code_output}" style="width:100%; height: 100vh;overflow:hidden; border: none;" scrolling="yes" id="my_iframe_${clicked_key}">

        </iframe>
      </div>
      `
  })
  $('.view-code').click(function () {
    outputHTML1(clicked_key, user_html_code, user_code_output)
  })
}
    function openthumbnail (clicked_key, user_html_code, user_code_output) {
      // console.log(clicked_key, user_html_code, user_code_output, 'Here is my key')
      viewThumbnail(clicked_key, user_html_code, user_code_output)
    }
    function outputHTML1 (clicked_key, user_html_code, user_code_output) {
      user_html_code = unescape(user_html_code)
      $('.modal ').remove()
      $.galleryDialog({
        modalName: 'outputHTML1-modal',
        // popupStyle: "speech-bubble top-align " + current_avatar + " happy",
        // actionButton: resetCurrentProject,
        htmlContent: `

            <div style="height:100vh;overflow:hidden;">
            <div class="popup-header">
              <a class="view-output view1_button" >OUTPUT</a>
            </div>
              <div><xmp id=view_html_code_${clicked_key} class="add-background">${user_html_code}</xmp></div>
            </div>
        `
      })
      $('.view1_button').click(function () {
        // viewThumbnail(clicked_key)
        viewThumbnail(clicked_key, user_html_code, user_code_output)
      })
    }

    // *************** pagination code ***********
    function hideButton () {
      if (current_page == 0) {
        document.getElementById('previous').style.visibility = 'hidden'
      }
    }
    function go_next () {
      // console.log(page_id_array, 'here is the array')
      current_page += 1
      loading.style.display = 'block'
      document.getElementById('galleryContainer').innerHTML = loading
      $('#bottom-pagination').css('visibility', 'hidden')
      $('#previous2').css('visibility', 'hidden')
      userCodeHtml2 = ''
      getGalleryData(page_id_array[current_page], '')
      document.getElementById('pageNumber').innerHTML = 1 + current_page

      if (current_page != 0) {
        document.getElementById('previous').style.visibility = 'visible'
      }
    }

    function go_previous () {
      if (current_page != 0) {
        current_page -= 1
        loading.style.display = 'block'
        document.getElementById('galleryContainer').innerHTML = loading
        $('#bottom-pagination').css('visibility', 'hidden')
        userCodeHtml2 = ''
        getGalleryData(page_id_array[current_page], '')
        document.getElementById('pageNumber').innerHTML = 1 + current_page
      }

      // Hide Previous button
      if (current_page == 0) {
        $('#previous').css('visibility', 'hidden')
      }
    }


    // *************** home page js code *********************

    $('.reset-profile').click(function () {
      resetCurrentProject()
    })
    function resetCurrentProject () {
      resetUserData()
      localStorage.setItem(lessson_url + '_' + auth_id + '_active_slide', 0)
      window.location.reload()
    }

    $('.reset-lesson').click(function () {
      $('.modal ').remove()
      // console.log('Came here to reset the lesson')
      $.createDialog({
        modalName: 'reset-lesson',
        popupStyle: 'speech-bubble ' + current_avatar + ' scared',
        actionButton: resetCurrentProject,
        htmlContent:
          '<div id="confirm_dialog" style="display: block;">' +
          '\t<h2 id="confirm_title">Are you sure you want to reset this lesson?</h2>' +
          '\t<div id="confirm_actions">' +
          '\t\t<button id="close_button" class="btn btn-primary light cancel" datax-dismiss="modal"><a href="#" rel="modal:close">Hmmm, maybe not</a></button>' +
          '\t\t<button id="action_button" class="btn btn-primary light action reset-profile">Yes</button>' +
          '\t</div>' +
          '</div>'
      })
    })

    function resetUserData () {
      firebase
        .database()
        .ref('user_profile/' + auth_id + '/lesson_progress/' + save_lesson_id)
        .remove()
    }

    var i
    var current_year = new Date().getFullYear()
    function pad2 (number) {
      return (number < 10 ? '0' : '') + number
    }

    date_form =
      '<form lpformnum="1" id="email-login"><div ><input class="form-control" id="f_name" placeholder="First name" name="fname" type="text" required><input class="form-control" id="l_name" placeholder="Last name" name="lname" type="text" required=""><input class="form-control" id="n_name" placeholder="Nickname (Username)" name="nickname" type="text" required=""></div><div><label style="    margin: 0;">Date of birth</label></div>\n' +
      '<select name="day" id="B-day" class="custom-select" style="width: 28%">\n' +
      '<option value="" selected></option>\n'
    for (i = 1; i <= 31; i++) {
      date_form += '<option value="' + pad2(i) + '">' + pad2(i) + '</option>\n'
    }
    date_form +=
      '</select>\n' +
      '<select name="month" id="B-month" class="custom-select" style="width: 28%">\n' +
      '<option value="" selected></option>\n' +
      '<option value="01">Jan</option>\n' +
      '<option value="02">Feb</option>\n' +
      '<option value="03">Mar</option>\n' +
      '<option value="04">Apr</option>\n' +
      '<option value="05">May</option>\n' +
      '<option value="06">June</option>\n' +
      '<option value="07">July</option>\n' +
      '<option value="08">Aug</option>\n' +
      '<option value="09">Sept</option>\n' +
      '<option value="10">Oct</option>\n' +
      '<option value="11">Nov</option>\n' +
      '<option value="12">Dec</option>\n' +
      '</select>\n' +
      '<select name="year" id="B-year" class="custom-select" style="width: 38%">\n' +
      '<option value="" selected></option>\n'
    for (i = current_year; i >= 1950; i--) {
      date_form += '<option value="' + i + '">' + i + '</option>\n'
    }
    date_form += '</select>\n'
    date_form += '</div><div id="error"></div></form>\n'

    $('.login-button').click(function () {
      loginModal()
    })

    $('.register-button').click(function () {
      registerModal()
    })

    $('.select_lego').click(function () {
      changeAvatar('lego')
    })
    $('.select_robot').click(function () {
      changeAvatar('robot')
    })
    // function loginModal () {
    //   $('.modal ').remove()
    //   $.createDialog({
    //     modalName: 'login-modal',
    //     popupStyle: 'speech-bubble top-align ' + current_avatar + ' happy',
    //     actionButton: saveLogin,
    //     htmlContent:
    //       '<div id="confirm_dialog" style="display: block;">' +
    //       '\t<h2 id="confirm_title">Please enter your details below to login</h2>' +
    //       date_form +
    //       '\t<div id="confirm_actions">' +
    //       '\t\t<button id="close_button" class="btn btn-primary light cancel" data-dismiss="modal">Maybe later</button>' +
    //       '\t\t<input id="action_button login-button" class="btn btn-primary light action" type="submit" value="Sign In">' +
    //       '\t</div>' +
    //       '<p class="sign-up register-button">Dont have an account? <span class="a-link register-button">Sign Up</span></p>' +
    //       '</div>'
    //   })
    //   $('.register-button').click(function () {
    //     registerModal()
    //   })
    //
    //   // close modal on save
    //   $('.action').click(function () {
    //     var first_name = document.getElementById('f_name').value
    //     var last_name = document.getElementById('l_name').value
    //     var nick_name = document.getElementById('n_name').value
    //     var day = document.getElementById('B-day').value
    //     var month = document.getElementById('B-month').value
    //     var year = document.getElementById('B-year').value
    //     // console.log(first_name, last_name, nick_name, day, month, year)
    //
    //     // store to users storage
    //     var ref = firebase.database().ref('/users')
    //
    //     var updateLoginDetails = {
    //       first_name: first_name,
    //       last_name: last_name,
    //       nick_name: nick_name,
    //       D_O_B: { day, month, year }
    //     }
    //     ref.update({
    //       [auth_id]: updateLoginDetails
    //     })
    //     // users storage end
    //
    //     $('#login-modal').modal('hide')
    //     $('.modal , .modal-backdrop').remove()
    //     $('.blocker').css('display', 'none')
    //     // console.log('closed #login-modal')
    //   })
    // }
    function registerModal () {
      // console.log("reg");
      $('.modal ').remove()
      $.createDialog({
        modalName: 'register-modal',
        popupStyle: 'speech-bubble top-align ' + current_avatar + ' happy',
        actionButton: registerNextModal,
        htmlContent:
          '<div id="confirm_dialog" style="display: block;">' +
          '\t<h2 id="confirm_title">Registration is soooo easy. We just need a few details to get started</h2>' +
          date_form +
          '\t<div id="confirm_actions">' +
          '\t\t<button id="close_button" class="btn btn-primary light cancel" >Maybe later</button>' +
          '\t\t<input id="action_button" class="btn btn-primary light action" type="submit" value="Sign up">' +
          '\t</div>' +
          '<p class="sign-up login-button">Already have an account ? <span class="a-link">Sign In</span></p>' +
          '</div>'
      })
      $('.login-button').click(function () {
        loginModal()
      })
    }

    function registerNextModal () {
      // console.log(
      //   'add code here to save user and then proceed to collect addtional data'
      // )
      var first_name = document.getElementById('f_name').value
      var last_name = document.getElementById('l_name').value
      var nick_name = document.getElementById('n_name').value
      // console.log(first_name, last_name, nick_name)

      if (first_name == '' || last_name == '') {
        document.querySelector('#f_name', '').classList.add('error-border')
      }
      if (nick_name == '') {
        document.querySelector('#n_name', '').classList.add('error-border')
      } else {
        var register_addtional =
          '<div id="confirm_dialog" style="display: block;">' +
          '\t<h2 id="confirm_title">Congrats you have successfullly registered.</h2>' +
          '<p style="margin-bottom: 8px;">Please could you add in few more details about yourself so we can stay in touch.</p>' +
          '<form lpformnum="1" id="email-login"><div >\n' +
          '<input class="form-control" placeholder="Mobile number" name="mobile" type="text" required="" style="width: 100%">\n' +
          '<input class="form-control" placeholder="Email address" name="email" type="text" required="" style="width: 100%">\n' +
          '<input class="form-control" placeholder="ID number" name="id_number" type="text" required="" style="width: 100%">\n' +
          '<select name="country" class="custom-select" style="width: 44%">\n' +
          '<option value="" selected>Country</option>\n' +
          '<option value="ZA">South Africa</option>\n' +
          '<option value="MZ">Mozambique</option>\n' +
          '<option value="NM">Nambia</option>\n' +
          '<option value="ZM">Zambia</option>\n' +
          '<option value="ZI">Zimbabwe</option>\n' +
          '</select>\n' +
          '<input class="form-control school" placeholder="Name of School" name="school" type="text" required="" style="width: 54%">\n' +
          '</div><div id="error"></div></form>\n' +
          '\t<div id="confirm_actions">' +
          '\t\t<button id="close_button" class="btn btn-primary light cancel" datax-dismiss="modal">Skip</button>' +
          '\t\t<input id="action_button" class="btn btn-primary light action" type="submit" value="Save">' +
          '\t</div>' +
          '</div>'
      }

      $('#register-modal .modal-body').html(register_addtional)

      $('#close_button, .modal-backdrop').click(function () {
        $('#register-modal').modal('hide')
        $('.modal , .modal-backdrop').remove()
        $('.blocker').css('display', 'none')
        // console.log('closed #register-modal')
      })
      $('previewIcon').click(function () {
        $('.modal , .modal-backdrop').remove()
      })

      $('.action').click(function () {
        $('#login-modal').modal('hide')
        $('.modal , .modal-backdrop').remove()
        $('.blocker').css('display', 'none')
        // console.log('closed #registerNext-modal')
      })
    }

    function changeAvatar (avatar) {
      $('.modal-content')
        .removeClass(current_avatar)
        .addClass(avatar)
      $('.select_robot, .select_lego').toggleClass(
        'option_selected option_unselected'
      )
      current_avatar = avatar
      successAnimation()
    }

    // ***************** title popup in home page ************
    function titleModal () {
      // console.log("reg");
      $('.modal ').remove()
      $.createDialog({
        modalName: 'title-modal',
        popupStyle: 'speech-bubble top-align ' + current_avatar + ' happy',
        actionButton: titleInput,
        htmlContent:
          '<div id="confirm_dialog" style="display: block;">' +
          '\t<h2 id="confirm_title">Awesome</h2>' +
          '<form titleForm="1" id="title-form"><div ><label>What do you want to call your project?<span class="required-mark">*</span></label><input class="form-control" id="project_name" placeholder="Super Code Stretch-man  v1" name="Pname" type="text" required=""><div><label style="    margin: 0;">Do you have any notes? (Optional)</label></div><textarea class="form-control" id="project_notes" placeholder="This project  reminds me of the pet spider I had in 2ndgrade, that.." name="nickname" type="text" cols="4"></textarea></div></form>\n' +
          '\t<div id="confirm_actions">' +
          '\t\t<button id="close_button" class="btn btn-primary light cancel" data-dismiss="modal">Skip</button>' +
          '\t\t<input id="action_button" class="btn btn-primary light action projectTitle" type="submit" value="Save">' +
          '\t</div>' +
          '<p class="sign-up register-button">Not Registered Yet? <span class="a-link register-button">Sign Up</span></p>' +
          '</div>'
      })
      $('.register-button').click(function () {
        registerModal()
      })
      // $('#action_button').click(function () {
      //   savedToGallery()
      // })
    }
    // ************** title popup end ==============

    // ============= unRegistered Model =========
    function UnRegisteredModal () {
      // console.log("reg");
      $('.modal ').remove()
      $.createDialog({
        modalName: 'unRegistered-modal',
        popupStyle: 'speech-bubble top-align ' + current_avatar + ' sad',
        actionButton: loginModal,
        htmlContent:
          '<div id="confirm_dialog" style="display: block;">' +
          '\t<h2 id="confirm_title">OOOOPS!</h2>' +
          '<label>This feature is only available for logged in users.</label>\n\n\n' +
          '<h4>Login Now</h4>\n' +
          '\t<div id="confirm_actions">' +
          '\t\t<button id="close_button" class="btn btn-primary light cancel" data-dismiss="modal">Skip</button>' +
          '\t\t<input id="action_button" class="btn btn-primary light action " type="submit" value="Login">' +
          '\t</div>' +
          '<p class="sign-up register-button">Not Registered Yet? <span class="a-link register-button">Sign Up</span></p>' +
          '</div>'
      })
      $('.register-button').click(function () {
        registerModal()
      })
    }
    // ============== unRegistered Model end =========

    // ============= saved your code Model =========
    function savedToGallery () {
      $('.modal ').remove()
      $.createDialog({
        modalName: 'projectSaved-modal',
        popupStyle: 'speech-bubble top-align ' + current_avatar + ' happy',
        // actionButton: loginModal,
        htmlContent:
          '<div id="confirm_dialog" style="display: block;">' +
          '\t<h2 id="confirm_title">Yeahhhh!</h2>' +
          '<label>Your Project is Saved...</label>\n\n\n' +
          '\t<div id="confirm_actions">' +
          '\t\t<button id="close_button" class="btn btn-primary light action " data-dismiss="modal">Ok</button>' +
          '\t</div>' +
          '</div>'
      })
    }
    // ***********  save user login details to local storage
function saveLogin () {
  // console.log('logindetails//////')
  var first_name = document.getElementById('f_name').value
  var last_name = document.getElementById('l_name').value
  var nick_name = document.getElementById('n_name').value
  var day = document.getElementById('B-day').value
  var month = document.getElementById('B-month').value
  var year = document.getElementById('B-year').value
  // console.log(first_name, last_name, nick_name, day, month, year)
}

function successProfileDetails(title, message) {
  $('.modal ').remove()
  $.createDialog({
    modalName: 'successProfileDetails',
    popupStyle: 'speech-bubble top-align ' + current_avatar + ' happy',
    // actionButton: loginModal,
    htmlContent:
      '<div id="confirm_dialog" style="display: block;">' +
      '\t<h2 id="confirm_title">'+title+'</h2>' +
      '<label>'+message+'</label>\n\n\n' +
      '\t<div id="confirm_actions">' +
      '\t\t<button id="close_button" class="btn btn-encouraging light action " data-dismiss="modal">Ok</button>' +
      '\t</div>' +
      '</div>'
  })
}

function checkUserKey(){
  let userName = document.querySelector('.profile_name')
  var validateAuthId = firebase.database()
  validateAuthId
    .ref('/users/')
    .once('value')
    .then(function (snapshot) {
      data = snapshot.val()
      var keys = Object.keys(data)
      var findKey = keys.filter(key => key == auth_id)
      keyString = findKey.toString()
       if (findKey != auth_id) {
        // registerModal()
      } else{
        validateAuthId.ref('/users/' + auth_id).once('value').then((snapshot)=>{res = snapshot.val()
          // var uKeys = Object.keys(res);
          var uFname = res.first_name;
          var uLname = res.last_name
          userName.innerText ='Hello, '+uFname;

        })
      }
    }
  )
}


/* submit offline code to the firebase
======================================*/
function validateOfllineCode(){
  var project_name = $(".project-name :selected").val();
  var project_code = $("textarea#offline-code").val();

  if(project_name === ""){
    $("select.project-name").css('border', 'solid 1px red');
    alert("Please select the project");
  }
  else if(project_code === ""){
    $("textarea#offline-code").css('border', 'solid 1px red');
    alert("Please paste your code in textarea");
  }
  else{
    selectproject_name();
  }
}

function selectproject_name() {
  var project_name = $(".project-name :selected").val();

  if (project_name === "project1") {
    validateProject1();
  }
  else if (project_name === "project2") {
   validateProject2();
  }
  else if (project_name === "project3") {
   validateProject3();
  }
  else {
    validateProject4();
  }
}
function validateProject2() {
  alert("Project 2 is not yet ready");
}

function validateProject3() {
  alert("Project 3 is not yet ready");
}
function validateProject4() {
  alert("Project 4 is not yet ready");
}


/*Declare html tags as checkpoint, validate and save to DB
==========================================================*/
function validateProject1() {
  /*Check points for P1-training 1
  ================================*/
  var check_points_for_training1 = [
    '(<head>|<head [^>]*>)((.|\n)*)\s*<\/head>',
    '(<body>|<body [^>]*>)((.|\n)*)\s*<\/body>',
    '(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>',
    '(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>',
    '(<h3>|<h3 [^>]*>)((.|\n)*)\s*Launching Soon...((.|\n)*)\s*<\/h3>',
    '(<p>|<p [^>]*>)((.|\n)*)\s*<\/p>'
  ];
  var order_of_HTML_tags_training1 = [
    '(<head>|<head [^>]*>)((.|\n)*)\s*<\/head>',
    '(<body>|<body [^>]*>)((.|\n)*)\s*<\/body>',
    '(<body>|<body [^>]*>)((.|\n)*)\s*(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>((.|\n)*)\s*<\/body>',
    '(<body>|<body [^>]*>)((.|\n)*)\s*(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>((.|\n)*)\s*<\/body>',
    '(<header>|<header [^>]*>)((.|\n)*)\s*(<h3>|<h3 [^>]*>)((.|\n)*)\s*<\/h3>((.|\n)*)\s*<\/header>',
    '(<header>|<header [^>]*>)((.|\n)*)\s*(<p>|<p [^>]*>)((.|\n)*)\s*(2019|2020)((.|\n)*)\s*<\/p>((.|\n)*)\s*<\/header>'
  ];
   /*End Checkpoints for P1 training 1
  ==================================== */

  /*Check points for P1-training 2
  ================================*/
  var check_points_for_training2 = [
    '(<style>|<style [^>]*>)((.|\n)*)\s*<\/style>',
    'h1((.|\n)*)\s*{((.|\n)*)\s*}',
    'h1((.|\n)*)\s*{((.|\n)*)\s*font-size((.|\n)*)\s*:((.|\n)*)\s*75px;((.|\n)*)\s*}',
    '(Motivation|motivation|MOTIVATION)',
    '(<h3>|<h3 [^>]*>)((.|\n)*)\s*<\/h3>',
    '(<h3>|<h3 [^>]*>)((.|\n)*)\s*(Motivation|motivation|MOTIVATION)((.|\n)*)\s*<\/h3>',
    '(<i>|<i [^>]*>)((.|\n)*)\s*<\/i>',    	
    'i((.|\n)*)\s*{((.|\n)*)\s*font-size((.|\n)*)\s*25px;((.|\n)*)\s*}'
  ];
  var order_of_HTML_tags_training2 = [
    '(<head>|<head [^>]*>)((.|\n)*)\s*(<style>|<style [^>]*>)((.|\n)*)\s*<\/style>((.|\n)*)\s*<\/head>',
    '(<style>|<style [^>]*>)((.|\n)*)\s*h1((.|\n)*)\s*{((.|\n)*)\s*}((.|\n)*)\s*<\/style>',
    '(<style>|<style [^>]*>)((.|\n)*)\s*h1((.|\n)*)\s*{((.|\n)*)\s*font-size((.|\n)*)\s*75px;((.|\n)*)\s*}((.|\n)*)\s*<\/style>',
    '(<body>|<body [^>]*>)((.|\n)*)\s*(Motivation|motivation|MOTIVATION)((.|\n)*)\s*<\/body>',
    '(<body>|<body [^>]*>)((.|\n)*)\s*(<h3>|<h3 [^>]*>)((.|\n)*)\s*(Motivation|motivation|MOTIVATION)((.|\n)*)\s*<\/h3>((.|\n)*)\s*<\/body>',
    '(<h3>|<h3 [^>]*>)((.|\n)*)\s*(Motivation|motivation|MOTIVATION)((.|\n)*)\s*<\/h3>',
    '(<h3>|<h3 [^>]*>)((.|\n)*)\s*(<i>|<i [^>]*>)((.|\n)*)\s*<\/i>((.|\n)*)\s*<\/h3>',    	
    '(<style>|<style [^>]*>)((.|\n)*)\s*i((.|\n)*)\s*{((.|\n)*)\s*font-size((.|\n)*)\s*25px;((.|\n)*)\s*}((.|\n)*)\s*<\/style>'
  ];
   /*End Checkpoints for P1 training 2
  =================================== */

  /*Check points for P1-training 3
  ================================*/
  var check_points_for_training3 = [
    '(<header>|<header [^>]*>)((.|\n)*)\s*<\/header>',
    'header((.|\n)*)\s*{((.|\n)*)\s*}',
    'header((.|\n)*)\s*{((.|\n)*)\s*background((.|\n)*)\s*:((.|\n)*)\s*linear-gradient((.|\n)*)\s*}',
    'header((.|\n)*)\s*{((.|\n)*)\s*background((.|\n)*)\s*:((.|\n)*)\s*linear-gradient[(]110deg((.|\n)*)\s*,((.|\n)*)\s*%((.|\n)*)\s*%((.|\n)*)\s*[)]((.|\n)*)\s*}',   
    '(<section>|<section [^>]*>)((.|\n)*)\s*<\/section>',
    '(<section>|<section [^>]*>)((.|\n)*)\s*(<h3>|<h3 [^>]*>)((.|\n)*)\s*(Motivation|motivation|MOTIVATION):((.|\n)*)\s*<\/h3>((.|\n)*)\s*<\/section>',
    'section((.|\n)*)\s*{((.|\n)*)\s*(background|background-color)((.|\n)*)\s*:((.|\n)*)\s*lightgrey((.|\n)*)\s*}',
    '(<footer>|<footer [^>]*>)((.|\n)*)\s*<\/footer>',
    '(<footer>|<footer [^>]*>)((.|\n)*)\s*&copy((.|\n)*)\s*<\/footer>',
    'footer((.|\n)*)\s*{((.|\n)*)\s*background((.|\n)*)\s*:((.|\n)*)\s*}'
  ];
  // 'header((.|\n)*)\s*{((.|\n)*)\s*background((.|\n)*)\s*:((.|\n)*)\s*linear-gradient[(]110geg,((.|\n)*)\s*yellow((.|\n)*)\s*40%,((.|\n)*)\s*pink((.|\n)*)\s*40%\s*[)]((.|\n)*)\s*}',  
  var order_of_HTML_tags_training3 = [
    '(<header>|<header [^>]*>)((.|\n)*)\s*(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>((.|\n)*)\s*<\/header>',
    '(<style>|<style [^>]*>)((.|\n)*)\s*header((.|\n)*)\s*{((.|\n)*)\s*}((.|\n)*)\s*<\/style>',
    '(<style>|<style [^>]*>)((.|\n)*)\s*header((.|\n)*)\s*{((.|\n)*)\s*}((.|\n)*)\s*<\/style>',
    '(<style>|<style [^>]*>)((.|\n)*)\s*header((.|\n)*)\s*{((.|\n)*)\s*}((.|\n)*)\s*<\/style>',
    '(<section>|<section [^>]*>)((.|\n)*)\s*(<h3>|<h3 [^>]*>)((.|\n)*)((.|\n)*)\s*<\/h3>((.|\n)*)\s*<\/section>',
    '(<section>|<section [^>]*>)((.|\n)*)\s*(<h3>|<h3 [^>]*>)((.|\n)*)\s*<\/h3>((.|\n)*)\s*<\/section>',
    '(<style>|<style [^>]*>)((.|\n)*)\s*section((.|\n)*)\s*{((.|\n)*)\s*}((.|\n)*)\s*<\/style>',
    '(<body>|<body [^>]*>)((.|\n)*)\s*(<footer>|<footer [^>]*>)((.|\n)*)\s*<\/footer>((.|\n)*)\s*<\/body>',
    '(<footer>|<footer [^>]*>)((.|\n)*)\s*&copy((.|\n)*)\s*<\/footer>',
    '(<style>|<style [^>]*>)((.|\n)*)\s*footer((.|\n)*)\s*}((.|\n)*)\s*<\/style>'
  ];
  /*End Checkpoints for P1 training 3
  =================================== */

/*Check points for P1-training 4
  ================================*/
  var check_points_for_training4 = [
    '(<div>|<div [^>]*>)((.|\n)*)\s*(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>((.|\n)*)\s*<\/div>',
    '(<div>|<div [^>]*>)((.|\n)*)\s*(<h3>|<h3 [^>]*>)((.|\n)*)\s*<\/h3>((.|\n)*)\s*<\/div>',
    '(<footer>|<footer [^>]*>)((.|\n)*)\s*(<div>|<div [^>]*>)((.|\n)*)\s*<\/div>((.|\n)*)\s*<\/footer>',
    'div((.|\n)*)\s*{((.|\n)*)\s*}',
    'div((.|\n)*)\s*{((.|\n)*)\s*text-align((.|\n)*)\s*:((.|\n)*)\s*center((.|\n)*)\s*}',
    'div((.|\n)*)\s*{((.|\n)*)\s*padding((.|\n)*)\s*:((.|\n)*)\s*40px((.|\n)*)\s*}',
    'h3((.|\n)*)\s*{((.|\n)*)\s*}',
    'h3((.|\n)*)\s*{((.|\n)*)\s*border((.|\n)*)\s*:((.|\n)*)\s*((white((.|\n)*)\s*solid((.|\n)*)\s*2px)|(solid((.|\n)*)\s*white((.|\n)*)\s*2px)|(solid((.|\n)*)\s*2px((.|\n)*)\s*white)|(2px((.|\n)*)\s*solid((.|\n)*)\s*white)|(2px((.|\n)*)\s*white((.|\n)*)\s*solid))((.|\n)*)\s*}',
    'h3((.|\n)*)\s*{((.|\n)*)\s*font-size((.|\n)*)\s*:((.|\n)*)\s*45px((.|\n)*)\s*padding((.|\n)*)\s*:((.|\n)*)\s*15px((.|\n)*)\s*}',
    'h3((.|\n)*)\s*{((.|\n)*)\s*margin((.|\n)*)\s*:((.|\n)*)\s*auto((.|\n)*)\s*}',
    'h3((.|\n)*)\s*{((.|\n)*)\s*max-width((.|\n)*)\s*:((.|\n)*)\s*400px((.|\n)*)\s*}',
    'h1((.|\n)*)\s*{((.|\n)*)\s*color((.|\n)*)\s*:((.|\n)*)\s*(white|#FFF|#fff)((.|\n)*)\s*}'
  ];
  var order_of_HTML_tags_training4 = [
    '(<header>|<header [^>]*>)((.|\n)*)\s*(<div>|<div [^>]*>)((.|\n)*)\s*<\/div>((.|\n)*)\s*<\/header>',
    '(<section>|<section [^>]*>)((.|\n)*)\s*(<div>|<div [^>]*>)((.|\n)*)\s*<\/div>((.|\n)*)\s*<\/section>',
    '(<footer>|<footer [^>]*>)((.|\n)*)\s*(<div>|<div [^>]*>)((.|\n)*)\s*<\/div>((.|\n)*)\s*<\/footer>',
    '(<style>|<style [^>]*>)((.|\n)*)\s*div((.|\n)*)\s*{((.|\n)*)\s*}((.|\n)*)\s*<\/style>',
    '(<style>|<style [^>]*>)((.|\n)*)\s*div((.|\n)*)\s*{((.|\n)*)\s*}((.|\n)*)\s*<\/style>',
    '(<style>|<style [^>]*>)((.|\n)*)\s*div((.|\n)*)\s*{((.|\n)*)\s*}((.|\n)*)\s*<\/style>',
    '(<style>|<style [^>]*>)((.|\n)*)\s*h3((.|\n)*)\s*{((.|\n)*)\s*}((.|\n)*)\s*<\/style>',
    '(<style>|<style [^>]*>)((.|\n)*)\s*h3((.|\n)*)\s*{((.|\n)*)\s*}((.|\n)*)\s*<\/style>',
    '(<style>|<style [^>]*>)((.|\n)*)\s*h3((.|\n)*)\s*{((.|\n)*)\s*}((.|\n)*)\s*<\/style>',
    '(<style>|<style [^>]*>)((.|\n)*)\s*h3((.|\n)*)\s*{((.|\n)*)\s*}((.|\n)*)\s*<\/style>',
    '(<style>|<style [^>]*>)((.|\n)*)\s*h3((.|\n)*)\s*{((.|\n)*)\s*}((.|\n)*)\s*<\/style>',
    '(<style>|<style [^>]*>)((.|\n)*)\s*h1((.|\n)*)\s*{((.|\n)*)\s*}((.|\n)*)\s*<\/style>'
  ];
   /*End Checkpoints for P1 training 4
  =================================== */
  
  var marks_for_training1 = validateHtmlElement( check_points_for_training1, order_of_HTML_tags_training1 );
  var marks_for_training2 = validateHtmlElement( check_points_for_training2, order_of_HTML_tags_training2 );
  var marks_for_training3 = validateHtmlElement( check_points_for_training3, order_of_HTML_tags_training3 );
  var marks_for_training4 = validateHtmlElement( check_points_for_training4, order_of_HTML_tags_training4 );

  marks_for_training1.project_name = "P1 Training 1";
  marks_for_training2.project_name = "P1 Training 2";
  marks_for_training3.project_name = "P1 Training 3";
  marks_for_training4.project_name = "P1 Training 4";

  var overall_marks = {
    p1_training1:  marks_for_training1,
    p1_training2:  marks_for_training2,
    p1_training3:  marks_for_training3,
    p1_training4:  marks_for_training4 
  }

  /*Save overall marks to DB
  ==========================*/
  saveProject(overall_marks);
}

/*Take array of html tags as parameter and validate input code
==============================================================*/
function validateHtmlElement(check_points, wrapping_tags) {
  if ( ! $.isArray(check_points) && ! $.isArray(wrapping_tags) ) {
     throw "validateHtmlElement() needs a array parameters";
  }
  var total_marks = 0;
  var input_code = $("#offline-code").val();
  var validation_pass = false;
  var invalid_tags = [];

  /*get total marks
  =================*/
  for (var i = 0; i < check_points.length; i++) {
    var challenge_number = i + 1;
    var results_for_check_point = new RegExp(check_points[i], "i").test(input_code);
    if ( results_for_check_point ) {
      var results_for_wrapping_tag = new RegExp(wrapping_tags[i], "i").test(input_code);
      if( results_for_wrapping_tag ){
        total_marks++;
      }
      else{
        var openning_tag = check_points[i].split('|')[0];
        var is_css_selector = openning_tag.includes("((.");
        
        
        if( is_css_selector ){
          console.log("=> "+openning_tag);

          var css_selector =  openning_tag.replace('((.', '');
          
          var wrapping_tag = wrapping_tags[i].split('|')[0];
          wrapping_tag = wrapping_tag.replace('(','');
          invalid_tags.push("C" + challenge_number + ". "+css_selector+" css selector must be inside "+ wrapping_tag +" tag.<br>");
        }
        else{
          if( openning_tag.includes('<') && openning_tag.includes('>')){
            openning_tag = openning_tag.replace('(', '');
            openning_tag = openning_tag.replace('<','&lt;');
            openning_tag = openning_tag.replace('>','&gt;');

            var results = new RegExp(openning_tag).test(input_code);
            if (results) {
              invalid_tags.push("C" + challenge_number + ". There seems to be no " + openning_tag + " closing tag.<br>");
            }
            else {
              invalid_tags.push("C" + challenge_number + ". There seems to be an error with " + openning_tag + " tag or is missing.<br>");
            }
          }
          else{
            // total_marks++;
            invalid_tags.push("C" + challenge_number + ". did not include: &ldquo;"+ openning_tag +"&rquo;.<br>");
          }
        }
      }
    }
  }
 

  /*get tags that have errors or are invalid
  ==========================================*/
  for (var i = 0; i < check_points.length; i++) {
    var challenge_number = i + 1;
    var reResults = new RegExp(check_points[i], "i").test(input_code);
    if (reResults) {
      if ((i + 1) == check_points.length) {
        invalid_tags.push("This project correct.<br>");
      }
    }
    else {
      var openning_tag = check_points[i].split('|')[0];
      var is_css_selector = openning_tag.includes("((.");
      if(is_css_selector){
        var css_selector =  openning_tag.replace('((.', '');
        invalid_tags.push("C" + challenge_number + ". "+css_selector+" ..... css selector is incorrect or it has errors.<br>");
      }
      else{
        if( openning_tag.includes('<') && openning_tag.includes('>')){
          openning_tag = openning_tag.replace('(', '');

          var results = new RegExp(openning_tag).test(input_code);
          if (results) {
            openning_tag = openning_tag.replace('<','&lt;');
            openning_tag = openning_tag.replace('>','&gt;');
            invalid_tags.push("C" + challenge_number + ". .....There seems to be no " + openning_tag + " closing tag.<br>");
          }
          else {
            openning_tag = openning_tag.replace('<','&lt;');
            openning_tag = openning_tag.replace('>','&gt;');
            invalid_tags.push("C" + challenge_number + "..... There seems to be an error with " + openning_tag + " tag or is missing.<br>");
          }
        }
        else{
          total_marks++;
        }
      }
    }
  }

  var total_marks_in_percent = (total_marks / check_points.length) * 100;
  total_marks_in_percent  =  Math.round( total_marks_in_percent * 10 ) / 10;

  var score_for_trainning = total_marks_in_percent ;
  var comments_on_score = "("+ total_marks + " of " + check_points.length + " key validators triggered.)<br>"; // Correct (3 of 5 key validators triggered.)
  
  return {
    comments_on_invalid_tags: invalid_tags,
    comments_on_score: comments_on_score,
    total_marks:total_marks,
    total_marks_in_percent: total_marks_in_percent
  }
}

/*save to firebase database
===========================*/
function saveProject(results) {
  var project_name = $(".project-name :selected").val();
  var project_code = $(".project-code").val();
  if (project_name !== "" && project_code !== "") {

    initFirebase();

    firebase.database().ref('/offline_version_submitted_code/' + auth_id).update(
      { project: results }
    );

    $(".offline-submition-page").fadeOut();
    $(".thank-you-page").fadeIn();
  }
}

function reSubmitCode(){
  location.reload();
}

/*Retrieve learner's marks from DB
==================================*/
function getOfflineProjectRecord(){
  initFirebase();

  var ref = firebase.database().ref('/offline_version_submitted_code/' + auth_id + '/project');
  ref.on("value", function(snapshot) {
    if(snapshot.exists()){
    
      var data = snapshot.val();

        /*Construct text line for the better display
        ============================================*/
        var project1_marks= "";
        project1_marks += data.p1_training1.project_name+ ": "+ data.p1_training1.total_marks_in_percent+"%";
        project1_marks += " Correct <span class='clickable-link' onclick='showComments()'>"+ data.p1_training1.comments_on_score +"</span><br>";
        project1_marks += data.p1_training2.project_name+ ": "+ data.p1_training2.total_marks_in_percent+"%";
        project1_marks += " Correct <span class='clickable-link' onclick='showComments()'>"+ data.p1_training2.comments_on_score +"</span><br>";
        project1_marks += data.p1_training3.project_name+ ": "+ data.p1_training3.total_marks_in_percent+"%";
        project1_marks += " Correct <span class='clickable-link' onclick='showComments()'>"+ data.p1_training3.comments_on_score +"</span><br>";
        project1_marks += data.p1_training4.project_name+ ": "+ data.p1_training4.total_marks_in_percent+"%";
        project1_marks += " Correct <span class='clickable-link' onclick='showComments()'>"+ data.p1_training4.comments_on_score +"</span><br>";
       
        $("#project01_marks").html(project1_marks);

        /*Filter project-training: if have errors or not
        ================================================*/
        if(data.p1_training1.comments_on_invalid_tags === undefined){
          $("#project01_comments-per-trainging").html("You passed all challenges.");
        }
        else{
          $("#project01_comments-per-trainging").html(data.p1_training1.comments_on_invalid_tags)
        }
        if(data.p1_training2.comments_on_invalid_tags === undefined){
          $("#project02_comments-per-trainging").html("You passed all challenges.");
        }
        else{
          $("#project02_comments-per-trainging").html(data.p1_training2.comments_on_invalid_tags);
        }
        if(data.p1_training3.comments_on_invalid_tags === undefined){
          $("#project03_comments-per-trainging").html("You passed all challenges.");
        }
        else{
          $("#project03_comments-per-trainging").html(data.p1_training3.comments_on_invalid_tags);
        }
        if(data.p1_training4.comments_on_invalid_tags === undefined){
          $("#project04_comments-per-trainging").html("You passed all challenges.");
        }
        else{
          $("#project04_comments-per-trainging").html(data.p1_training4.comments_on_invalid_tags);
        }
         
        var total_marks = (parseInt(data.p1_training1.total_marks_in_percent) / 100) + (parseInt(data.p1_training2.total_marks_in_percent) / 100) + (parseInt(data.p1_training3.total_marks_in_percent) / 100) + (parseInt(data.p1_training4.total_marks_in_percent) / 100);
        total_marks = (total_marks / 4)  * 100;
        total_marks = Math.round( total_marks * 10 ) / 10;
        $("#total-marks").html("Total score: " + total_marks + "% Correct" );
    }
  }, function (error) {
    console.log("Error: " + error.code);
  });
}
