<?php $home_url = 'https://'.$_SERVER['SERVER_NAME']; ?>
<?php $current_page = 'https://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI']; ?>
<?php $email_domain = preg_replace ("~^www\.~", "", $_SERVER['SERVER_NAME']); ?>
<?php $wpcf7_form_id = "2536" ?>

 <div class="modal-body">
<link rel='stylesheet' id='contact-form-7-css'  href='<?php echo $home_url;?>/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.0.2' type='text/css' media='all' />

<script type='text/javascript'>
/* <![CDATA[ */
//var wpcf7 = {"apiSettings":{"root":"https:\/\/<?php echo $_SERVER['SERVER_NAME'];?>\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"},"recaptcha":{"messages":{"empty":"Please verify that you are not a robot."}}};
/* ]]> */
</script>
<script type='text/javascript' src='<?php echo $home_url;?>/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.0.2'></script>
<script type='text/javascript' src='<?php echo $home_url;?>/static/scripts.js'></script> 
<form  class="wpcf7-form floraforms needs-validation " class="container" novalidate="" id="myForm"   novalidate>
<div style="display: none;">
<input type="hidden" name="_wpcf7" value="<?php echo $wpcf7_form_id; ?>" />
<input type="hidden" name="_wpcf7_version" value="5.0.2" />
<input type="hidden" name="_wpcf7_locale" value="en_US" />
<input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f<?php echo $wpcf7_form_id; ?>-o1" />
<input type="hidden" name="visitor-country" value="<?php echo $_SERVER['HTTP_CF_IPCOUNTRY']; ?>" />
<input type="hidden" name="website-lang" value="<?php echo ucwords($_COOKIE['pll_language']); ?>" />
<input type="hidden" name="email-domain" value="<?php echo $email_domain; ?>" />
<input type="hidden" name="current-page" value="<?php echo $current_page; ?>" />
</div>
<div id="formMultiSteps" class="carXousel slide" data-ride="false" data-wrap="false">



    <div class="form-row mt-5 mb-2 text-center">
      <div class="col-md-24 mb-3">
      <h2 class="mb-1 text-center" >Launching soon!</h2> 
      <h4 class="text-center mb-2">Code for Change is sponsoring<br/>1,000 seats in your country.</h4>     
      <h4 class="text-center mb-3">Apply for yours now</h4>    
    <div class="form-group">

        <input type="email" class="form-control flo-input" name="your-email" required id="inputSuccess1"  placeholder="Enter your Email Address " autocomplete="off" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" style="max-width: 200px;"> <button  class="btn btn-lg text-center btn-secondary" style="color:#fff!important;margin: auto; padding: 12px 18px;" type="submit" id="btnSubmit" data-formID="<?php echo $wpcf7_form_id; ?>">GO</button>
        <div class="valid-feedback">Success! You've done it.</div>
        <div class="invalid-feedback">Enter a valid E-Mail Address</div>
    </div>
    <div>
    

  </div>
  </div>
  </div>
   

    <div class="wpcf7-response-output wpcf7-display-none"></div>

</div>
</form>

<div class="success" style="display:none;">
      <div class="col-md-24 mb-3 text-center">
        <h2 class="mt-5 mb-2" >Great stuff! Thanks for signing up.</h2> 
        <img src="<?php echo $home_url;?>/static/high-five.jpg"/>
        <p class="text-center mt-2"> We'll be in touch to let you know when we launch CodeJIKA in your country.</p>      
      </div>
</div>
</div>

<script>
$("#btnSubmit").click(function(event) {

    // Fetch form to apply custom Bootstrap validation
    var form = $("#myForm")

    if (form[0].checkValidity() === false) {
      event.preventDefault()
      event.stopPropagation()
    }
    
    form.addClass('was-validated');
    // Perform ajax submit here...
    
});

$( document ).ready(function() {

carouselNormalization('#formMultiSteps .carousel-item');

submitButton("#btnSubmit", "#myForm", "<?php echo $wpcf7_form_id; ?>" );


});
</script>