function carouselNormalization(index) {
var items = $(index), //grab all slides
    heights = [], //create empty array to store height values
    tallest; //create variable to make note of the tallest slide

if (items.length) {
    function normalizeHeights() {
        items.each(function() { //add heights to array
            heights.push($(this).height()); 
        });
        tallest = Math.max.apply(null, heights); //cache largest value
        items.each(function() {
            $(this).css('min-height',tallest + 'px');
        });
    };
    normalizeHeights();

    $(window).on('resize orientationchange', function () {
        tallest = 0, heights.length = 0; //reset vars
        items.each(function() {
            $(this).css('min-height','0'); //reset min-height
        }); 
        normalizeHeights(); //run it again 
    });
}
}


function formValidate (checkElements) {
  $(checkElements).each(function(index){ 
    console.log($(this).attr("id"));
    console.log($(this).data("valrule"));
    bootstrapValidate('#' + $(this).attr("id"), $(this).data("valrule"), 
    function (isValid) {  return isValid;  });
  });
}

function submitButton(buttonID, formID, wpcf7ID ) {
$(buttonID).click(function(event) {
console.log("start");
    // Fetch form to apply custom Bootstrap validation
    var form = $(formID)

    if (form[0].checkValidity() === false) {
      event.preventDefault()
      event.stopPropagation()
      console.log(form[0]);
    }
    
    form.addClass('was-validated');
    // Perform ajax submit here...
    $(formID).submit(function(e) {

    var url = "https://www.codejika.com/#wpcf7-f" + wpcf7ID + "-o2"; // the script where you handle the form input.

    $.ajax({
           type: "POST",
           url: url,
           data: $("#myForm").serialize() + "&_wpcf7=" + wpcf7ID + "&_wpcf7_is_ajax_call=1&_wpcf7_request_ver=" + jQuery.now(),
           success: function(data)
           {
                console.log("success"); // show response from the php script.
                
           }
         });
     
           console.log(url);
           console.log($(formID).serialize() + "&_wpcf7=" + wpcf7ID + "&_wpcf7_is_ajax_call=1&_wpcf7_request_ver=" + jQuery.now());
    e.preventDefault(); // avoid to execute the actual submit of the form.
});
     $(formID).hide();   
     $(formID).siblings('.success').show();   
});
$(formID).on('hidden.bs.modal', function (e) {
  $(formID).siblings('.success').hide();   
})
}